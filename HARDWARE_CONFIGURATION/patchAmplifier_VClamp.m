% This file contains the conversion factors needed for giving the correct
% commands to the patch amplifier and reasing the outpus signals from it.
% The resaon for this is the fact, that amplifiers generally take in and
% put out voltage commands/signals in the range of 0-10 volts. For e.g., a
% command input presented to the command port of the amplifier of 4 volts
% will tell the amplifier to hold the cell at xx mV (voltage clamp) or
% inject current of yy pA. xx and yy depend on these conversion factors.

% If you do not know how to set these, read the apmlifier manual to find
% out the conversion factors.


% conversion factors

%% VOLTAGE CLAMP
SecondaryOutputConversionFactor = 20;   % in [mA/V], THIS MUST EQUAL to V sensitivity setting of amplifier CMD, e.g. 20mV/V, i.e. 20mV to be injested for each volt of command
% TNB group at UA: -- this should normally correspond to = 1 / (Mike's AFS File to DAC mV/V seeting)

%% BOTH MODES
PrimaryOutputConversionFactor = 50;          % in [mV/mV] = [V/V], THIS MUST EQUAL to sensitivity setting of amplifier PRIMARY out , e.g. 50mV/mV, i.e. 1mV of real signal for every 50mV of amplifer output
% TNB group at UA:  -- this should normally correspond to =  1000 / (Mike's AFS ADC to File mV/V seeting)
SecondaryOutputConversionFactor = 0.25/1000 %0.005;       % in [V/pA], THIS MUST EQUAL to sensitivity setting of amplifier SECONDARY out , e.g. 0.005V/pA, i.e. 1pA of real signal for every 0.005V of amplifer output
% TNB group at UA:  -- this should normally correspond to =  1 / (Mike's AFS ADC to File pA/V seeting)
