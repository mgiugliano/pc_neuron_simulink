function [RR  taus Vrests Vs Is figh] = get_VI_R_specialForVIprotocol(foldername, varargin)
% opens files and gets R - specific to these files.

%% prepare
Vrests = []; % restig potentials
Vs = []; % voltages for VI curve
Is = []; % currents for VI curve
taus = []; % membrane time constants from each measurement
RR = 0;

threshold = 0; % spike detection threshold

ignoreSpikyTracesFlag = 1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%% EVALUATE VARAGIN %%%%%%%%%%%%%%%%%%%
% evaluating varargin; (Name, value) pairs, as ('plotFitHandle', 67)
% expected
noVars = length(varargin);
for i=1:2:noVars
    if isnumeric(varargin{i+1})
        if (isscalar(varargin{i+1})), commandName=sprintf('%s%s%f%s',varargin{i},'=',varargin{i+1},';');
        else commandName=sprintf('%s%s%s%s',varargin{i},'=',mat2str(varargin{i+1}),';');
        end
    else commandName=sprintf('%s%s%s%s',varargin{i},'=''',varargin{i+1},''';');
    end
    eval(commandName);
end



ff= figure(); hold all;

dataFiles = fuf( fullfile(  foldername ,  'RECORDINGS.mat'), 1 , 'detail');
% get data and select appropriate parts for each part of the
% analysis

for i=1:length(dataFiles)
    load(dataFiles{i});
    myvoltage = [];
    myvoltage(1,:) =  rec.voltage; % deconvoluted voltage
    mycurrent =  rec.current;
    dt = parameters.dt;
    
    % time
    time = [0:dt:dt*(length(myvoltage)-1)];
    
     
    
    if time(end)>7.5
        
        %
        signalIndex = find( (time>4.5) & (time<7.5) );
        
        % if no spikes detecten on this trace, get cell properties
        if ignoreSpikyTracesFlag==1,        out = I_extract_peaks(myvoltage(signalIndex), threshold);
        else out = [1];
        end
        
        if isempty(out)
            
            
            
            % calculate parameters
            baseIndex = find( (time>0) & (time<0.5) );
            
            % resting membrane potential
            Vrests = [Vrests mean(mean(myvoltage(baseIndex)))];
            
            % membrane time constant
            signalIndex = find( (time>0.5103) & (time<0.565) );
            expNo = 1;
            [tauuu, tauu_rmse, V0]=I_MembraneTimeConstant(myvoltage(signalIndex), time(signalIndex), expNo);
            taus = [taus tauuu*1000]; % [s] -> [ms]
            
            % resistance
            %         signalIndex = find( (time>1.4) & (time<1.6) );
            %         [RR RRstdev]=I_R_fromDeflection(myvoltage(:,baseIndex), myvoltage(:,signalIndex), mycurrent(:,signalIndex));
            
            
            % V and I
            signalIndex = find( (time>4.5) & (time<7.5) );
            Vs = [Vs mean(myvoltage(signalIndex)) - Vrests(end) ]; % V-Vrest
            Is = [Is mean(mycurrent(signalIndex))];
            
            figure(ff);
            plot(time, myvoltage);
%             Vs
%             Is
             dataFiles{i}
%             
%             keyboard
            
        else
            disp('---------Skipping trace, cell spiked!!!');
        end
    end
    
    
end

close(ff);

Vrest = mean(Vrests);
tauu = mean(taus);

% ind1 = ~isnan(Vs) & ( Vs>10 | Vs<-10 );
% ind2 = ~isnan(Is) & ( Is>10 | Is<-10 );
% ind = ind1 & ind2;
% ind = find(ind~=0);
% Vs = Vs(ind) ;
% Is = Is(ind) ;

if length(Vs)>=3
    
    %% calculate V(I) curve and get resistance
    % fitting and getting R
    s = fitoptions('Method','NonlinearLeastSquares',...
        'Lower',     [ -Inf,  -Inf  ],...
        'Upper',     [ Inf,   Inf   ],...
        'Startpoint',[ -100,  50]);
    %               a0      a1
    ff = fittype('a0 + a1 * x', 'options',s);
    [results, details] = fit(Is', Vs', ff);
    RR = results.a1*1000; %  MOhm (converted )
    %Rrmse = details.rmse; % MOhm
    
    figh = figure(); hold all;
    plot(Is,Vs,'ro');
    p = plot(results);
    legend off;
    legend(p,[num2str(RR) 'MOhm']);
    
else
    figh = [];
    RR = 0;
end

end