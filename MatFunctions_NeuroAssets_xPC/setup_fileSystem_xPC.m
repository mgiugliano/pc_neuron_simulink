function [driveLetterFound, tgfs] = setup_fileSystem_xPC(tg, driveLetter)
% This function will set up the correct drive in xPC if you are using the
% hard disk to log data to or read data from. Please make sure a FAT32
% partition exists on your harddisk.

% INPUT
% - tg: target object. You obtain this from tg =
% setup_general_model_settings_for_xPC(...) or tg=xpctarget.xpc(...);
% - driveLetter : the drive letter you want to use, for e.g. 'd:' or 'f:'.
% Use '' (empty) or 'auto' to auto-detect.

% OUTPUT:
% - tgfs is the file system object. Use it for further communication ot []
% if none established.
% - driveLetterFound: is the drive letter found or '' if none.

% NOTE: this is necessary due to a bug in xPC target 5.5 : the system tries
% to access the first partition, even if not supported. Therefore we need
% to select the right partition to access.


% Istvan Biro, 2014.03.26.

possibleDriveLetters = {'a:', 'b:', 'c:', 'd:', 'e:', 'f:', 'g:', 'h:', 'i:', 'j:', 'k:', 'l:', 'm:', 'n:', 'o:', 'p:', 'r:', 's:', 't:', 'u:', 'v:', 'w:', 'z:', 'x:', 'y:'};

tg.CommunicationTimeOut = 100; % makes the ICP/IP communication more reliable by increasing time-out

tgfs = tg.fs;  % target file system object

driveLetterFound = '';
driveIsOk = 0;

if ~ (  isempty(driveLetter) || strcmp(driveLetter,'auto')==1  ) % drive letter provided
    try
        tgfs.cd(driveLetter);
        driveIsOk = 1; 
    catch err
        driveIsOk = 0;
        disp([' --> drive ' '''' driveLetter '''' ' could not be found... searching automatically...']);
    end
end

% if drive was not found or autodect mode asked for, we look among possible
% drives
if driveIsOk~=1
    i = 1;
    while i<=length(possibleDriveLetters) && driveIsOk~=1 % look for drives
        driveLetter = possibleDriveLetters{i};
        try
            tgfs.cd(driveLetter);
            driveIsOk = 1;
        catch err
            driveIsOk = 0;
        end
        i = i + 1;
    end
end


% Say what has been done
if driveIsOk==1 , disp(['--> Drive set successfully to: ' '''' driveLetter '''' '.']); driveLetterFound = driveLetter;
else disp('--> W A R N I N G :  NO DRIVE COULD BE FOUND! Check your file system settings and disk availability! '); driveLetterFound = '';
end


end