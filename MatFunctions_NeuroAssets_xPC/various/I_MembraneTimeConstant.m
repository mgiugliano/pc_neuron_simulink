
function [tau, tau_rmse, V0]=I_MembraneTimeConstant(V, time, expNo, varargin)
% [tau, tau_rmse, V0]=I_MembraneTimeConstant(V, time, expNo, varargin[plotFitHandle, plotFitColor]);
% Get the membrane time constant (tau) in fotm of V0 + A1*exp(-t/tau1) +
% .... .
% Each trace is a ROW of a matrix ! ! !
% Usage e.g.: [tau, tau_rmse, V0]=I_MembraneTimeConstant(V, t, 2);
%             [tau, tau_rmse, V0]=I_MembraneTimeConstant(d, t, 3, 958, 'plotFitHandle', -1,  'figHandle', 567, 'xVal', xVal, figColor, [0 0 1], 'verbose', 1);

% input
% -- V : (MATRIX) voltage traces as recorded (mV). V is 2 dimensional, each row
% containing data from one trace (i.e. V(i,:) = [points of data series
% i]). NOTE!!: traces mut have the SAME LENGTH (nr. of points)!
% -- time: (ROW VECTOR) time values (s). This is only ONE row (a vector), and it is used for
% all rows of V. NOTE!!: accordingly, sampling rates must be the same.
% -- expNo: number of exponential terms to use for fitting

% varargin
% plotFitHandle, plotFitColor
% --plotFitHandle: if it is 0, the fit is not plotted, otherwise it is
% plotted on the figure with handle plotFitHandle.
% IF NEGATIVE, a new figure is generated.
% IF EXISTING and POSITIVE handle, it is used
% IF NON-EXISTING and POSITIVE, it is created
% IF ZERO, plotting is not done ! ! !
% -- plotFitColor: [r g b color]

% return:
% -- tau: membrane time constant (as the largest tau) and its error
% tau_rmse (Root mean squared error (standard error)).
% -- V0: estimated resting value (V0)

% Procedure:
% 1) time is recalculated, by substracting time(1) from all elements of
% time vector. This way time will be a vector stating from 0.
% 2) V traces are considered already alligned in time. Now they are
% averaged point by point (across all traces), resulting in one single
% vector containing the averaged trace.
% 3) a function having the requested number of exponentials and a free
% term (representing the baseline)is now fitted on the averaged trace.
% Thus the form is: V0 + a1*exp(time/tau1) + a2*exp(time/tau1) + ...
% 4) the returned value for time constant is the largest exponent. Error
% value and estimated baseline (from free term) are also returned.
% 5) (optional) If figHandle is not 0, then the resulting data point, that
% is, the value of tau is plotted on the figure having the handle
% figHandle on the Y axis, using the vaue xVal for the X axis.

% Istvan Biro
% version 2
% 2011.03.15.

% setting default values. If these are specified in varargin they are
% overwritten below.
plotFitHandle = 0;
plotFitColor = [0 0 1];

% evaluating varargin; (Name, value) pairs, as ('plotFitHandle', 67)
% expected
noVars = length(varargin);
for i=1:2:noVars
    if isnumeric(varargin{i+1})
        if (isscalar(varargin{i+1})), commandName=sprintf('%s%s%f%s',varargin{i},'=',varargin{i+1},';');
        else commandName=sprintf('%s%s%s%s',varargin{i},'=',mat2str(varargin{i+1}),';');
        end
    else commandName=sprintf('%s%s%s%s',varargin{i},'=''',varargin{i+1},''';');
    end
    eval(commandName);
end


% looking at input size...
[noOfTraces pointsPerTrace] = size(V); % get number of traces and the number of data points in each trace
if pointsPerTrace~=length(time), fprintf(1,'%s\n',' ! ERROR ! : time vector must be of the same length as the traces contain data points!'); end % just error checking...

% recalculating times to start from 0
time = time - time(1); % that is, assuming time(1) is the lowest! time MUST be an ordered list.

% averaging traces
V=V.*1e-3; %(mV to V conversion)
if noOfTraces==1, Vavg = V; % if only one trace passed, no averaging can be done
else Vavg = mean(V);        % if more than 1 trace is received, we average across them
end

% looking for how many exponentials to fit and doing the fits

switch expNo
    case 1
        %one exponential
        s = fitoptions('Method','NonlinearLeastSquares',...
            'Lower',     [ -0.1,   -10,    1e-9  ],...
            'Upper',     [ 0,      10,     1     ],...
            'Startpoint',[ -0.07,  -0.01,   0.01  ]);
        %                a0      a1     tau1
        
        ff = fittype('a0 + a1 * exp( -x / tau1 )',...
            'options',s);
        % coeffnames(ff)  % the order of the parameter values must be
        % given as the parameters appear on the call of this function.
        
        [results, details] = fit(time', Vavg', ff);
        tau = results.tau1;
        tau_rmse = details.rmse;
        V0 = results.a0;
        fitText = sprintf('%s%f%s%f','V0=',V0,' tau=',tau);
        %fitText = sprintf('%s%f%s%f%s%f','V0=',V0,' tau=',tau,' (taus=',results.tau1,')');
        
    case 2
        % two exponentials
        s = fitoptions('Method','NonlinearLeastSquares',...
            'Lower',     [ -0.1,   -10,   -10,     1e-9,   1e-9  ],...
            'Upper',     [ 0,      10,    10,      1,      1   ],...
            'Startpoint',[ -0.07,  -0.01,  -0.001,   0.01,   0.001 ]);
        %                a0      a1      a2      tau1    tau2
        
        ff = fittype('a0 + a1 * exp( -x / tau1 ) + a2 * exp( -x / tau2 )',...
            'options',s);
        % coeffnames(ff)
        
        [results, details] = fit(time', Vavg', ff);
        tau = max([results.tau1 results.tau2]);
        tau_rmse = details.rmse;
        V0 = results.a0;
        fitText = sprintf('%s%f%s%f','V0=',V0,' tau=',tau);
        %fitText = sprintf('%s%f%s%f%s%f%s%f','V0=',V0,' tau=',tau,' (taus=',results.tau1,' ',results.tau2,')');
        
    case 3
        % three exponentials
        s = fitoptions('Method','NonlinearLeastSquares',...
            'Lower',     [ -0.1,   -10,   -10,     -10,        1e-9,   1e-9,   1e-9  ],...
            'Upper',     [ 0,      10,    10,      10,         1,      1,      1     ],...
            'Startpoint',[ -0.07,  -0.01,  -0.001, -0.001,     0.01,   0.001   0.001 ]);
        %                a0      a1      a2      a3        tau1    tau2    tau3
        
        ff = fittype('a0 + a1 * exp( -x / tau1 ) + a2 * exp( -x / tau2 ) + a3 * exp( -x / tau3 )',...
            'options',s);
        % coeffnames(ff)
        
        [results, details] = fit(time', Vavg', ff);
        tau = max([results.tau1 results.tau2 results.tau3]);
        tau_rmse = details.rmse;
        V0 = results.a0;
        fitText = sprintf('%s%f%s%f','V0=',V0,' tau=',tau);
        % fitText = sprintf('%s%f%s%f%s%f%s%f%s%f','V0=',V0,' tau=',tau,'(taus=',results.tau1,' ',results.tau2,' ',results.tau3,')');
        
    otherwise
        % error
        fprintf(1,'%s\n','WARNING: wrong exp munber!');
        V0 = 0; tau = 0; tau_rmse=0;
end % end switch


% plotting the actual fitting curves if required on plot plotFitHandle
if plotFitHandle~=0
    [out]=I_plot_series(time,Vavg, 'figHandle', plotFitHandle, 'figColor', plotFitColor, 'figLegend', fitText);
    fitplot = plot(results);
    set(fitplot,'color',plotFitColor);
    xlabel('Time (s)');
    ylabel('V-Vrest (mV)');
end


end


