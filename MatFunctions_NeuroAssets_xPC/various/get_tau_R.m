function [outFileList_Vrest_single, outFileList_R_single, outFileList_tau_single, outFileList_F_currentNoise_single] = get_tau_R(dataFolder, includeFiringTest, varargin)
% ARGUMENTS :
% includeFiringTest: 1 or 0 to say if firing test is also in the recordings
% - pass -1 to set to auto, meaning extraction is attemted if file is long
% enough. (this means: if noisy current resulting in action potentials was also injected in the cell to monitor firing rate stability over time)


%
% This function will analize files obtained with running the function doCurrentNoiseStability() .
% The data is expected to be in files named "RECORDINGS.mat" and ALL OF THEM (recursively) MUST have the
% structure :
% rec =
%
%            modelname: [1x32 char]
%                   dt: 6.6667e-005
%          runDuration: 7.0099
%     current_measured: [105149x1 double]
%     current_intended: [105149x1 double]
%              voltage: [105149x1 double]
%          voltage_raw: [105149x1 double]
%               kernel: [30x1 double]
%             AECdelay: 2
%            enableAEC: 1
%           kernelFile: [1x91 char]
%           systemtime: [2012 11 13 11 58 5.4530]

% OUTPUTs for each "RECORDINGS.mat" file are the files :
% - prepr_Vrest_single.mat : resting potential calculated from SINGLE negative
%   current pulse and its voltage response.
%   ...containing:
%   * time of aquisition
%   * resting potential calculated
% - prepr_R_single.mat : resistance calculated from SINGLE negative
%   current pulse and its voltage response.
%   ...containing:
%   * time of aquisition
%   * Resistance calculated
% - prepr_tau_single.mat : membr time constant calculated from SINGLE negative
%   current pulse and its voltage response.
%   ...containing:
%   * time of aquisition
%   * membrane time constant
% - prepr_F_currentNoise_single.mat : firing properties calculated from
% SINGLE noisy current injection and its voltage response.
%   ...containing:
%   * time of aquisition
%   * firing.xx (F ISI meanISI stdISI cvISI Nspikes)
%       - F : (number of spikes)/(trace time)
%       - ISI : interspike intervals, one by one
%       - meanISI : mean interspike interval
%       - stdISI : std of interspike intervals
%       - cvISI : coefficient of variation of interspike intervals
%       - Nspikes : total number of spikes
%   * injectedCurrent.xx (mean std)

% OUTPUTs are in the same folder as the respective "RECORDINGS.mat" file.

% Istvan Biro, 2012.12.02.



%% PARAMETERS
threshold = -5;             % [mV] threshold for sike detection

checkForSpikesInPassiveProperties = 1; % 1 or 0 : check or not if an AP is present in the traces used for passeive cell properties. If checking is on and an AP is present, the passive properties will be [].

% Vrest part
vrestStartStopTime = [0 0.5]; % [s] starting and stopping time of the trace to extract Vrest from.

% tau part
tauStartStopTime = [0.5103 0.565]; % [s] starting and stopping time of the trace to extract tau from.
expNo = 1;              % number of esplonentilas to fit - use 1, 2 or 3. 1 is best though.

% R part
RStartStopTime = [1.4 1.6]; % [s] starting and stopping time of the trace to extract R from.

% Firing part
FireStartStopTime = [2.4 6.8]; % [s] starting and stopping time of the trace to extract firing properties from.


%% EVALUATE VARARGIN
% evaluating varargin; (Name, value) pairs, as ('plotFitHandle', 67)
% expected
noVars = length(varargin);
for i=1:2:noVars
    if isnumeric(varargin{i+1})
        if (isscalar(varargin{i+1})), commandName=sprintf('%s%s%f%s',varargin{i},'=',varargin{i+1},';');
        else commandName=sprintf('%s%s%s%s',varargin{i},'=',mat2str(varargin{i+1}),';');
        end
    else commandName=sprintf('%s%s%s%s',varargin{i},'=''',varargin{i+1},''';');
    end
    eval(commandName);
end



%% DO THE JOB
% get all files named "RECORDINGS.mat", recursively.
fileList = fuf( fullfile(dataFolder,  'RECORDINGS.mat'), 1 , 'detail');
outFileList_Vrest_single = {};
outFileList_R_single = {};
outFileList_tau_single = {};
outFileList_F_currentNoise_single = {};

% for all files perform the necessary operations
for i=1:length(fileList)
    load(fileList{i}); % see resulting structure in function description
    
    [pathstr, name, ext] = fileparts(fileList{i});
    
    % fileList{i}
    
    % clear old
    voltage = [];
    current = [];
    time = [];
    
    % obtain traces
    voltage(1,:) = rec.voltage(:);
    current(1,:) = rec.current(:);
    dt = parameters.dt;
    time(1,:) = 0:dt:dt*(length(voltage)-1);
    systemtime = parameters.systemtime;
    
    % get resting potential
    ind = find( time>= vrestStartStopTime(1)  & time <= vrestStartStopTime(2));
    peakIndexes = extract_peaks(voltage(ind), threshold);
    if checkForSpikesInPassiveProperties==1 & isempty(peakIndexes) % if no spikes here
        Vrest = mean(voltage(ind));
        Vrest_stdev = std(voltage(ind));
    else
        Vrest = [];
        Vrest_stdev = [];
    end
    baseIndex = ind; % remember for later
    
    outName = fullfile(pathstr , 'prepr_Vrest_single.mat');
    save(outName, 'systemtime', 'Vrest', 'Vrest_stdev');
    outFileList_Vrest_single{i} = outName;
    
    % get tau
    ind = find( time>= tauStartStopTime(1)  & time <= tauStartStopTime(2));
    peakIndexes = extract_peaks(voltage(ind), threshold);
    if checkForSpikesInPassiveProperties==1 & isempty(peakIndexes) % if no spikes here
        [tau, tau_rmse, V0]=I_MembraneTimeConstant(voltage(ind), time(ind), expNo);
    else
        tau = [];
        tau_rmse = [];
    end
    
    outName = fullfile(pathstr , 'prepr_tau_single.mat');
    save(outName, 'systemtime', 'tau', 'tau_rmse');
    outFileList_tau_single{i} = outName;
    
    % get R
    ind = find( time>= RStartStopTime(1)  & time <= RStartStopTime(2));
    peakIndexes = extract_peaks(voltage(ind), threshold);
    if checkForSpikesInPassiveProperties==1 & isempty(peakIndexes) % if no spikes here
        [R Rstdev]=I_R_fromDeflection(voltage(baseIndex), voltage(ind), current(ind));
    else
        R = [];
        Rstdev = [];
    end
    
    outName = fullfile(pathstr , 'prepr_R_single.mat');
    save(outName, 'systemtime', 'R', 'Rstdev');
    outFileList_R_single{i} = outName;
    
    
    % get Firing properties
    if ( includeFiringTest == 1 ) || ( includeFiringTest == -1 && time(end) >= FireStartStopTime(2) )
        firing = []; injectedCurrent = [];
        ind = find( time>= FireStartStopTime(1)  & time <= FireStartStopTime(2));
        peakIndexes = extract_peaks(voltage(ind), threshold);
        if checkForSpikesInPassiveProperties==1 & ~isempty(peakIndexes) % if THERE ARE SPIKES
            [firing.F firing.ISI firing.meanISI firing.stdISI firing.cvISI firing.Nspikes] = getFiringProperties(voltage(ind), dt, 'threshold', threshold);
        else
            firing.F = 0;
            firing.ISI = [];
            firing.meanISI = [];
            firing.stdISI = [];
            firing.cvISI = [];
            firing.Nspikes = 0;
        end
        injectedCurrent.mean = mean(current(ind));
        injectedCurrent.std = std(current(ind));
        
        outName = fullfile(pathstr , 'prepr_F_currentNoise_single.mat');
        save(outName, 'systemtime', 'firing', 'injectedCurrent');
        outFileList_F_currentNoise_single{i} = outName;
    else
        outFileList_F_currentNoise_single{i} = '';
    end
    
end



end