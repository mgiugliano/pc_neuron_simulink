function [Gm_exc,Gm_inh,Gs_exc,Gs_inh] = ...
    computeSynapticBackgroundCoefficients(Rm, R_exc, R_excOverR_inh, tau_exc, tau_inh)
% [G_exc,G_inh,Gs_exc,Gs_inh] = 
%     computeSynapticBackgroundCoefficients(Rm, R_exc, R_excOverR_inh, tau_exc, tau_inh)
% 
% Parameters:
%              Rm - membrane resistance (MOhm).
%           R_exc - rate of excitatory inputs (Hz).
%  R_excOverR_inh - ratio between excitatory and inhibitory inputs.
%         tau_exc - time constant of excitatory inputs (msec), default 5.
%         tau_inh - time constant of inhibitory inputs (msec), default 10.
% 

if ~ exist('tau_exc','var')
    tau_exc = 5;
end
if ~ exist('tau_inh','var')
    tau_inh = 10;
end

Rm = Rm * 1e6;              % (Ohm)
tau_exc = tau_exc*1e-3;     % (s)
tau_inh = tau_inh*1e-3;     % (s)
g_exc = 0.02 / Rm;          % (S)
g_inh = 0.06 / Rm;          % (S)
% g_exc = 50e-12;         % (S)
% g_inh = 190e-12;        % (S)

R_inh = R_exc/R_excOverR_inh;
G_exc = g_exc * tau_exc * R_exc;
G_inh = g_inh * tau_inh * R_inh;
Gm_exc = G_exc * 1e9;        % (nS)
Gm_inh = G_inh * 1e9;        % (nS)
D_exc = 0.5 * g_exc^2 * tau_exc^2 * R_exc;
D_inh = 0.5 * g_inh^2 * tau_inh^2 * R_inh;
Gs_exc = sqrt(D_exc / tau_exc) * 1e9;   % (nS)
Gs_inh = sqrt(D_inh / tau_inh) * 1e9;   % (nS)
