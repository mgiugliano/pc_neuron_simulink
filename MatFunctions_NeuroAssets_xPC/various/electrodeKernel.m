function [Ke,Re,Km,Rm,Taum] = electrodeKernel(K,startTail,dt)
% ELECTRODEKERNEL extracts the electrode kernel Ke from the raw kernel K
% by removing the membrane kernel, estimated from the
% indexes >= startTail of the raw kernel.
%
% [Ke,Re,Km,Rm,Taum] = electrodeKernel(K,startTail)
%
% Parameters:
%           K - the raw kernel (in MOhms).
%   startTail - the beginning of the tail of the kernel (number of samples).
%          dt - sampling time step.
% 
% Returns:
%          Ke - the electrode kernel, of length startTail samples (in MOhms).
%          Re - the electrode resistance (in MOhms).
%          Km - the membrane kernel (in MOhms).
%          Rm - the membrane resistance (in MOhms).
%        Taum - the membrane time constant (same units as dt).
% 
%
% R. Brette, Sep 2005
%
% Modified by D. Linaro, April 2012, to return additional values,
% among which the membrane kernel.
% 

    function [err,Kel]=removeKm(RawK,x,tau,dt,tail)
    % Solves Ke = RawK - Km * Ke/Re
    % for an exponential Km
    % Returns the error on the tail
    
        alpha=x*dt/tau;
        lambda=exp(-dt/tau);
        Y=0*RawK;
        Y(1)=alpha/(alpha+1)*RawK(1); % K(1) == 0 ?
        for i=2:length(RawK)
            Y(i)=(alpha*RawK(i)+lambda*Y(i-1))/(1+alpha);
        end
        Kel=RawK-Y;
        err=Kel(tail)'*Kel(tail);
    end

    % Exponential fit of the tail
    mymodel=fittype('a*exp(b*x)');
    tail=startTail:length(K);
    t=((0:(length(K)-1))*dt)';
    rfit=fit(t(tail),K(tail),mymodel,'start',[1,-0.2]);

    % Membrane time constant and resistance
    tau = -1 / rfit.b;
    R = rfit.a*tau/dt - sum(rfit.a*exp(rfit.b*t(1:2)));
    % NB: the first two steps are ignored

    % Electrode resistance
    Re = sum(K(3:startTail))-sum(rfit.a*exp(rfit.b*t(3:startTail)));

    % Replace tail by fit
    K(tail)=feval(rfit,t(tail));

    % Clean the beginning (optional)
    %K(1:2)=0;

    Km = feval(rfit,t);

    % Optimize the membrane kernel
    z = fminbnd(@(x)removeKm(K,x,tau,dt,tail),0.5*R/Re,2*R/Re);
    [err,Ke] = removeKm(K,z,tau,dt,tail);

    % Output values
    Ke = Ke(1:startTail);
    Re = sum(Ke);
    Rm = z*Re;
    Taum = tau;

end
