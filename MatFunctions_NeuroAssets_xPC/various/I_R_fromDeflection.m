

function [R Rstdev]=I_R_fromDeflection(Vbases, Vnews, Currents)
% [R Rstdev]=I_RfromDeflection(Vbases, Vnews, Currents);
% Get the membrane resistance R (in MOhm) from: R = (Vnew-Vbase) / Currents (point by point and then average).
% Each trace is a ROW of a matrix! ! !
% !!! All V traces in Vnews must result from the same Currents I applied !!!
% Usage e.g.: [R Rstdev]=I_RfromDeflection(Vbases, Vnews, Currents);

% input
% -- Vbases : (MATRIX) BASELINE (where Iapplied=0) voltage traces as recorded (mV).
% Each Vbase trace is in a row of Vbases.
% -- Vnews : (MATRIX) deflected (where I=Iapplied<>0) voltage trace as recorded (mV).
% Each Vnew trace is in a row of Vnews.
% -- Currentss: (MATRIX) applied Currents in (pA). MUST be the same size
% as Vnew.


% return:
% -- R: resistance in MOhm
% -- Rstdev: standard deviation of R

% Procedure:
% 1) Each of the base taces Vbases is averaged: VbaseAvgs contains then
% the abverage baseline levels for each trace.
% 2) The base value corresponding to each signal trace is now substracted
% to obtain the difference-traces: DVs = Vnews - VbaseAvgs. (here each row
% is a difference-trace).
% 3) Calculate resistance point by point from the DVs and Currents by
% RPByP = DVavg ./ Currents , for all traces.
% 4) Calculate resistance averages for each trace.
% 5) Average for overall resistance.

% Istvan Biro
% version 4
% 2011.05.22.


% checking size restrictions
[noOfTraces noOfPoints] = size(Vnews);
[noOfBaseTraces noOfBasePoints] = size(Vbases);
if size(Vnews)~=size(Currents), error('ERROR: The size of Vnews and the size of I must be the same!'); end
if noOfTraces~=noOfBaseTraces, error('ERROR: number of signal traces and the number of base traces must be the same!'); end

% averaging base traces
if noOfTraces==1, VbaseAvgs = mean(Vbases); % if there is only 1 trace, there is only 1 mean
else VbaseAvgs = mean(Vbases'); % ' needed as mean averages a matrix by columns, giving a row vector with the averages of each column
end

% substracting the corresponding average base value from each point of the respective trace
DVs = zeros(noOfTraces,noOfPoints);
for i=1:noOfTraces
    DVs(i,:) = Vnews(i,:) - VbaseAvgs(i);
end


% Calculate resistance point by point from the DVs and Currents
RPByP = [];
for i=1:noOfTraces
    RPByP(i,:) = DVs(i,:) ./ Currents(i,:) *1000; % *1000 is the conversion to MOhm
end


% Average to get average resistance
if noOfTraces==1, Rtraces = RPByP;  % each element of Rtraces contains now the average resistance of a trace
else Rtraces = mean(RPByP');
end
R = mean(Rtraces);
Rstdev = std(Rtraces);
end


