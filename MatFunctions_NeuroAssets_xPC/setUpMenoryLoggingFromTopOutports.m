function setUpMenoryLoggingFromTopOutports(myModelName, runDuration, myStepSize)

startTime = 0;

    par = find_system(myModelName, 'SearchDepth', 1, 'BlockType', 'Outport');  % looking how many outport blocks are on the top level (need loggong).
    signalNumber = length(par);
    
    TETlogFlag = 0;                   % 1=on OR 0=off : set enabling or disabling of TET logging (note: this is regarded as a signal too -memory allocation...)
    % what to log
    OUTPORTSignalLoggingFlag = 1;
    SaveFinalStateFlag	=  0;         % log final state? 1=on 0=off
    SaveStateFlag		=  0;         % log data at state? 1=on 0=off
    SaveTimeFlag		=  0;         % log time? 1=on 0=off
    SaveOutputFlag		=  1;         % log data at Outports (each a column)? 1=on 0=off
    
    DSMLoggingFlag = 0;               % 1=on OR 0=off
    SignalLoggingSaveFormat = 'ModelDataLogs';  % to log from outport (?) - leave it like this
    SignalLoggingName = 'logsout';    % logging name - leave as it is.
    DSMLoggingName	  ='dsmout';      % logging name - leave as it is.

    
%%%%%% calculating consequences and needs rising from your requests... (DO NOT MODIFY)
if runDuration==inf
    RD = 60; % record 60 seconds then overwrite
    disp(' ----------------  ');
    disp(' !!! WARNING : infinite running time. OUTPORTs will only be recorded for the last 60 seconds.');
    (' ----------------  ');
else RD = runDuration; end
totalPointsStore = 10000 + (TETlogFlag + SaveStateFlag + SaveTimeFlag + signalNumber)*RD/myStepSize;
% space for TET, time, state and "signalNumber" variables (SaveOutput) - during entire run
% this is the total number of points (values) to be logged. Thus if you log time and one data (via one outport), thus 2 things, each will have its last totalPointsStore/2 points stored.
% 10000 for security...
% do not excede memory!
if TETlogFlag==0, TETlogSting = 'off'; else TETlogSting = 'on'; end
if OUTPORTSignalLoggingFlag==0, SignalLoggingString = 'off'; else SignalLoggingString = 'on'; end
if DSMLoggingFlag==0, DSMLogging = 'off'; else DSMLogging = 'on'; end
if SaveFinalStateFlag==0, SaveFinalState = 'off'; else SaveFinalState = 'on'; end
if SaveOutputFlag==0, SaveOutput = 'off'; else SaveOutput = 'on'; end
if SaveStateFlag==0, SaveState = 'off'; else SaveState = 'on'; end
if SaveTimeFlag==0, SaveTime = 'off'; else SaveTime = 'on'; end

% set them
set_param(myModelName,'StartTime',num2str(startTime)); % time zero
set_param(myModelName,'FixedStep',num2str(myStepSize)); % step size
set_param(myModelName,'StopTime',num2str(startTime+runDuration)); % step size
set_param(myModelName,'DSMLogging',DSMLogging); % to workspace logging
set_param(myModelName,'DSMLoggingName',DSMLoggingName); % to workspace logging
set_param(myModelName,'SaveFinalState',SaveFinalState); % to workspace logging
set_param(myModelName,'SaveOutput',SaveOutput); % to workspace logging
set_param(myModelName,'SaveState',SaveState); % to workspace logging
set_param(myModelName,'SaveTime',SaveTime); % to workspace logging
set_param(myModelName,'SignalLoggingName',SignalLoggingName); % logging name.
set_param(myModelName,'SignalLoggingSaveFormat',SignalLoggingSaveFormat); % to save from.
set_param(myModelName,'SignalLogging',SignalLoggingString); % enable or disable signal logging
set_param(myModelName,'RL32LogTETModifier',TETlogSting); % set enabling or disabling of TET logging
set_param(myModelName,'RL32LogBufSizeModifier',num2str(totalPointsStore)); % set logging memory (no of doubles) for all signals logged





end