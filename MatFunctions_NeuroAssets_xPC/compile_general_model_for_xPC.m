function compile_general_model_for_xPC(myModelName)

% here is the compilation command of the model: mdl-->dlm
% adding the .dlm file now to our Target Object (preresenting the Target PC)
open(myModelName);
set_param(myModelName,'xPCisDownloadable','off');  % Do not download automatically after building.
set_param(myModelName,'RTWVerbose','off');         % Configure for a non-Verbose build. Warnings on though.

% save and compile
save_system(myModelName);
rtwbuild(myModelName);

end
