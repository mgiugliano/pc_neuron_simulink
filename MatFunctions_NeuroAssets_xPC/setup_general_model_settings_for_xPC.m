function tg = setup_general_model_settings_for_xPC(myTargetPcName , myModelName, runDuration, myStepSize, varargin)
% This function receives the name of the target machine (myTargetPcName) on
% which the model (myModelName) is supposed to be ran and it sets up a
% general set of conditions, that -in most cases- are well suited to run
% this model under xPC.
% 'runDuration' and 'myStepSize' are the total run duration of this model (seconds) and
% its discrete timestep (in seconds, at which data is logged to memory - 
% - this is typically the smallest timestep used for any block in the
% model), respectively.
%
% tg - is the target object (handle).
%
%  Take this function as a useful tool for getting started quickly, as 
% it contains the most likely parameters you will use -- or as inspiration for  writing your own.  
%
% Notes:
% - PLEASE MAKE SURE TO RUN setup_hostSettings_xPC_boot(..) - provided with
% this package, ONCE, when you setup your system, but before calling this
% function. This will set up the myTargetPcName that you need here.
% - myTargetPcName : if provided empty, 'myOneAndOnlyXPC' will be set. This
% is what setup_hostSettings_xPC_boot(..) normally creats.

% Istvan Biro, 2014.02.12.


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%% DEFAULT PARAMETERS %%%%%%%%%%%%%%%%% 
% Setting up default parameters. Note that you can overwrite/change them
% here or using varargin: all variabled defined before the section "%%
% EVALUATE VARAGIN %%" can be overwitten by providing varargin to the
% function as: (Name, value) pairs, as ('plotFitHandle', 67).

OUTPORTSignalLoggingFlag = 1 ;  % 1 or 0 : log or not signals from OUTPUT ports in the model. Only top level outout ports are logged, i.e. the ones on the top level of the model, not in subsystems

MultiTaskingFlag = 0;           % 1 or 0: if 1, the solver will work in 'MultiTasking' mode otherwise 'Auto'. Note: Solver mode can be: 'Auto', 'MultiTasking' or 'SingleTasking' -- 'MultiTasking' is NECESSARY for blocks with different timesteps and uses multiple threads. It REQUIRES multicore computers. 

startTime = 0;                    % time zero of execution start


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%% EVALUATE VARAGIN %%%%%%%%%%%%%%%%%%%
% evaluating varargin; (Name, value) pairs, as ('plotFitHandle', 67)
% expected
noVars = length(varargin);
for i=1:2:noVars
    if isnumeric(varargin{i+1})
        if (isscalar(varargin{i+1})), commandName=sprintf('%s%s%f%s',varargin{i},'=',varargin{i+1},';');
        else commandName=sprintf('%s%s%s%s',varargin{i},'=',mat2str(varargin{i+1}),';');
        end
    else commandName=sprintf('%s%s%s%s',varargin{i},'=''',varargin{i+1},''';');
    end
    eval(commandName);
end



%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%% APPLY MODOFICATION %%%%%%%%%%%%%%%%%

% ********************** OPEN MODEL

fprintf(1,'%s%s%s\n',' +++++ opening model ',myModelName,' & setting parameters...');
open(myModelName);                                 % open model to be able to set parameters


% ********************** LOGGING OF SIGNALS - please put the right OUTPORT blocks in the model first

if OUTPORTSignalLoggingFlag == 1
    par = find_system(myModelName, 'SearchDepth', 1, 'BlockType', 'Outport');  % looking how many outport blocks are on the top level (need loggong).
    signalNumber = length(par);
    
    TETlogFlag = 1;                   % 1=on OR 0=off : set enabling or disabling of TET logging (note: this is regarded as a signal too -memory allocation...)
    % what to log
    SaveFinalStateFlag	=  0;         % log final state? 1=on 0=off
    SaveStateFlag		=  1;         % log data at state? 1=on 0=off
    SaveTimeFlag		=  1;         % log time? 1=on 0=off
    SaveOutputFlag		=  1;         % log data at Outports (each a column)? 1=on 0=off

    DSMLoggingFlag = 1;               % 1=on OR 0=off
    SignalLoggingSaveFormat = 'ModelDataLogs';  % to log from outport (?) - leave it like this
    SignalLoggingName = 'logsout';    % logging name - leave as it is.
    DSMLoggingName	  ='dsmout';      % logging name - leave as it is.
else
    signalNumber = 0;
    
    TETlogFlag = 0;                   % 1=on OR 0=off : set enabling or disabling of TET logging (note: this is regarded as a signal too -memory allocation...)
    SaveFinalStateFlag	=  0;         % log final state? 1=on 0=off
    SaveStateFlag		=  0;         % log data at state? 1=on 0=off
    SaveTimeFlag		=  0;         % log time? 1=on 0=off
    SaveOutputFlag		=  0;         % log data at Outports (each a column)? 1=on 0=off
    DSMLoggingFlag = 0;               % 1=on OR 0=off
    SignalLoggingName = 'logsout';    % logging name - leave as it is.
    DSMLoggingName	  ='dsmout';      % logging name - leave as it is.
    SignalLoggingSaveFormat = 'ModelDataLogs';  % to log from outport (?) - leave it like this
end
%%%%%% calculating consequences and needs rising from your requests... (DO NOT MODIFY)
if runDuration==inf
    RD = 60; % record 60 seconds then overwrite
    disp(' ----------------  ');
    disp(' !!! WARNING : infinite running time. OUTPORTs will only be recorded for the last 60 seconds.');
    (' ----------------  ');
else RD = runDuration; end
totalPointsStore = 10000 + (TETlogFlag + SaveStateFlag + SaveTimeFlag + signalNumber)*RD/myStepSize;
% space for TET, time, state and "signalNumber" variables (SaveOutput) - during entire run
% this is the total number of points (values) to be logged. Thus if you log time and one data (via one outport), thus 2 things, each will have its last totalPointsStore/2 points stored.
% 10000 for security...
% do not excede memory!
if TETlogFlag==0, TETlogSting = 'off'; else TETlogSting = 'on'; end
if OUTPORTSignalLoggingFlag==0, SignalLoggingString = 'off'; else SignalLoggingString = 'on'; end
if DSMLoggingFlag==0, DSMLogging = 'off'; else DSMLogging = 'on'; end
if SaveFinalStateFlag==0, SaveFinalState = 'off'; else SaveFinalState = 'on'; end
if SaveOutputFlag==0, SaveOutput = 'off'; else SaveOutput = 'on'; end
if SaveStateFlag==0, SaveState = 'off'; else SaveState = 'on'; end
if SaveTimeFlag==0, SaveTime = 'off'; else SaveTime = 'on'; end

% set them
set_param(myModelName,'StartTime',num2str(startTime)); % time zero
set_param(myModelName,'FixedStep',num2str(myStepSize)); % step size
set_param(myModelName,'StopTime',num2str(startTime+runDuration)); % step size
set_param(myModelName,'DSMLogging',DSMLogging); % to workspace logging
set_param(myModelName,'DSMLoggingName',DSMLoggingName); % to workspace logging
set_param(myModelName,'SaveFinalState',SaveFinalState); % to workspace logging
set_param(myModelName,'SaveOutput',SaveOutput); % to workspace logging
set_param(myModelName,'SaveState',SaveState); % to workspace logging
set_param(myModelName,'SaveTime',SaveTime); % to workspace logging
set_param(myModelName,'SignalLoggingName',SignalLoggingName); % logging name.
set_param(myModelName,'SignalLoggingSaveFormat',SignalLoggingSaveFormat); % to save from.
set_param(myModelName,'SignalLogging',SignalLoggingString); % enable or disable signal logging
set_param(myModelName,'RL32LogTETModifier',TETlogSting); % set enabling or disabling of TET logging
set_param(myModelName,'RL32LogBufSizeModifier',num2str(totalPointsStore)); % set logging memory (no of doubles) for all signals logged






% ********************** COMPILER SETTINGS - only modify if you are familiar with it

SystemTargetFile	=  'xpctarget.tlc';  % ESSENTIAL - otherwise code generated is not suitable for xPC.
GenCodeOnly		    =  'off';            % to also have dlm executable (?)
MakeCommand		    = 'make_rtw';        % compiler command - leave it so
GenerateMakefile	=  'on';             % to also have dlm executable (?)
TemplateMakefile	=  'xpc_default_tmf';% ...
Description		    = 'xPC Target';      % ...
RTWCompilerOptimization = 'On';          % optimized code for faster run

% SOLVER - set the most important solver parameters. The solver evolves the
% model in time.
Solver		=  'FixedStepDiscrete';   % the solver engine type.
%'FixedStepDiscrete' is a discrete
%step-taker with a fixed step size of
%myStepSize. Continuous ones also
%exist (as ode4), and others...
if MultiTaskingFlag ==1, SolverMode = 'MultiTasking';  % 'Auto', 'MultiTasking' or 'SingleTasking' -- 'MultiTasking' allows blocks with different timesteps and uses multiple threads. It REQUIRES multicore computers. 
else SolverMode = 'Auto';
end
    
% now we set them...     
% set model parameters (those which you want to be sure of!)
set_param(myModelName,'Solver',Solver); % set Solover
set_param(myModelName,'SolverMode',SolverMode); % 
set_param(myModelName,'SystemTargetFile',SystemTargetFile);
set_param(myModelName,'GenCodeOnly',GenCodeOnly);
set_param(myModelName,'MakeCommand',MakeCommand);
set_param(myModelName,'GenerateMakefile',GenerateMakefile);
set_param(myModelName,'TemplateMakefile',TemplateMakefile);
set_param(myModelName,'Description',Description);
set_param(myModelName,'RTWCompilerOptimization',RTWCompilerOptimization);



% ********************** compile and download settings

% here is the compilation command of the model: mdl-->dlm
% adding the .dlm file now to our Target Object (preresenting the Target PC)
set_param(myModelName,'xPCisDownloadable','off');  % Do not download automatically after building.
set_param(myModelName,'RTWVerbose','off');         % Configure for a non-Verbose build. Warnings on though.



% ********************** LOOKING FOR TARGET (ENVIRONMENT)

% get the targets, from which one can tell the current one... or create a new one
tgs = xpctarget.targets; % tgs is a "target object collection environment container"

if isempty(myTargetPcName), myTargetPcName = 'myOneAndOnlyXPC'; end

% look if a Target Object Environment named myTargetPcName exists...
allNames = tgs.getTargetNames; % all Target Object Environment names
ii = 0; found = 0;
while found==0 && ii<length(allNames)  % search...
    ii = ii + 1;
    if strcmp(allNames{ii},myTargetPcName), found=1; end
end
if found == 0 % if myTargetPcName does not yet exist, complain
    error('  !!!! ERROR !!! - cannot find specified target. Please make sure the name is correct and/or run setup_hostSettings_xPC_boot(..) to create it.');
else % if it already exists, we select it
    tge = tgs.Item(myTargetPcName);
end
% now tge is the  Target Object Environment that we want to set (and use it for our Target Object)

tg = xpctarget.xpc(myTargetPcName); % tg is now the effective Target Object (representing the Target PC)



% ********************** OTHER SETTINGS
tg.CommunicationTimeOut = 100;   % makes the ICP/IP communication more reliable


end