function [amplitudes upStrokeSpeeds upstrokeFit downStrokeSpeeds downstrokeFit halfwidths startTimes peakTimes stopTimes] = exractSpikes(timeTrace, voltageTrace, vrest, threshold, n)
% Extracts spike properties of all spikes in this trace. Vrest must be the
% resting potential [mV]. n is the number of (first comming) spikes to look
% at.

% startTimes peakTimes stopTimes are the beginning , the peak and the
% ending times of the action potentials

% spike detection parameters 
% > spikes are originally taken from base level to their peak values...
% > their slope is estimated from  lowSlopeEstimateCut % to highSlopeEstimateCut% of this height -- rising and
% falling phase separately.
% > now spike detection is refined by considering the spike to start and
% end where its slope gets less than slopeCutPercent of this estimated
% slope.
% > now spike amplitude is taken from spike onset point to maximum, slope
% (upstroke, downstroke) is taken from lowSlopeCut to highSlopeCut % of
% this amplitude, by linear fit. Halfwidth is taken as width at half
% amplitude, between points closest to half width, actually. 

%%% Istvan Biro, 2015.

plotting = 0;   % 1 or 0 to enable or disable plotting

lowSlopeEstimateCut = 40;
highSlopeEstimateCut = 85;
slopeCutPercent = 50; 
lowSlopeCut = 10;
highSlopeCut = 70;

%% initialize
amplitudes = [];
upStrokeSpeeds  = [];
downStrokeSpeeds  = [];
halfwidths = [];
startTimes = [];
peakTimes = [];
stopTimes = [];
upstrokeFit = [];
downstrokeFit = [];

%% detect spikes
peakIndexes = extract_peaks(voltageTrace, threshold);
if n>length(peakIndexes), n=length(peakIndexes); end

if plotting==1
    fig=figure(66666); 
    close(fig); 
    fig=figure(66666); 
    plot(timeTrace, voltageTrace,'ro-'); 
    hold on; 
end

%% go peak by peak and analize
upstrokeHighIndex_est=0; upstrokeLowIndex_est=0;
for i=1:n
    errorHappened=0;
    
    % > spikes are originally taken from base level to their peak values...
    height = abs(voltageTrace(peakIndexes(i))-vrest);
    maxtime = timeTrace(peakIndexes(i));
    %> their slope is estimated from  lowSlopeEstimateCut % to highSlopeEstimateCut% of this height -- rising and falling phase separately
    ylow = vrest + lowSlopeEstimateCut/100*height;
    yhigh = vrest + highSlopeEstimateCut/100*height;
    % now looking for the location of these points going down the spike
    found_max = 0; found_min = 0; here = peakIndexes(i);
    while (found_max==0 || found_min==0) && here>1 % look for maximum and minimum index of upstroke
        here = here -1;
        if voltageTrace(here)<= yhigh && found_max==0, found_max=1; upstrokeHighIndex_est = here; end
        if voltageTrace(here)<= ylow && found_min==0, found_min=1; upstrokeLowIndex_est = here; end
    end
    if (found_max==0 || found_min==0), errorHappened=1;  end % no good
    
    if upstrokeHighIndex_est-upstrokeLowIndex_est<3, upstrokeHighIndex_est=upstrokeHighIndex_est+1; upstrokeLowIndex_est=upstrokeLowIndex_est-2; end
    if (upstrokeLowIndex_est<=0 || upstrokeHighIndex_est>length(voltageTrace)), errorHappened=1; end
   if errorHappened==1, fprintf(1,'%s\n','WARNING: ERROR IN SPIKE DETECTION - putting to 0.'); amplitudes(i)=0;  upStrokeSpeeds(i)=0; upstrokeFit(i)=0; downStrokeSpeeds(i)=0; downstrokeFit(i)=0; halfwidths(i)=0; startTimes(i)=0; peakTimes(i)=0; stopTimesend(i)=0; return; end

    
    
    found_max = 0; found_min = 0; here = peakIndexes(i);     % now we look for downstroke
    while (found_max==0 || found_min==0) && here<length(voltageTrace)-1 % look for maximum and minimum index of downstroke
        here = here +1;
        if voltageTrace(here)<= yhigh && found_max==0, found_max=1;downstrokeHighIndex_est = here; end
        if voltageTrace(here)<= ylow && found_min==0, found_min=1; downstrokeLowIndex_est = here; end
    end
    if (found_max==0 || found_min==0), errorHappened=1;  end % no good
    
   if errorHappened==1, fprintf(1,'%s\n','WARNING: ERROR IN SPIKE DETECTION - putting to 0.'); amplitudes(i)=0;  upStrokeSpeeds(i)=0; upstrokeFit(i)=0; downStrokeSpeeds(i)=0; downstrokeFit(i)=0; halfwidths(i)=0; startTimes(i)=0; peakTimes(i)=0; stopTimesend(i)=0; return; end


   if downstrokeLowIndex_est-downstrokeHighIndex_est<3, downstrokeLowIndex_est=downstrokeLowIndex_est+2; downstrokeHighIndex_est=downstrokeHighIndex_est-2; end
   if (downstrokeHighIndex_est<=0 || downstrokeLowIndex_est>length(voltageTrace)), errorHappened=1; end
   if errorHappened==1, fprintf(1,'%s\n','WARNING: ERROR IN SPIKE DETECTION - putting to 0.'); amplitudes(i)=0;  upStrokeSpeeds(i)=0; upstrokeFit(i)=0; downStrokeSpeeds(i)=0; downstrokeFit(i)=0; halfwidths(i)=0; startTimes(i)=0; peakTimes(i)=0; stopTimesend(i)=0; return; end

    
    upstrokeSpeed_est = ( voltageTrace(upstrokeLowIndex_est)-voltageTrace(upstrokeHighIndex_est) )/(  timeTrace(upstrokeLowIndex_est)-timeTrace(upstrokeHighIndex_est)  ); % estimated, +
    downstrokeSpeed_est = ( voltageTrace(downstrokeLowIndex_est)-voltageTrace(downstrokeHighIndex_est) )/(  timeTrace(downstrokeLowIndex_est)-timeTrace(downstrokeHighIndex_est)  ); % estimated, -

    % > now spike detection is refined by considering the spike to start and
    % end where its slope gets less than slopeCutPercent of this estimated
    % slope.
    diffvoltageTrace = diff(voltageTrace)./diff(timeTrace);
    
    found=0; here = upstrokeLowIndex_est; % we start at low point of before and go down
    while (found==0) && here>1 % look for upstroke start
        here = here -1;
        if diffvoltageTrace(here)< upstrokeSpeed_est*slopeCutPercent/100, found=1; spikeStartIndex = here; end
    end
    if (found==0), errorHappened=1;  end % no good
     
    found=0; here = downstrokeLowIndex_est; % we start at low point of before and go down
    while (found==0) && here<length(voltageTrace)-2 % look for downstroke end
        here = here +1;
        if diffvoltageTrace(here)> downstrokeSpeed_est*slopeCutPercent/100, found=1; spikeStopIndex = here;  end
    end
    if (found==0), errorHappened=1;  end % no good
    
    if errorHappened==1, fprintf(1,'%s\n','WARNING: ERROR IN SPIKE DETECTION - putting to 0.'); amplitudes(i)=0;  upStrokeSpeeds(i)=0; upstrokeFit(i)=0; downStrokeSpeeds(i)=0; downstrokeFit(i)=0; halfwidths(i)=0; startTimes(i)=0; peakTimes(i)=0; stopTimesend(i)=0; return; end
    
    % > now spike amplitude is taken from spike onset point to maximum, slope
    % (upstroke, downstroke) is taken from lowSlopeCut to highSlopeCut % of
    % this amplitude, by linear fit.
    amplitudes(i) = abs( voltageTrace(spikeStartIndex) - voltageTrace(peakIndexes(i)) );
    startTimes(i) = timeTrace(spikeStartIndex);
    peakTimes(i) =  timeTrace(peakIndexes(i));
    stopTimes(i) =  timeTrace(spikeStopIndex);
    
    % upstroke slope
    slopeIndexes_up = find (   (voltageTrace(spikeStartIndex:peakIndexes(i)) >= voltageTrace(spikeStartIndex)+amplitudes(i)*lowSlopeCut/100) & ...
        (voltageTrace(spikeStartIndex:peakIndexes(i)) <= voltageTrace(spikeStartIndex)+amplitudes(i)*highSlopeCut/100) );
    slopeIndexes_up = slopeIndexes_up + spikeStartIndex;
    if length(slopeIndexes_up)<3, slopeIndexes_up = [slopeIndexes_up(1)-1  slopeIndexes_up  slopeIndexes_up(end)+1]; end
    s = fitoptions('Method','NonlinearLeastSquares',...
        'Lower',     [voltageTrace(spikeStartIndex)-10    -Inf    0.99*timeTrace(peakIndexes(i))     ],...
        'Upper',     [voltageTrace(peakIndexes(i))         Inf    1.1*timeTrace(peakIndexes(i))     ],...
        'Startpoint',[voltageTrace(spikeStartIndex)         0     timeTrace(peakIndexes(i))     ]);
    %                                 a0                     a1                a2   
    syms a0 a1 x;
    equation = 'a0+a1*(x-a2)';
    ff = fittype(equation, 'options',s);
    [fitresults_upstroke, details_upstroke] = fit( timeTrace(slopeIndexes_up)',  voltageTrace(slopeIndexes_up)', ff);
    upStrokeSpeeds(i) = fitresults_upstroke.a1;
    upstrokeFit=fitresults_upstroke;
    
 %   fig=figure(66666); plot(timeTrace(slopeIndexes_up),voltageTrace(slopeIndexes_up),'gd'); hold on;
        
    % downstroke slope
    slopeIndexes_dn = find (   (voltageTrace(peakIndexes(i):spikeStopIndex) >= voltageTrace(spikeStopIndex)+amplitudes(i)*lowSlopeCut/100) & ...
        (voltageTrace(peakIndexes(i):spikeStopIndex) <= voltageTrace(spikeStopIndex)+amplitudes(i)*highSlopeCut/100) );
    slopeIndexes_dn = slopeIndexes_dn + peakIndexes(i);
    if length(slopeIndexes_dn)<3, slopeIndexes_dn = [slopeIndexes_dn(1)-1  slopeIndexes_dn  slopeIndexes_dn(end)+1]; end
    s = fitoptions('Method','NonlinearLeastSquares',...
        'Lower',     [voltageTrace(spikeStopIndex)-10        -Inf    0.99*timeTrace(peakIndexes(i))     ],...
        'Upper',     [voltageTrace(peakIndexes(i))+10         Inf    1.1*timeTrace(peakIndexes(i))     ],...
        'Startpoint',[voltageTrace(peakIndexes(i))             0     timeTrace(peakIndexes(i))     ]);
%     %                                 a0                     a1                a2    

    syms a0 a1 x;
    equation = 'a0+a1*(x-a2)';
    ff = fittype(equation, 'options',s);
    [fitresults_downstroke, details_downstroke] = fit( timeTrace(slopeIndexes_dn)',  voltageTrace(slopeIndexes_dn)', ff);
    downStrokeSpeeds(i) = fitresults_downstroke.a1;
    downstrokeFit = fitresults_downstroke;
    
 %   fig=figure(66666); plot(timeTrace(slopeIndexes_dn),voltageTrace(slopeIndexes_dn),'bd');
    
    % halfwidth
    halfvalue = voltageTrace(spikeStartIndex)+amplitudes(i)/2;
    indx1 = find( voltageTrace(spikeStartIndex:peakIndexes(i)) <=  halfvalue);
    indx1 = indx1(end) + spikeStartIndex;
    indx2 = find( voltageTrace(peakIndexes(i):spikeStopIndex) <=  halfvalue);
    indx2 = indx2(1) + peakIndexes(i);
    halfwidths(i) = mean(timeTrace(indx2-1:indx2+1))  - mean( timeTrace(indx1-1:indx1+1) );
    
    % plot if needed
    if plotting==1 
        col= rand(1,3);
        fig=figure(66666); 
        hold on;
        plot(timeTrace(spikeStartIndex:spikeStopIndex), voltageTrace(spikeStartIndex:spikeStopIndex),'d','Color',col);
        
        plot(timeTrace(spikeStartIndex:peakIndexes(i)), upstrokeFit(timeTrace(spikeStartIndex:peakIndexes(i))),'--','Color',col);
  
        plot(timeTrace(peakIndexes(i):spikeStopIndex), downstrokeFit(timeTrace(peakIndexes(i):spikeStopIndex)),'--','Color',col);
        
        tmpt = mean(timeTrace(indx2-1:indx2+1));
        tmpv = mean(voltageTrace(indx2-1:indx2+1));
        plot([tmpt tmpt-halfwidths(i)], [tmpv tmpv],'--', 'Color',col);
         
    end
    
end


end
