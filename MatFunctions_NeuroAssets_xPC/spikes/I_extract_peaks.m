function out = I_extract_peaks(data, threshold, varargin)
% peaks_indexes = I_extract_peaks(data_waveform, threshold)
%
% note: this is based on (positive slope) threshold crossing detections..
%
% Simplified version - this extracts the +peaks, returning just the indexes
% corresponding to those maxima which are between two successive threshold
% crossings..
%
%varargin - to be provided as pairs of 'name',value
% -- ignore : crossings lasting (having) this or less data points, are ignored. 
%
% Aug 22nd 2007 - Michele Giugliano
% Modified by Istvan Biro, 2011.03.22.
% version 2
%

N    = length(data);        % Number of points in the data waveform..
bool = 0;                   % Useful boolean variable for threshold-cross
out  = [];                  % Vector containing first the indexes of 
                            % threshold crossings and later of the peaks..
back = []; % return below threshold
ignore = 0;

% evaluating varargin; (Name, value) pairs, as ('plotFitHandle', 67)
 % expected
 noVars = length(varargin);
 for i=1:2:noVars
    if isnumeric(varargin{i+1})
        if (isscalar(varargin{i+1})), commandName=sprintf('%s%s%f%s',varargin{i},'=',varargin{i+1},';');
        else commandName=sprintf('%s%s%s%s',varargin{i},'=',mat2str(varargin{i+1}),';');
        end
    else commandName=sprintf('%s%s%s%s',varargin{i},'=''',varargin{i+1},''';');
    end
    eval(commandName);
 end

for i=1:N,                  % Detection of threshold crossings..
 if ((data(i) > threshold) && (~bool)), bool = 1; out = [out, i]; end % if
 if ((data(i) < threshold) && (bool)),  bool = 0; back=[back, i]; end % if
end % for i

if length(out)==length(back)+1, back=[back, 1]; end % if the last back-crossing was missed, we just put this to data(1) so it wi�� be ognored below as noise (back(k)-out(k)>ignore)


M    = length(out);         % Number of (positive slope) threshold crossing
peak = [];                  % Temporary data structure..

for k=1:M,                  
 istart = out(k);
 if (k == M), istop = N; else istop = out(k+1)-1; end; % (k+1)-1 : -1 needed not to confuse the second peak with the first in case of low sampling signals
 chunk  = data(istart:istop);   % An index interval is set for each event..
 if back(k)-out(k)>ignore % ignoring too short signals (noise)
    tmq    = find(chunk == max(chunk));
    peak   = [peak, (istart + tmq(1) - 1)];
 end % if
end % for

out = peak;            % Indexes corresponding to peaks only are returned.
