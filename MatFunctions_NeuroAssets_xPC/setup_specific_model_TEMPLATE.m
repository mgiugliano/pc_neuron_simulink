function tg = setup_test_streaming(varargin)
% sets up model "test_streaming.mdl"

%% 1) parameters
myTargetPcName = 'myOneAndOnlyXPC';
myModelName = 'test_streaming';
runDuration = 10;
myStepSize = 1./20000;

compileFlag = 0;
useTargetFileSystem = 1;
logToMemoryFlag = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%% EVALUATE VARAGIN %%%%%%%%%%%%%%%%%%%
% evaluating varargin; (Name, value) pairs, as ('plotFitHandle', 67)
% expected
% This allows to overwrite the settings defined above this level, from
% parameters passed to the function as arguments, for e.g. setting it to
% compile, using the additional arguments: 'compileFlag', 1.
noVars = length(varargin);
for i=1:2:noVars
    if isnumeric(varargin{i+1})
        if (isscalar(varargin{i+1})), commandName=sprintf('%s%s%f%s',varargin{i},'=',varargin{i+1},';');
        else commandName=sprintf('%s%s%s%s',varargin{i},'=',mat2str(varargin{i+1}),';');
        end
    else commandName=sprintf('%s%s%s%s',varargin{i},'=''',varargin{i+1},''';');
    end
    eval(commandName);
end


%%%% OPEN MODEL
open_system(myModelName);



%% 2) run general setup
% This will set up most commonly used settings for running the model on
% xPC. This function (setup_general_model_settings_for_xPC) should be
% called at least once, after model construction, to set up the right
% compiler and other options for xPC.
if compileFlag~=0 || ~exist(fullfile(pwd, [myModelName '.dlm']),'file')
    tg = setup_general_model_settings_for_xPC(myTargetPcName , myModelName, runDuration, myStepSize);
else
    tg = xpctarget.xpc(myTargetPcName);
end


%% 3) set other model parameters, if needed (this is on HOST level, before save and compile !!)
% using set_param( myModelName/mySubSystemname/.../blockName , 'PropertyName', PropertyValue );
% NOTE: if you change model parameters here, using this command, you MUST
% compile the model (see step 4) ) again before downloading (next steps). If you want to
% avoid this, set parameters AFTER downloading (see step 6) ). Any model
% needs to be compiled once, but if no major changes to it are done (major
% changes are those which mofify more than the values of its parameters),
% you can just download it and than set parameter values, avoiding the
% lengthy compilation time.



% Set up memory for logging data to memory, if that is require. Do so by
% running here:
% setUpMenoryLoggingFromTopOutports(myModelName, runDuration, myStepSize);
% Note: siganls logged will be those, which connect to an "OUT" port on the
% top level of the model (not in a subsystem but on highest level). See
% step 8) on how to retrieve data after the run (data = tg.OutputLog;).
% NOTE: if you did not log to memory from this model before and just
% changed this option, make sure to set compileFlag=1, as this change
% requires re-compilation.
if logToMemoryFlag==1
    setUpMenoryLoggingFromTopOutports(myModelName, runDuration, myStepSize);
end



%% 4) compile if necessary
% NOTE: see comments at 3)  !!!
% this takes long time
if compileFlag~=0 || ~exist(fullfile(pwd, [myModelName '.dlm']),'file')
    compile_general_model_for_xPC(myModelName);
end


%% 5) download model and files to xPC
% NOTE: files from which you'd like to tream can be put on xPC like this:
if useTargetFileSystem~=0, [driveLetterFound, tgfs] = setup_fileSystem_xPC(tg, ''); end  % set up tardet hard disk if needed, choosing the correct drive.

% download files if needed
% datavalues = [ sin(1.*t); sin(2.*t); sin(3.*t); sin(4.*t); sin(5.*t) ]; -- this will give 5 parallel output signals
% xpcbytes2file('file.dat', datavalues);
% myftp = tg.ftp;
% myftp.put(currentAddFile);
% You can also write to xPC file system directly (faster). For this use
% [driveLetterFound, tgfs] = setup_fileSystem_xPC(tg, ''); to get tgfs (IF
% NOT DONE SO PREVIOUSLY!! ) and than:
% xpcbytes2file(currentAddFile, datavalues); % makes binary file on host, in special xPC format
% fh = fopen('input.dat','r');
% ddataa = fread(fh);
% fclose(fh);   % data now read
% h = tgfs.fopen(currentAddFile, 'w');
% fwrite(tgfs, h, ddataa);
% tgfs.fclose(h);

% download model
download_general_model_for_xPC(tg, myModelName);



%% 6) set specific values IN THE DOWNLOADED TARGET MODEL (directly in xPC)
% using tg.setparam(tg.getparamid( 'mySubSystemname/.../blockName', 'PropertyName') ,PropertyValue);
% Since this affects the downloaded model, no recompilation is necessary at
% this step.

% set sample time
tg.SampleTime = myStepSize;
% tg.setparam(tg.getparamid( 'ModelParaneters/myStepSize', 'Value'),  myStepSize );  % use this if you have a constant block in the model,
% outputing the sample time.


%% 7) Do what is necessary for starting the model and handle data/parameters/feedback/... DURING its running time
% Note: you can check if a model is running by strcmp(tg.Status,'running')
% and access values of signals too using tg.xxxxx
% NOTE: start model by tg.start;
% NOTE: wait for model to finish like this:
% while strcmp(tg.Status,'running')
%     pause(0.2);
% end

tg.set('StopTime',runDuration); % total run time
tg.start;
while strcmp(tg.Status,'running')
    pause(0.2);
end


%% 8) Do what is necessary after model finished running

% NOTE: if you recorded into a file, get it like this:
% xpcftp=tg.ftp;
% try
%     xpcftp.get('xPCdata.dat');
% catch err
%     xpcftp.get('xPCdata.dat');
% end
% xpcFileData = readxpcfile('xPCdata.dat'); % Convert the data

% You can also read from xPC file system directly (faster). For this use
% [driveLetterFound, tgfs] = setup_fileSystem_xPC(tg, ''); to get tgfs (IF
% NOT DONE SO PREVIOUSLY!! ) and than:
% h = tgfs.fopen('xPCdata.dat');
% data = tgfs.fread(h);
% tgfs.fclose(h);
% xpcFileData  = readxpcfile(data);
% tgfs.removefile('xPCdata.dat');

% NOTE: if you recorded data to xPC memory with top level (highest level in model diagram) OUTPORT elements, get
% it like this:
% data = tg.OutputLog;

% Do not forget to save the data to the file format of your choice.



end