function STG_Close(DeviceID)
% close connection and unload libraries

% Istvan Biro, 2012.03.02.


res = calllib('McsUsbDLL', 'McsUsb_Disconnect', DeviceID);
unloadlibrary('McsUsbDLL');

end % function
