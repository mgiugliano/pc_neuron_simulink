function problemHappened = STG_UploadData(DeviceID, stimTimeValuesSTG, stimDataValuesSTG, controlMode)
% uploads data to the initiated STG. Here you ONLY UPLOAD DATA to an
% INITIATED device.

% Uploads the time and data in stimTimeValuesSTG{i} and
% stimDataValuesSTG{i} to channel i of te STG. When STG is triggered,
% consecutive values of stimDataValuesSTG{i}(j) are set and held for the
% DURATION specified by the corresponding(j)  stimTimeValuesSTG{i}(j).

% controlMode = 0 : CURRENT CONTROL (data is current)
%               1 : VOLTAGE CONTROL (data is voltage)

% Units: stimTimeValuesSTG : [s] ;; stimDataValuesSTG = [uA] OR [mV]

% Use DeviceID=STG_Prepare(serial) ONCE before, to
% initiate device - note: there is where you program the behavior of your device.

% Do not forget to terminate connections with
% STG_Close(DeviceID).

% Istvan Biro, 2012.03.02.

problemHappened = 0; % so far all is ok

  for i=1:length(stimDataValuesSTG) % for all channels, meaning all data arrays
      
      % converting data and time to right units
      myTime = stimTimeValuesSTG{i}*1e6;  % [s] -> [us]
      if controlMode ==0 , myData = stimDataValuesSTG{i}.*10;   end % [uA] -> [1/10uA]; 
      if controlMode ==1 , myData = stimDataValuesSTG{i};       end % [mV] -> [mV]; 
      
      % conversion for sign and DAC and setting data type required
      zero_indexes = find(myData==0); % because sing(0)=0 these must be owerwritten after conversion (incorrect conversion for these points)
      myData = sign(myData).*fix(myData./1.953) - 2048.*(sign(myData)-1);
      myData(zero_indexes) = 0;

      % get array from cell data and set them in array, by pionter
      %making sure the last value is 0 by adding one 20us long 0 stimulus
      ptime = libpointer('uint32Ptr', uint32([ myTime 20]) ); 
      pdata = libpointer('uint16Ptr', uint16([ myData 0]) );
      % uploading data
      nr_points=length(ptime.val);
          % printout for testing
      %if (verbose==1),    fprintf(1,'DeviceID: %s uploading channel %d - datapoints=%d. \n', DeviceID, i, nr_points); end
      res = calllib('McsUsbDLL', 'STG200x_ClearChannelData', DeviceID, i-1);
      res = calllib('McsUsbDLL', 'STG200x_SendChannelData32', DeviceID, i-1, pdata, ptime, nr_points);
      if res~=0, problemHappened=1; end % detect if somewhere something went wrong
      clear pdata
      clear ptime
  end % for


end % function
