function [state, allStimuli, allSlidingEPSPsizes, allTimes , TET,dt, tg] = setup_EPSPsizeClamp_old(runDuration,varargin)
% This function sets up the model for response EPSP size clamping :
% EPSPsizeClamp.mdl . Here parameters and timing is defined.

% *** runDuration : time in [s] for which the model will run
% *** PlaybackModeSTGvalues : 
%      -- if empty [] , than EPSP clamp is performed on target
%      firing probability targetEPSPsize
%      -- if values are provided, NO EPSP clamp is performed, but
%      these stimulus values are applied, in the provided order (but the
%      total run time will still be limited to runDuration : last ones might be skipped).

% Note : to change parameters of this function from outside, at call time,
% provide additional parameters, in the form like, for e.g.:
% ...=setup_EPSPsizeClamp(15, 'PlaybackModeSTGvalues', [your_stimulus_values], ...) ;

% Istvan Biro, 
% version 1
% 2012.02.28. 

% 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%% MODEL SPECIFIC DATA %%%%%%%%%%%%%%%%
% MODEL
myModelName = 'EPSPsizeClamp';   % this is the name of the uncompiled(!) .mdl Simulink model (and also of the Target Application) - this is assigned to our Target Object (step 2))
                          % the model is compiled (see command in step 2))
                          % the model file must be in the same (or working) directory.
compileFlag = 1;                  % compile model before loading? 1=yes, 0=no. WARNING : only parameters are updated. For deep change, recompile it!

getDataNow = 0;

kernelfile = '';
kernelColumn = 3;

destinationFolder = 'EPSPsizeClamp';
fileIndexOffset = 0;   % when recording traces in the xPC, the files are named with increasing numbers. This offsets the starting value to this. 

amplifierResolutionFile = '../03 AMPLIFIER STATE/ampRes.m';

SignalLoggingFlag = 0;            % LOG SIGNALS? 1=yes,0=no. If 0, NO SIGNALS ARE LOGGED AT ALL! (RECCOMMENDED : 1)
signalNumber      =  0;         % how many signals (except time, TET, state) will you record? (i.e. how many Outport blocks you have built in)? 
                                  % - needed to calculate loging space needs (see totalPointsStore). If you set this too low (less than the Out Ports / logged signals / (sink) you actually built in the model), the total number is divided to all signals evenly, keeping only the last part (of the run) of the signals, which fit this memory. 

%runDuration = 20.2%10*60;   % [s] total run time
myStepSize = 1/20e3;             % time-step size in (s). 
startTime = 0;                    % time zero of execution start
% if ~ exist('runDuration','var'),    runDuration = 20;     end            % duration of the run in (s). 

modelResetTimes = [0];            % [s] times to reset the model. e.g. before running it again, even if it was not recompiled. Needed for correct starting conditions and values.

% DATA LOGGING - before and after TTL initiating the STG
logTimeBefore = 10/1000;     % [s] LOG time before TTL out ONSET
logTimeAfter = 500/1000;     % [s] LOG time after TTL out ONSET


% STIMULUS TRIGGERING TO STG
InterstimInterval = 3;       % [s] interstimulus interval
TTLlevel = 1;                % 1 : high TTL
TTLLength = 25*1e-6 ;       % [s] length of TTL pulse to send out

% STIMULUS VALUES TO STG
STG_serial = '9058-0163';     % serial of the STG to use
stimulusLength = 100*1e-6; % [s] duration that a stimulus amplitude is held (per phase -- i.e. total duration is 2x this) . MUST be multiple of 20us
stimulusZeroAfterLength = 20*1e-6; % [s] duration of enforcing 0 stimulus after the pulse of (stimulusAmplitudes(x) for stimulusLength) and (-stimulusAmplitudes(x) for stimulusLength)
                                   % resulting total run time of stimulator of (2*stimulusLength + stimulusZeroAfterLength).
                                   % NOTE: stimulator times must be multiples of 20us. 
controlMode = 0;              % 0: use current driven stimulus, 1: use voltage driven stimulus
firstSTGstimulusValue = 300;   % [uA] OR [mV] fist STG stimulus value (to start from).
LIMITSTGstimulusValue = 700;   % [uA] OR [mV] maximal possible STG stimulus value (to start from).

PlaybackModeSTGvalues = [];    % * for probability clamping, leave this EMPTY
                               % * if you enter values, these stimuli will
                               % be applied, probability clamp will NOT be done

% PROBABILITY CLAMP
P_stg = 1;                 % P of a PID controller
I_stg = 0.3;                 % I of a PID controller
EPSPToStimAmpltude = 100;   % "gain", i.e. conversion of Probability-->nextSTGstimulus calculation
targetEPSP = 9;   % [mV] EPSP size to keep
% tau_history = 200;        % [s] decay time constant of history
tau_history = 10;        % [s] decay time constant of history



% EPSP DETECTION 
EPSPWindow = logTimeAfter;  % [s] time window after STG stimulus to detect spike
blankingTime = 1/1000;            % [s] blanking time after STG stimulus (no detection)


% VOLTAGE HOLDING - apply current to hold voltage at vHold, except at
% stimulus and recording events - done by PI controller
enableVoltageHold = 0;      % 1 or 0 for enabling or disabling feature
vHold = -50;                % [mV] voltage to hold by current injection
proportionalFactor = 10;     % PI controller settings
integratorFactor = 100;       % PI controller settings
currentLimit = 1000;         % +-[pA] this is the maximal current to be used for compensation


% INTRACELLULAR STIMULUS ADDITION
% here you can do pulses, or to do it generally, overwrite intracellTimes
% and intracellValues below, for onset-duration pairs... (NO VARARGIN on these 2).
% Note: this is only performed during duration of run, set by extracellular
% stimulus : put 0 extracellular stimulus to only do intracellular.
enableIntracellStim = 0;      % 1 or 0 to enable or disable feature 
pulseLength = 10*myStepSize;     % [s] duration of a pulse : must be multiple of myStepSize.
pulseAmplitudes =   [10 0]  ;  % [pA] amplitudes of a pulse. WILL BE PLAYED IN THIS ORDER, on trigger signal (STG), one at a time. After the last, it will be 0.
pulseOffsetTime = 0 ;         % [s] pulse on osset relative to time 0, that is, just after logTimeBefore, OR (=) just before logTimeAfter OR (=) from the onset of the TTL trigger going to STG.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%% EVALUATE VARAGIN %%%%%%%%%%%%%%%%%%%
% evaluating varargin; (Name, value) pairs, as ('plotFitHandle', 67)
% expected
noVars = length(varargin);
for i=1:2:noVars
    if isnumeric(varargin{i+1})
        if (isscalar(varargin{i+1})), commandName=sprintf('%s%s%f%s',varargin{i},'=',varargin{i+1},';');
        else commandName=sprintf('%s%s%s%s',varargin{i},'=',mat2str(varargin{i+1}),';');
        end
    else commandName=sprintf('%s%s%s%s',varargin{i},'=''',varargin{i+1},''';');
    end
    eval(commandName);
end



% GENERATE VALUES

% store units
if controlMode==0, myunits = 'uA';
else myunits = 'mV'; end

% add path of DLL
addpath('C:\Program Files\Multi Channel Systems\MC_Stimulus II');

% number of stimulus events to do
eventNumber = floor(runDuration/InterstimInterval);

% add kernel information
if ( ~isempty(kernelfile) && exist(kernelfile,'file') )
    kerneldataaa = load(kernelfile);
    Kernel = kerneldataaa(:,kernelColumn);          % Kernel of the electrode, for convolution
    Kernel = Kernel';
else
    kernelfile = '';
    Kernel = zeros(100,1);
end


% make coordinator switcher-signal : at these time values will switch to stimulus
% and record mode for a duration of logTimeBefore+logTimeAfter
switchOnTimes = [InterstimInterval:InterstimInterval:eventNumber*InterstimInterval];
switchOnTimes = [switchOnTimes runDuration*9*ones(1,1000-length(switchOnTimes))]; % just fill up to have size 1000 all the time (pass as parameter, not recompile)
switchOnDuration = logTimeBefore + logTimeAfter; % time for which ONE stimulus and recording lasts
% make triggering 
triggerOnTimes = switchOnTimes + logTimeBefore; % basically logTimeBefore after turning on the recording
triggerDuration = TTLLength; % time for which trigger is on level triggerLevel
triggerLevel = TTLlevel;
% calculate times when STG program needs to be updated
STGtotalStreamingTime = 2*stimulusLength+stimulusZeroAfterLength;
STGUpdateTimes =  [0 triggerOnTimes] + EPSPWindow  + blankingTime + max([10.*myStepSize 0.01*InterstimInterval]);  % ; just after the spike detection


% Generate intracellular stimulus addition
if enableIntracellStim == 0
    intracellTimes = [0 Inf];    % [s] times : current value intracellValues(i) is put on at time intracellTimes(i) and held on until intracellTimes(i+1).
    intracellValues = [0 0];   % [pA] current values to be applied at time intracellTimes(i) and held until the next value
else
    for i = 0:length(triggerOnTimes)-1
        intracellTimes(2*i+1) = triggerOnTimes(i+1) + pulseOffsetTime;                  % on
        if i+1<=length(pulseAmplitudes) , intracellValues(2*i+1) = pulseAmplitudes(i+1);
        else  intracellValues(2*i+1) = 0;
        end
        intracellTimes(2*i+2) = triggerOnTimes(i+1) + pulseOffsetTime + pulseLength;    % off
        intracellValues(2*i+2) = 0;
    end
end

intracellTimes = [intracellTimes runDuration*9*ones(1,1100-length(intracellTimes))]; % just fill up to have size 1100 all the time
intracellValues = [intracellValues zeros(1,1100-length(intracellValues))]; % just fill up to have size 1100 all the time

% initiate stimulus generator (ONCE!)
[DeviceID] = STG_Prepare(STG_serial);


% get time for the filename


                                  
if controlMode==0, STG_voltageToStimSignal = 100;  % if current control mode, so I+ GND is fed into MEA and U+ GND is fed into xPC for control, this is the conversion rate to record actual stimulus value in [uA]  
else STG_voltageToStimSignal = 1000;  % if voltage control mode, so U+ GND is fed into MEA and U+ GND is fed into xPC for control, this is the conversion rate to record actual stimulus value in [mV]  
end

%%%%%%%%%%%%%%%%%%%%%%%% END MODEL SPECIFIC DATA %%%%%%%%%%%%





% /////////////////////////////////////////////////////////////
% /////////////////////////////////////////////////////////////
% /////////////////////////////////////////////////////////////
%  FROM HERE ON ALL SHOULD STAY AS IT IS unless dramatic changes 
% are needed.
% /////////////////////////////////////////////////////////////
% /////////////////////////////////////////////////////////////
% /////////////////////////////////////////////////////////////




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%% PARAMETERS %%%%%%%%%%%%%%%%%%%%%%%%%
% here you can set the parameters which are most often modified

% AMPLIFIER RESOLUTION VALUES
run(amplifierResolutionFile);

% COMMUNICATION
myTargetPcName = 'myOneAndOnlyXPC';     % just a name for your Target PC Environment Object
myIP = '143.168.1.2';       % the IP of the target machine you want to use
myGate = '143.168.1.1';   % the TCP/IP Gateway address
myMask = '255.255.0.0';       % subnet mask address
myPort = '22222';             % communication port


                          
% LOGGING OF SIGNALS - please put the right OUTPORT blocks in the model first
if SignalLoggingFlag == 1
    TETlogFlag = 1;                   % 1=on OR 0=off : set enabling or disabling of TET logging (note: this is regarded as a signal too -memory allocation...)
    % what to log
    SaveFinalStateFlag	=  0;         % log final state? 1=on 0=off
    SaveStateFlag		=  1;         % log data at state? 1=on 0=off
    SaveTimeFlag		=  0;         % log time? 1=on 0=off
    SaveOutputFlag		=  1;         % log data at Outports (each a column)? 1=on 0=off
    
    
    
    DSMLoggingFlag = 1;               % 1=on OR 0=off
    SignalLoggingSaveFormat = 'ModelDataLogs';  % to log from outport (?) - leave it like this
    SignalLoggingName = 'logsout';    % logging name - leave as it is.
    DSMLoggingName	  ='dsmout';      % logging name - leave as it is.
else
    TETlogFlag = 0;                   % 1=on OR 0=off : set enabling or disabling of TET logging (note: this is regarded as a signal too -memory allocation...)
    SaveFinalStateFlag	=  0;         % log final state? 1=on 0=off
    SaveStateFlag		=  0;         % log data at state? 1=on 0=off
    SaveTimeFlag		=  0;         % log time? 1=on 0=off
    SaveOutputFlag		=  0;         % log data at Outports (each a column)? 1=on 0=off
    DSMLoggingFlag = 0;               % 1=on OR 0=off
    SignalLoggingName = 'logsout';    % logging name - leave as it is.
    DSMLoggingName	  ='dsmout';      % logging name - leave as it is.
    SignalLoggingSaveFormat = 'ModelDataLogs';  % to log from outport (?) - leave it like this
end
%%%%%% calculating consequences and needs rising from your requests... (DO NOT MODIFY)
if runDuration==inf, RD = 200; % record 200 seconds then overwrite
else RD = runDuration; end
totalPointsStore = 10000 + (TETlogFlag + SaveStateFlag + SaveTimeFlag + signalNumber)*RD/myStepSize;  
                        % space for TET, time, state and "signalNumber" variables (SaveOutput) - during entire run    
                           % this is the total number of points (values) to be logged. Thus if you log time and one data (via one outport), thus 2 things, each will have its last totalPointsStore/2 points stored.
                           % 10000 for security...
                           % do not excede memory!
if TETlogFlag==0, TETlogSting = 'off'; else TETlogSting = 'on'; end
if SignalLoggingFlag==0, SignalLoggingString = 'off'; else SignalLoggingString = 'on'; end
if DSMLoggingFlag==0, DSMLogging = 'off'; else DSMLogging = 'on'; end
if SaveFinalStateFlag==0, SaveFinalState = 'off'; else SaveFinalState = 'on'; end
if SaveOutputFlag==0, SaveOutput = 'off'; else SaveOutput = 'on'; end
if SaveStateFlag==0, SaveState = 'off'; else SaveState = 'on'; end
if SaveTimeFlag==0, SaveTime = 'off'; else SaveTime = 'on'; end




% COMPILER SETTINGS - only modify if you are familiar with it
SystemTargetFile	=  'xpctarget.tlc';  % ESSENTIAL - otherwise code generated is not suitable for xPC.
GenCodeOnly		    =  'off';            % to also have dlm executable (?)
MakeCommand		    = 'make_rtw';        % compiler command - leave it so
GenerateMakefile	=  'on';             % to also have dlm executable (?)
TemplateMakefile	=  'xpc_default_tmf';% ...
Description		    = 'xPC Target';      % ...
RTWCompilerOptimization = 'On';          % optimized code for faster run  

% SOLVER - set the most important solver parameters. The solver evolves the
% model in time. 
Solver		=  'FixedStepDiscrete';   % the solver engine type. 
                                      %'FixedStepDiscrete' is a discrete
                                      %step-taker with a fixed step size of
                                      %myStepSize. Continuous ones also
                                      %exist (as ode4), and others...
                                      
%%%%%%%%%%%%%%%%%%%%%%%% now we set them...      %%%%%%%%%%%%
% set model parameters (those which you want to be sure of!)
fprintf(1,'%s%s%s\n',' +++++ opening model ',myModelName,' & setting parameters...');
open(myModelName);                                 % open model to be able to set parameters
set_param(myModelName,'StartTime',num2str(startTime)); % time zero
set_param(myModelName,'Solver',Solver); % set Solover 
set_param(myModelName,'FixedStep',num2str(myStepSize)); % step size
set_param(myModelName,'StopTime',num2str(startTime+runDuration)); % step size
set_param(myModelName,'DSMLogging',DSMLogging); % to workspace logging
set_param(myModelName,'DSMLoggingName',DSMLoggingName); % to workspace logging
set_param(myModelName,'SaveFinalState',SaveFinalState); % to workspace logging
set_param(myModelName,'SaveOutput',SaveOutput); % to workspace logging
set_param(myModelName,'SaveState',SaveState); % to workspace logging
set_param(myModelName,'SaveTime',SaveTime); % to workspace logging
set_param(myModelName,'SignalLoggingName',SignalLoggingName); % logging name.
set_param(myModelName,'SignalLoggingSaveFormat',SignalLoggingSaveFormat); % to save from.
set_param(myModelName,'SignalLogging',SignalLoggingString); % enable or disable signal logging
set_param(myModelName,'RL32LogTETModifier',TETlogSting); % set enabling or disabling of TET logging
set_param(myModelName,'RL32LogBufSizeModifier',num2str(totalPointsStore)); % set logging memory (no of doubles) for all signals logged 
set_param(myModelName,'SystemTargetFile',SystemTargetFile);
set_param(myModelName,'GenCodeOnly',GenCodeOnly);
set_param(myModelName,'MakeCommand',MakeCommand);
set_param(myModelName,'GenerateMakefile',GenerateMakefile);
set_param(myModelName,'TemplateMakefile',TemplateMakefile);
set_param(myModelName,'Description',Description);
set_param(myModelName,'RTWCompilerOptimization',RTWCompilerOptimization);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%% 1) target object and parameters %%%%%%%%

 % get the targets, from which one can tell the current one... or create a new one
tgs = xpctarget.targets; % tgs is a "target object collection environment container"

 % look if a Target Object Environment named myTargetPcName exists...
 allNames = tgs.getTargetNames; % all Target Object Environment names
 ii = 0; found = 0; 
 while found==0 && ii<length(allNames)  % search...
     ii = ii + 1;
     if strcmp(allNames{ii},myTargetPcName), found=1; end
 end
 if found == 0 % if myTargetPcName does not yet exist, we add a new
     tge = tgs.Add(myTargetPcName);
 else % if it already exists, we select it
     tge = tgs.Item(myTargetPcName);
 end
 % now tge is the  Target Object Environment that we want to set (and use it for our Target Object)
     
 
 % setting environment parameters for our Target
    % Name already set (myTargetPcName)                                
    tge.TargetRAMSizeMB          = 'Auto';                         
    tge.MaxModelSize             = '1MB';                           
    tge.SecondaryIDE             = 'off';                           
    tge.NonPentiumSupport        = 'off';                           
    tge.MulticoreSupport         = 'off';                           
    tge.HostTargetComm           = 'TcpIp';                         
    tge.TcpIpTargetAddress       = myIP;                 
    tge.TcpIpTargetPort          = myPort;                         
    tge.TcpIpSubNetMask          = myMask;                 
    tge.TcpIpGateway             = myGate;               
    tge.RS232HostPort            = 'COM1';                          
    tge.RS232Baudrate            = '115200';                        
    tge.TcpIpTargetDriver        = 'Auto';                          
    tge.TcpIpTargetBusType       = 'PCI';                           
    tge.TcpIpTargetISAMemPort    = '0x300';                         
    tge.TcpIpTargetISAIRQ        = '5';                             
    tge.TargetScope              = 'Enabled';                       
    tge.TargetBoot               = 'BootFloppy'; 
 
 % the actual Target Objext is then (corresponding to tge)
tg = xpctarget.xpc(myTargetPcName); % tg is now the effective Target Object (representing the Target PC)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% 3) compile model and download it %%%%%%%%%%%%

% here is the compilation command of the model: mdl-->dlm
% adding the .dlm file now to our Target Object (preresenting the Target PC)
set_param(myModelName,'xPCisDownloadable','off');  % Do not download automatically after building.
set_param(myModelName,'RTWVerbose','off');         % Configure for a non-Verbose build. Warnings on though.

% make sure model is loaded or complile and load it
if exist(fullfile(pwd, [myModelName '.dlm']),'file'), tg.load(myModelName); 
else save_system(myModelName); rtwbuild(myModelName); tg.load(myModelName);
end


set_param('EPSPsizeClamp/intracellularStimGenerator/times', 'Value', ['[' num2str(intracellTimes) ']']);
id = getparamid(tg, 'intracellularStimGenerator/times', 'Value'); if ( length( tg.getparam(id) )  ~= length(intracellTimes) ),  compileFlag=1;   end

set_param('EPSPsizeClamp/intracellularStimGenerator/levels', 'Value', ['[' num2str(intracellValues) ']']);
id = getparamid(tg, 'intracellularStimGenerator/levels', 'Value'); if ( length( tg.getparam(id) )  ~= length(intracellValues) ),  compileFlag=1;   end

set_param('EPSPsizeClamp/STGTriggerGenerator/triggerOnTimes', 'Value', ['[' num2str(triggerOnTimes) ']']);
id = getparamid(tg, 'STGTriggerGenerator/triggerOnTimes', 'Value'); if ( length( tg.getparam(id) )  ~= length(triggerOnTimes) ),  compileFlag=1;   end

set_param('EPSPsizeClamp/STGTriggerGenerator/triggerDuration', 'Value', ['[' num2str(triggerDuration) ']']);

set_param('EPSPsizeClamp/STGTriggerGenerator/triggerLevel', 'Value', ['[' num2str(triggerLevel) ']']);

set_param('EPSPsizeClamp/recordTriggerGenerator/switchOnTimes', 'Value', ['[' num2str(switchOnTimes) ']']);
id = getparamid(tg, 'recordTriggerGenerator/switchOnTimes', 'Value'); if ( length( tg.getparam(id) )  ~= length(switchOnTimes) ),  compileFlag=1;   end

set_param('EPSPsizeClamp/recordTriggerGenerator/switchOnDuration', 'Value', ['[' num2str(switchOnDuration) ']']);

set_param('EPSPsizeClamp/Input//Output_model_related/modelResetTimes_inputParameter', 'Value', ['[' num2str(modelResetTimes) ']']);

set_param('EPSPsizeClamp/Input//Output_model_related/myStepSize_inputParameter', 'Value', ['[' num2str(myStepSize) ']']);

set_param('EPSPsizeClamp/Input//Output_board_related/commandCurrentSensitivity_inputParameter', 'Value', ['[' num2str(commandCurrentSensitivity) ']']);

set_param('EPSPsizeClamp/Input//Output_board_related/currentSensitivity_inputParameter', 'Value', ['[' num2str(currentSensitivity) ']']);

set_param('EPSPsizeClamp/Input//Output_board_related/voltageSensitivity_inputParameter', 'Value', ['[' num2str(voltageSensitivity) ']']);

set_param('EPSPsizeClamp/VoltageHolder/currentLimit', 'Value', ['[' num2str(currentLimit) ']']);

set_param('EPSPsizeClamp/VoltageHolder/vHold', 'Value', ['[' num2str(vHold) ']']);

set_param('EPSPsizeClamp/VoltageHolder/enableVoltageHold', 'Value', ['[' num2str(enableVoltageHold) ']']);

set_param('EPSPsizeClamp/VoltageHolder/default: active compensation/Discrete PID Controller','P', ['[' num2str(proportionalFactor) ']']);

set_param('EPSPsizeClamp/VoltageHolder/default: active compensation/Discrete PID Controller','I', ['[' num2str(integratorFactor) ']']);

% set_param('EPSPsizeClamp/EPSPsizeClamp/EPSPDetector/threshold','Value', ['[' num2str(threshold) ']']);

set_param('EPSPsizeClamp/EPSPsizeClamp/EPSPDetector/EPSPWindow','Value', ['[' num2str(EPSPWindow) ']']);

set_param('EPSPsizeClamp/EPSPsizeClamp/EPSPDetector/blankingTime','Value', ['[' num2str(blankingTime) ']']);

set_param('EPSPsizeClamp/EPSPsizeClamp/EPSPsizeClamp/firstSTGstimulusValue','Value', ['[' num2str(firstSTGstimulusValue) ']']);

set_param('EPSPsizeClamp/EPSPsizeClamp/EPSPsizeClamp/P_stg','Value', ['[' num2str(P_stg) ']']);

set_param('EPSPsizeClamp/EPSPsizeClamp/EPSPsizeClamp/I_stg','Value', ['[' num2str(I_stg) ']']);

set_param('EPSPsizeClamp/EPSPsizeClamp/EPSPsizeClamp/EPSPToStimAmpltude','Value', ['[' num2str(EPSPToStimAmpltude) ']']);

set_param('EPSPsizeClamp/EPSPsizeClamp/EPSPsizeClamp/targetEPSPsize','Value', ['[' num2str(targetEPSP) ']']);

set_param('EPSPsizeClamp/EPSPsizeClamp/EPSPsizeClamp/tau','Value', ['[' num2str(tau_history) ']']);






save_system(myModelName);                      % Save your command line changes to the model file


if compileFlag==1
    fprintf(1,'%s%s%s\n',' +++++ building model ',myModelName,' ...');
    save_system(myModelName);                      % Save your command line changes to the model file
    rtwbuild(myModelName);                         % Build and download application.
end
if exist('tg','var'),tg.close;end                  % Close the target connection if open.

fprintf(1,'%s%s%s\n',' +++++ uploading model ',myModelName,' ...');
tg.load(myModelName);                              % downloading model to the Target PC
% now the simulink model is assigned and the Target Objest tg is downloaded
% to the Target PC (it is automatically connected here).

tg.ShowSignals = 'on';                             % to show signals when displaying tg




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% 4) adding scopes ,etc %%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% setting recording scope sample number -- this is scope ID 1
myRecScopeID = tg.getscope(1);
sgs = myRecScopeID.getsignals;
% numberOfSamplesTotal = length(sgs) * ( eventNumber + 1 ) * ( ceil(switchOnDuration/myStepSize) ) ;
numberOfSamplesTotal = length(sgs) * ( ceil(switchOnDuration/myStepSize) ) ; % one recordoing event only, as we will change filenames for next one
myRecScopeID.set('NumSamples', numberOfSamplesTotal );
myRecScopeID.Mode = 'Commit';
%myRecScopeID.set('NumSamples', switchOnDuration/myStepSize*(length(switchOnTimes)+1) );

% set parameters of the model

tg.setparam(tg.getparamid( 'intracellularStimGenerator/times', 'Value') ,intracellTimes);
tg.setparam(tg.getparamid( 'intracellularStimGenerator/levels', 'Value') ,intracellValues);

tg.setparam(tg.getparamid( 'Input//Output_board_related/commandCurrentSensitivity_inputParameter', 'Value'), commandCurrentSensitivity);
tg.setparam(tg.getparamid( 'Input//Output_board_related/currentSensitivity_inputParameter', 'Value') ,currentSensitivity);
tg.setparam(tg.getparamid( 'Input//Output_board_related/voltageSensitivity_inputParameter', 'Value') ,voltageSensitivity);
tg.setparam(tg.getparamid( 'Input//Output_model_related/modelResetTimes_inputParameter', 'Value') ,modelResetTimes);
tg.setparam(tg.getparamid( 'Input//Output_model_related/myStepSize_inputParameter', 'Value') ,myStepSize);

tg.setparam(tg.getparamid( 'STGTriggerGenerator/triggerOnTimes', 'Value') ,triggerOnTimes);
tg.setparam(tg.getparamid( 'STGTriggerGenerator/triggerDuration', 'Value') ,triggerDuration);
tg.setparam(tg.getparamid( 'STGTriggerGenerator/triggerLevel', 'Value') ,triggerLevel);

tg.setparam(tg.getparamid( 'recordTriggerGenerator/switchOnTimes', 'Value') ,switchOnTimes);
tg.setparam(tg.getparamid( 'recordTriggerGenerator/switchOnDuration', 'Value') ,switchOnDuration);

tg.setparam(tg.getparamid( 'VoltageHolder/currentLimit', 'Value') ,currentLimit);
tg.setparam(tg.getparamid( 'VoltageHolder/vHold', 'Value') ,vHold);
tg.setparam(tg.getparamid( 'VoltageHolder/enableVoltageHold', 'Value') ,enableVoltageHold);


tg.setparam(tg.getparamid( 'VoltageHolder/default: active compensation/Discrete PID Controller/Proportional Gain','Gain') ,proportionalFactor);
tg.setparam(tg.getparamid( 'VoltageHolder/default: active compensation/Discrete PID Controller/Integral Gain','Gain') ,integratorFactor);


% tg.setparam(tg.getparamid( 'EPSPsizeClamp/EPSPDetector/threshold','Value') ,threshold);
tg.setparam(tg.getparamid( 'EPSPsizeClamp/EPSPDetector/EPSPWindow','Value') ,EPSPWindow);
tg.setparam(tg.getparamid( 'EPSPsizeClamp/EPSPDetector/blankingTime','Value') ,blankingTime);


tg.setparam(tg.getparamid( 'EPSPsizeClamp/EPSPsizeClamp/firstSTGstimulusValue','Value') ,firstSTGstimulusValue);
tg.setparam(tg.getparamid( 'EPSPsizeClamp/EPSPsizeClamp/P_stg','Value') ,P_stg);
tg.setparam(tg.getparamid( 'EPSPsizeClamp/EPSPsizeClamp/I_stg','Value') ,I_stg);
tg.setparam(tg.getparamid( 'EPSPsizeClamp/EPSPsizeClamp/EPSPToStimAmpltude','Value') ,EPSPToStimAmpltude);
tg.setparam(tg.getparamid( 'EPSPsizeClamp/EPSPsizeClamp/targetEPSPsize','Value') ,targetEPSP);
tg.setparam(tg.getparamid( 'EPSPsizeClamp/EPSPsizeClamp/tau','Value') ,tau_history); 


% IMPORTAANT: set running time update
tg.set('StopTime',startTime+runDuration); % total run time

tg.setparam(tg.getparamid( 'Input//Output_board_related/STG_voltageToStimSignal', 'Value'), STG_voltageToStimSignal);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% 5) running or preparing triggers %%%%%%%%%%%%%%%%%%

% START IT
fprintf(1,'%s%s%s\n',' +++++ STARTING model ',myModelName,' ...');
tg.start






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% 6) waiting for run to finish and recover data %%%%%

% take care of updating the STG regularly, at the right times
% AND
% change file name for recordings

% initialize
STGcount = 1;

% SAVE TIMES, STIMULI, PROBABILITIES OR INITIALIZE THEM
allStimuli = zeros(1,eventNumber); 
allTimes = triggerOnTimes(1:length(allStimuli)); 
allI_Vhold = zeros(1,eventNumber); 

allSlidingEPSPsizes = zeros(1,eventNumber);
allInstanteneousEPSPsizes = zeros(1,eventNumber);
  
KeepListening = 1;
while KeepListening==1 
    timePassed = tg.ExecTime; % look at time passed on the xPC 
    
    % PROGRAMMING STG AND SAVE THINGS AND UPDATE FILE NAME
    if timePassed > STGUpdateTimes(STGcount) % it's time to update
        
        
        % defining stimulus amplitude
        if isempty(PlaybackModeSTGvalues)  % IF PROBABILITY CLAMP IS DONE
            
            if STGcount==1 % first time
                nextStim = firstSTGstimulusValue;
                oldStim = firstSTGstimulusValue;
            else  % get it from the model
                idd = tg.getsignalidsfromlabel('nextSTGstimulus');
                nextStim = tg.getsignal(idd);
            end
            
        else  % IF PLAYBACK IS PERFORMED
            disp(' --> !!! NO CLAMPING JUST PLAYBACK ! ! ! ' ); 
            if STGcount==1 % first time
                oldStim = PlaybackModeSTGvalues(STGcount);
                nextStim = PlaybackModeSTGvalues(STGcount);
            else
                nextStim = PlaybackModeSTGvalues(STGcount);
            end
        end
        
        % safety
        if nextStim>LIMITSTGstimulusValue
            disp([' --> !! STG STIMULUS ' num2str(nextStim) ' TOO HIGH, cap at limit of: ' num2str(LIMITSTGstimulusValue) ' !! ']); 
        nextStim = LIMITSTGstimulusValue;
        end
        
        % get sliding probability and fired or not information (if the last
        % event evoked a spike or not)
        
        if STGcount==1 
            thisTraceFile = 'none.a';  % NON existent for first time
            EPSPsliding_this = 0;
            InstanteneousEPSPsizes_this = 0;
            allI_Vhold_this = 0;
        else
            thisTraceFile = myRecScopeID.FileName;
            idd = tg.getsignalidsfromlabel('slidingEPSPsize');
            EPSPsliding_this = tg.getsignal(idd);
            idd = tg.getsignalidsfromlabel('lastEPSPsize');
            InstanteneousEPSPsizes_this = tg.getsignal(idd);
            idd = tg.getsignalidsfromlabel('IvclampSignal');
            allI_Vhold_this = tg.getsignal(idd);            
        end
        
        % store value ( here PREVIOUS EVENT information is stored);        
        allStimuli(STGcount) = oldStim;
        allSlidingEPSPsizes(STGcount) = EPSPsliding_this;
        fileList{STGcount} = thisTraceFile;
        allI_Vhold(STGcount) = allI_Vhold_this;     
        allInstanteneousEPSPsizes(STGcount) = InstanteneousEPSPsizes_this;
        
        % set next filename 
        myRecScopeID.stop;
        ttt = clock; lname = sprintf('%02d%02d%02d', ttt(4),ttt(5),floor( ttt(6) )); % name HHMMSS
        myRecScopeID.FileName = [lname '.a'];
        myRecScopeID.start;
        
        % applying...
        if triggerOnTimes(STGcount)+STGtotalStreamingTime <= timePassed, error(' YOU ARE NOT UPDATING STG IN CORRECT TIMES : PREMATURE UPDATE! ! ! STOP RUN !'); end
        problemHappened = STG_UploadData(DeviceID, {[stimulusLength stimulusLength stimulusZeroAfterLength]}, {[-1*nextStim 1*nextStim 0]}, controlMode); % biphasic!
        if problemHappened~=0, error(' STG UPLOAD ERROR! ! ! STOP RUN !'); end
        % fprintf(1,'%s%d%s%d%s%d%s\n', ' ** Progress: ', STGcount, ' of ', eventNumber, '. Time left (approx.):', ceil(runDuration-timePassed) ,' s ...');
        timeBeforeTrigger = triggerOnTimes(STGcount) - tg.ExecTime;
        fprintf(1,'%s%f%s%f%s\n', ' ** Uploaded stimulus: ', nextStim, '[uA] ... trigger in:', timeBeforeTrigger,' s...');
        if( (timeBeforeTrigger<=0) || (timeBeforeTrigger>=InterstimInterval) ), error(' YOU ARE NOT UPDATING STG IN CORRECT TIMES ! ! ! STOP RUN !'); end
        
        disp([' ** Output name is : ' thisTraceFile ]);
        disp([' ** Sliding EPSP size: ' num2str(EPSPsliding_this) ' --']); 
        disp([' ** Current EPSP size: ' num2str(InstanteneousEPSPsizes_this) ' --']);        
        disp(['------------------- Progress: ' num2str(STGcount) '/' num2str(eventNumber) ' ...']);
        
        % PREPARE FOR NEXT EVENT
        % update counter 
        STGcount = STGcount + 1;             
        if STGcount >= eventNumber+1, KeepListening = 0; end
        % update stimulus history log
        oldStim = nextStim;

        
    else % sleep a bit
        pause(InterstimInterval/200);
    end

end



% wait for run to finish OR use stop button
if runDuration~=inf % run by timer
fprintf(1,'%s%s%s\n',' +++++ waiting for model run to finish: ',myModelName,' ...');
    while strcmp(tg.Status,'running')   % while it runs, wait. NOTE: THIS COMMAND PAUSES MATLAB (avoid by submitting this function as a job)
        pause(0.2);                     % ask status every half a second 
    end
else % run until stopped
    fprintf(1,'%s%s%s\n',' +++++ run INFINITly long - until STOP pressed: ',myModelName,' ...');
    h = msgbox('Press OK to STOP running the xPC!'); uiwait(h);
    tg.stop;
end




% closing STG and connection to it (ONCE!)
STG_Close(DeviceID);


    
if signalNumber ~= 1
    % retrieving logged data from Target PC
    fprintf(1,'%s%s%s\n',' +++++ Retrieving data for model ',myModelName,' ...');
    state = tg.StateLog;
    data = tg.OutputLog;
    TET = tg.TETLog;
    dt = myStepSize;
else
    state = [];
    data = [];
    TET = [];
    dt = myStepSize;
end


% decoding and sorting data
parameters.dt = myStepSize;
parameters.runDuration = runDuration;
parameters.extracellStim.logTimeBeforeStim = logTimeBefore;
parameters.extracellStim.logTimeAfterStim = logTimeAfter;
parameters.extracellStim.InterstimInterval = InterstimInterval;
parameters.extracellStim.stimulusLength = stimulusLength;
parameters.extracellStim.stimulusAmplitudes = allStimuli;                     
parameters.extracellStim.controlMode = controlMode;
parameters.extracellStim.units = myunits; 
parameters.extracellStim.LIMITSTGstimulusValue = LIMITSTGstimulusValue;

parameters.extracellStim.firstSTGstimulusValue = firstSTGstimulusValue; 
parameters.EPSPsizeClamp.P_stg = P_stg; 
parameters.EPSPsizeClamp.I_stg = I_stg; 
parameters.EPSPsizeClamp.EPSPToStimAmpltude = EPSPToStimAmpltude; 
parameters.EPSPsizeClamp.targetEPSPsize = targetEPSP ; 
parameters.EPSPsizeClamp.tau = tau_history ; 
% parameters.spikeDetection.threshold = threshold ; 
parameters.spikeDetection.EPSPWindow = EPSPWindow; 
parameters.spikeDetection.blankingTime = blankingTime; 


parameters.voltageClamping.enableVoltageHold = enableVoltageHold;
parameters.voltageClamping.vHold = vHold;
parameters.voltageClamping.currentLimit = currentLimit;
parameters.totalEventNumber = eventNumber;
parameters.Kernel = Kernel;
parameters.kernelfile = kernelfile;
parameters.switchOnTimes = switchOnTimes;
parameters.triggerOnTimes = triggerOnTimes;
parameters.STGUpdateTimes=STGUpdateTimes;

parameters.intracellStimTimes=intracellTimes;
parameters.intracellStimValues=intracellValues;

parameters.commandCurrentSensitivity = commandCurrentSensitivity;
parameters.commandVoltageSensitivity = commandVoltageSensitivity;
parameters.voltageSensitivity = voltageSensitivity;
parameters.currentSensitivity = currentSensitivity;

for i = 1:eventNumber
    recording{i}.startTime = switchOnTimes(i);
    recording{i}.duration = switchOnDuration;
        
    recording{i}.traceFile = fileList{i};
    recording{i}.extracellStimAmplitude = allStimuli(i);    
    recording{i}.EPSPsize = allSlidingEPSPsizes(i);
    recording{i}.InstanteneousEPSPsize = allInstanteneousEPSPsizes(i);
    
    recording{i}.allI_Vhold = allI_Vhold(i);
        
    recording{i}.extracellStimDuration = stimulusLength;
    recording{i}.extracellStimStartTimeOffset = logTimeBefore;
    recording{i}.intracellStimulus = intracellValues(i);
    recording{i}.intracellStimulusOffset = logTimeBefore + pulseOffsetTime;
end

% saving results to a file, having as name date and time
ttt = clock; logname = sprintf('%04d%02d%02d%02d%02d%02d%02d',ttt(1),ttt(2),ttt(3),ttt(4),ttt(5),floor( ttt(6) ), ceil(mod( ttt(6),100 )) ); % name YYYMMDDHHMMSSdd
logname = fullfile(destinationFolder,logname);
mkdir(logname);
systemtime = clock; 
save(fullfile(logname, 'MYDATA.mat'), 'parameters', 'recording', 'systemtime');

% GET FILES

if getDataNow == 1
    getXPCtraces(tg, fullfile(logname, 'MYDATA.mat'));
end





fprintf(1,'%s%s%s\n',' +++++ DONE handling model ',myModelName,' ...');

end
