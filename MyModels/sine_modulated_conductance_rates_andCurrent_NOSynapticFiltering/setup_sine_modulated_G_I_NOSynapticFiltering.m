function [tg measurementData StreamValues parameters] = setup_sine_modulated_G_I_withSynapticFiltering(CurrentStimFile, varargin)
% ** Sets up a model as used in Brio, Linaro, Guigliano, 2014, in
% preparation, for injecting sine-modulated current or conductance siganls
% in patchen neurons, on the top of a synaptic background. Dynamic clamp
% methids are used. Sine frequencies are varied to probe the response on a
% broad range.

% ** Conductance traces (excitatory and inhibitory) are generated
% on-the-fly. Current modulation can be generated on-the-fly too and / or added
% from a file (pre-generated).

% ** There is the possibility to enable the firing clamp module, which will
% generate a feedback current to clamp the neuuronal fitring rate. Dor not
% use this in parallel with modulated inputs, as they loose their menaing.




% Istvan Biro, 2014.05.26.


%% 1) parameters


%%%%% Synaptic input properties
% R = r0 + R1*sin(2*pi*R_omega*(t+R_t0));
RMembrane = [];% **** RMembrane - [Mohm]. if empty, standatd unit conductances are used, otherwise 2 and 6 % of 1/Rm.

v_balance = -60; % mV -- membrane voltege to balace conductance components for. use [] to set default exc and inh rates instead.

R0_exc = 7000;  % [Hz] excitatory rate - MEAN
tau_exc = 5;                      % [ms]
tau_inh = 10;                      % [ms]
E_rev_exc = 0;                    % [mV]
E_rev_inh = -80;
R_omega_exc = 1;
R_omega_inh = 1;
R_t0_exc = 0;
R_t0_inh = 0;
R_seed_exc = intmax('int16')*rand(1,1);
R_seed_inh = intmax('int16')*rand(1,1);
if ~isempty(RMembrane)
    g_unit_exc =  2/100 * 1/(RMembrane*1e6) * 1e9;  % [nS]
    g_unit_inh =  6/100 * 1/(RMembrane*1e6) * 1e9; % [nS]
else
    g_unit_exc =  50/1000;  % [nS]
    g_unit_inh =  190/1000; % [nS]
end
if ~isempty(v_balance)
    R_excOverR_inh = computeRatesRatio(v_balance, tau_exc, tau_inh, E_rev_exc, E_rev_inh, g_unit_exc, g_unit_inh);
    R0_inh = 1./R_excOverR_inh.* R0_exc;
else
    R0_inh = 3000;
end

R1_exc = 0.1*R0_exc;
R1_inh = 0.1*R0_inh;


%%%%% Spike
threshold = 0; % [mV] detection


%%%%% Sine modulated current
I0 = 0 ;
I1 = 0 ;
I_omega = 1;
I_time0 = 0;


%%%%% Firing rate clamp (do not use sumultaneously with sine-modulation)
enableFiringRateClamp = 0; % flag (0 or 1) to disable and enable feature.
targetFrate = 10;
maxISI = 2; % [s] maximal interspike interval.
fi_P = 20;
fi_I = 10;
fi_currentIncreaseStep = 0.01;
fi_skipFirstNspikes = 10;
fi_historyWeight = 10;
fi_minSpikeDur = 0.00333;


%%%%% Model paranaters
myTargetPcName = 'myOneAndOnlyXPC';
myModelName = 'sine_modulated_G_I_withSynapticFiltering';
myStepSize = 1./20000;

compileFlag = 0;
useTargetFileSystem = 1;
logToMemoryFlag = 1;

% warnings_checks = 'on';

apmilifierConfigFile = '.\..\..\HARDWARE_CONFIGURATION\patchAmplifier_CClamp.m';  % file that stores conversion factors for amplifier

kernelfile = ''; %'20120203191020_kernel.dat';  File name in which the AEC kernel is stored, as single variable, called Kernel


runDuration = 30; % [s]






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%% EVALUATE VARAGIN %%%%%%%%%%%%%%%%%%%
% evaluating varargin; (Name, value) pairs, as ('plotFitHandle', 67)
% expected
% This allows to overwrite the settings defined above this level, from
% parameters passed to the function as arguments, for e.g. setting it to
% compile, using the additional arguments: 'compileFlag', 1.
noVars = length(varargin);
for i=1:2:noVars
    if isnumeric(varargin{i+1})
        if (isscalar(varargin{i+1})), commandName=sprintf('%s%s%f%s',varargin{i},'=',varargin{i+1},';');
        else commandName=sprintf('%s%s%s%s',varargin{i},'=',mat2str(varargin{i+1}),';');
        end
    else commandName=sprintf('%s%s%s%s',varargin{i},'=''',varargin{i+1},''';');
    end
    eval(commandName);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%% prepare some things %%%%%%%%%%%%%%%%%%%%%%%%
% open model
open_system(myModelName);

% AMPLIFIER RESOLUTION VALUES
run(apmilifierConfigFile);

% AEC kernel
if ( ~isempty(kernelfile) && exist(kernelfile,'file') )
    Kernel = load(kernelfile);
    enableAEC = 1;
    disp('-Kernel loaded.');
else
    kernelfile = '';
    Kernel = zeros(1,100);
    enableAEC = 0;
    disp('- NO kernel loaded.');
end

% time
systemtime=clock;

%%%%%%%%%%%%%%%%%%%%%%%%%%%% create data to be streamed %%%%%%%%%%%%%%%%%

% create stimulus
if ~isempty(CurrentStimFile)
    
    % generate trace
    command = [ 'create_stimulus.exe' ' 0 1 ' num2str(1./myStepSize) ' ' CurrentStimFile]; % result data is now in stimulus.dat
    [status, result] = system(command);
    [srate N M time values] = load_binary('stimulus.dat');
    StreamValues(1,:) = values;
    dur = time(end);
else
    % make current step stimulus file - of 0 !!!!!
    CurrentStimFile = 'thisStim.stim'; % DONT TOUCH!
    fid = fopen(CurrentStimFile,'w');
    fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', runDuration,	1,	0,	    0, 	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fclose(fid);
    
    % generate trace
    command = [ 'create_stimulus.exe' ' 0 1 ' num2str(1./myStepSize) ' ' CurrentStimFile]; % result data is now in stimulus.dat
    [status, result] = system(command);
    [srate N M time values] = load_binary('stimulus.dat');
    StreamValues(1,:) = values;
    dur = time(end);
end
stimFileContents = [];
% get simt file contents
stimFileContents.name = CurrentStimFile;
stimFileContents.line = {};
fpp = fopen(CurrentStimFile);
tline = fgetl(fpp);
while ischar(tline)
    stimFileContents.line{end+1} = tline;
    tline = fgetl(fpp);
end
fclose(fpp);
    
    

% calculate run duration
runDuration = max([runDuration, dur]);




%% 2) run general setup
% This will set up most commonly used settings for running the model on
% xPC. This function (setup_general_model_settings_for_xPC) should be
% called at least once, after model construction, to set up the right
% compiler and other options for xPC.
if compileFlag~=0 || ~exist(fullfile(pwd, [myModelName '.dlm']),'file')
    tg = setup_general_model_settings_for_xPC(myTargetPcName , myModelName, runDuration, myStepSize);
else
    tg = xpctarget.xpc(myTargetPcName);
end


%% 3) set other model parameters, if needed (this is on HOST level, before save and compile !!)
% using set_param( myModelName/mySubSystemname/.../blockName , 'PropertyName', PropertyValue );
% NOTE: if you change model parameters here, using this command, you MUST
% compile the model (see step 4) ) again before downloading (next steps). If you want to
% avoid this, set parameters AFTER downloading (see step 6) ). Any model
% needs to be compiled once, but if no major changes to it are done (major
% changes are those which mofify more than the values of its parameters),
% you can just download it and than set parameter values, avoiding the
% lengthy compilation time.

% set kernel length
set_param( [myModelName '/AEC'] , 'Kernel', [' [' num2str(Kernel) ' ]' ] );


% Set up memory for logging data to memory, if that is require. Do so by
% running here:
% setUpMenoryLoggingFromTopOutports(myModelName, runDuration, myStepSize);
% Note: siganls logged will be those, which connect to an "OUT" port on the
% top level of the model (not in a subsystem but on highest level). See
% step 8) on how to retrieve data after the run (data = tg.OutputLog;).
% NOTE: if you did not log to memory from this model before and just
% changed this option, make sure to set compileFlag=1, as this change
% requires re-compilation.
if logToMemoryFlag==1
    setUpMenoryLoggingFromTopOutports(myModelName, runDuration, myStepSize);
end
measurementData = [];



%% 4) compile if necessary
% NOTE: see comments at 3)  !!! 
% this takes long time
if compileFlag~=0 || ~exist(fullfile(pwd, [myModelName '.dlm']),'file')
    compile_general_model_for_xPC(myModelName);
end


%% 5) download model and files to xPC
% NOTE: files from which you'd like to tream can be put on xPC like this: 
if useTargetFileSystem~=0, [driveLetterFound, tgfs] = setup_fileSystem_xPC(tg, ''); end  % set up tardet hard disk if needed, choosing the correct drive.
% datavalues = [ sin(1.*t); sin(2.*t); sin(3.*t); sin(4.*t); sin(5.*t) ]; -- this will give 5 parallel output signals
% xpcbytes2file('file.dat', datavalues);
% myftp = tg.ftp;
% myftp.put(currentAddFile);
% You can also write to xPC file system directly (faster). For this use
% [driveLetterFound, tgfs] = setup_fileSystem_xPC(tg, ''); to get tgfs (IF
% NOT DONE SO PREVIOUSLY!! ) and than:
% xpcbytes2file(currentAddFile, datavalues); % makes binary file on host, in special xPC format
% fh = fopen('input.dat','r');
% ddataa = fread(fh);
% fclose(fh);   % data now read
% h = tgfs.fopen(currentAddFile, 'w');
% fwrite(tgfs, h, ddataa);
% tgfs.fclose(h);

% generate the stimulus and download it 
disp('--> Uploading stimulus file, please wait..');

currentAddFile = 'input.dat'; 
xpcbytes2file(currentAddFile, StreamValues); % makes binary file on host, in special xPC format
fh = fopen('input.dat','r');
ddataa = fread(fh);
fclose(fh);   % data now read
h = tgfs.fopen(currentAddFile, 'w');
fwrite(tgfs, h, ddataa);
tgfs.fclose(h);
ddataa = []; % to save memory

% download model
download_general_model_for_xPC(tg, myModelName);


%% 6) set specific values IN THE DOWNLOADED TARGET MODEL (directly in xPC)
% using tg.setparam(tg.getparamid( 'mySubSystemname/.../blockName', 'PropertyName') ,PropertyValue);
% Since this affects the downloaded model, no recompilation is necessary at
% this step.

% sample time
tg.SampleTime = myStepSize;
tg.setparam(tg.getparamid( 'ModelParaneters/myStepSize', 'Value'), myStepSize ); 

% set patch amplifier conversion factors
tg.setparam(tg.getparamid( 'Input//Output_DAQ/commandConversionFactor', 'Value'), commandConversionFactor ); 
tg.setparam(tg.getparamid( 'Input//Output_DAQ/PrimaryOutputConversionFactor', 'Value'), PrimaryOutputConversionFactor ); 
tg.setparam(tg.getparamid( 'Input//Output_DAQ/SecondaryOutputConversionFactor', 'Value'), SecondaryOutputConversionFactor ); 

% set AEC Kernel
tg.setparam(tg.getparamid( 'AEC/Kernel', 'Value'), Kernel ); 
tg.setparam(tg.getparamid( 'ModelParaneters/EnableKernel', 'Value'), enableAEC ); 

% synaptic variables
tg.setparam(tg.getparamid( 'GeneratePresynapticInputs/R0s', 'Value'), [R0_exc; R0_inh] ); 
tg.setparam(tg.getparamid( 'GeneratePresynapticInputs/R1s', 'Value'), [R1_exc; R1_inh] ); 
tg.setparam(tg.getparamid( 'GeneratePresynapticInputs/R_omeagas', 'Value'), [R_omega_exc; R_omega_inh] ); 
tg.setparam(tg.getparamid( 'GeneratePresynapticInputs/R_time0s', 'Value'), [R_t0_exc; R_t0_exc] ); 
tg.setparam(tg.getparamid( 'GeneratePresynapticInputs/g0s', 'Value'), [g_unit_exc; g_unit_inh] ); 
tg.setparam(tg.getparamid( 'GeneratePresynapticInputs/ts', 'Value'), [tau_exc; tau_inh] ); 
tg.setparam(tg.getparamid( 'GeneratePresynapticInputs/Ers', 'Value'), [E_rev_exc; E_rev_inh] ); 
tg.setparam(tg.getparamid( 'GeneratePresynapticInputs/seeds1', 'Value'), [ R_seed_exc; R_seed_inh] );

% current
tg.setparam(tg.getparamid( 'GenerateAddedCurrents/I0', 'Value'), I0 ); 
tg.setparam(tg.getparamid( 'GenerateAddedCurrents/I1', 'Value'), I1 ); 
tg.setparam(tg.getparamid( 'GenerateAddedCurrents/I_omega', 'Value'), I_omega ); 
tg.setparam(tg.getparamid( 'GenerateAddedCurrents/I_time0', 'Value'), I_time0 ); 

% firing rate clamp
tg.setparam(tg.getparamid( 'Processing/ClampFiringFrequency/targetFrate', 'Value'), targetFrate ); 
tg.setparam(tg.getparamid( 'Processing/enableFiringRateClamp', 'Value'), enableFiringRateClamp ); 
tg.setparam(tg.getparamid( 'Processing/ClampFiringFrequency/firingFrequencyClampPID_spikeTriggered/P', 'Value'), fi_P ); 
tg.setparam(tg.getparamid( 'Processing/ClampFiringFrequency/firingFrequencyClampPID_spikeTriggered/I', 'Value'), fi_I );
tg.setparam(tg.getparamid( 'Processing/ClampFiringFrequency/firingFrequencyClampPID_spikeTriggered/currentIncreaseStep', 'Value'), fi_currentIncreaseStep ); 
tg.setparam(tg.getparamid( 'Processing/ClampFiringFrequency/firingFrequencyClampPID_spikeTriggered/skipFirstNspikes', 'Value'), fi_skipFirstNspikes ); 
tg.setparam(tg.getparamid( 'Processing/ClampFiringFrequency/spikeFrequencyMonitor_spikeTriggered/historyWeight', 'Value'), fi_historyWeight ); 
tg.setparam(tg.getparamid( 'Processing/ClampFiringFrequency/spikeFrequencyMonitor_spikeTriggered/minSpikeDur', 'Value'), fi_minSpikeDur ); 
tg.setparam(tg.getparamid( 'Processing/ClampFiringFrequency/spikeFrequencyMonitor_spikeTriggered/thresholdFDetect', 'Value'), threshold ); 


%% 7) Do what is necessary for starting the model and handle data/parameters/feedback/... DURING its running time
% Note: you can check if a model is running by strcmp(tg.Status,'running')
% and access values of signals too using tg.xxxxx
% NOTE: start model by tg.start;
% NOTE: wait for model to finish like this:
% while strcmp(tg.Status,'running')
%     pause(0.2);
% end

 tg.set('StopTime',runDuration); % total run time % IMPORTANT to set run duration right
 tg.start;
 disp('--> Model running, please wait..');
while strcmp(tg.Status,'running')
    pause(0.2);
end


%% 8) Do what is necessary after model finished running

% NOTE: if you recorded into a file, get it like this:
% xpcftp=tg.ftp;
% try 
%     xpcftp.get('xPCdata.dat');
% catch err
%     xpcftp.get('xPCdata.dat');
% end
% xpcFileData = readxpcfile('xPCdata.dat'); % Convert the data

% You can also read from xPC file system directly (faster). For this use
% [driveLetterFound, tgfs] = setup_fileSystem_xPC(tg, ''); to get tgfs (IF
% NOT DONE SO PREVIOUSLY!! ) and than:
% h = tgfs.fopen('xPCdata.dat');
% data = tgfs.fread(h);
% tgfs.fclose(h);
% xpcFileData  = readxpcfile(data);
% tgfs.removefile('xPCdata.dat');

% NOTE: if you recorded data to xPC memory with top level (highest level in model diagram) OUTPORT elements, get
% it like this:
% data = tg.OutputLog;

% Do not forget to save the data to the file format of your choice.

disp('--> Obtaining data from target, please wait..');

if logToMemoryFlag==1 % if we logged to memory, we save the data to the host
    measurementData = tg.OutputLog;
end

disp('--> DONE!');



%%% Store some things just to be sure
parameters.myModelName= myModelName;
parameters = [];
parameters.dt = myStepSize;
parameters.runDuration = runDuration;
parameters.AEC_Kernel = Kernel;
parameters.enableAEC = enableAEC;
parameters.systemtime = systemtime;
parameters.stimFileContents = stimFileContents;

parameters.R0_exc = R0_exc;
parameters.R0_inh = R0_inh ;
parameters.R1_exc = R1_exc;
parameters.R1_inh = R1_inh ;
parameters.R_omega_exc = R_omega_exc ;
parameters.R_seed_inh = R_seed_inh ;
parameters.R_t0_exc = R_t0_exc ;
parameters.R_t0_inh = R_t0_inh ;
parameters.R_seed_exc = R_seed_exc ;
parameters.R_seed_inh = R_seed_inh ;
parameters.tau_exc = tau_exc;
parameters.tau_inh = tau_inh;
parameters.E_rev_exc = E_rev_exc;
parameters.E_rev_inh = E_rev_inh;
parameters.g_unit_exc = g_unit_exc;
parameters.E_rev_inh = E_rev_inh;

parameters.I0 = I0 ;
parameters.I1 = I1 ;
parameters.I_omega = I_omega ;
parameters.I_time0 = I_time0 ;

parameters.RMembrane = RMembrane ;
parameters.v_balance = v_balance ;

parameters.enableFiringRateClamp = enableFiringRateClamp ;
parameters.targetFrate = targetFrate ;
parameters.maxISI = maxISI ;


end