function [tg measurementData StreamValues parameters] = setup_arbitraryStream_and_record_ConductanceNoiseTest(StimFiles, varargin)
% ** Sets up model "arbitraryStream_and_record.mdl": stream pre-generated
% data from file and record outputs.
% ** StimFiles is the name (with path if not local) of the .stim file used by
% create_stimulus.exe, containing the stimulus information that will be
% generated and uploaded to xPC. 
% ** If you wish to output several streams,
% please provide a .stim file for each, in the form: StimFiles =
% {StimFile_1, StimFile_2, ... , StimFile_n}. If you choose to do this, you
% must make sure that the "From File" and "Byte Unpacking " blocks are
% properly configures and the signals are wired to the proper destination.
% It is recommended to create another model for that purpose.
% ** The model will run for the time to complete streaming the provided
% stimulus or for FORCE_runDuration seconds (if FORCE_runDuration is not  []).
% ** If a stimulus is shorter than the run time, its last value is held
% until run time ends.




% Istvan Biro, 2014.05.26.


%% 1) parameters


targetFrate = 10;



myTargetPcName = 'myOneAndOnlyXPC';
myModelName = 'arbitraryStream_and_record_ConductanceNoiseTest';
FORCE_runDuration = [];  % leave [] to have a runtime definged by the stimulus (StimFile). If you want to run longer/shorter, define the duration here: FORCE_runDuration = x; (in seconds)
myStepSize = 1./20000;

compileFlag = 0;
useTargetFileSystem = 1;
logToMemoryFlag = 1;

warnings_checks = 'on';

apmilifierConfigFile = '.\..\..\HARDWARE_CONFIGURATION\patchAmplifier_CClamp.m';  % file that stores conversion factors for amplifier

kernelfile = ''; %'20120203191020_kernel.dat';  File name in which the AEC kernel is stored, as single variable, called Kernel


RMembrane = [];% **** RMembrane - [Mohm]. if empty, standatd unit conductances are used,
% otherwise 2 and 6 % of 1/Rm.

R0_exc = 7000;  % [Hz] excitatory rate
tau_exc = 5;                      % [ms]
tau_inh = 10;                      % [ms]
E_rev_exc = 0;                    % [mV]
E_rev_inh = -80;
% g_unit_exc =  2/100 * 1/(RMembrane*1e6) * 1e9;  % [nS]
% g_unit_inh =  6/100 * 1/(RMembrane*1e6) * 1e9; % [nS]
% g_unit_exc =  50/1000;  % [nS]
% g_unit_inh =  190/1000; % [nS]
if ~isempty(RMembrane)
    g_unit_exc =  2/100 * 1/(RMembrane*1e6) * 1e9;  % [nS]
    g_unit_inh =  6/100 * 1/(RMembrane*1e6) * 1e9; % [nS]
else
    g_unit_exc =  50/1000;  % [nS]
    g_unit_inh =  190/1000; % [nS]
end

R0_inh = 3000;
% R_excOverR_inh = computeRatesRatio(v_balance, RMembrane, tau_exc, tau_inh, E_rev_exc, E_rev_inh);
% R0_inh = 1./R_excOverR_inh.* R0_exc;




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%% EVALUATE VARAGIN %%%%%%%%%%%%%%%%%%%
% evaluating varargin; (Name, value) pairs, as ('plotFitHandle', 67)
% expected
% This allows to overwrite the settings defined above this level, from
% parameters passed to the function as arguments, for e.g. setting it to
% compile, using the additional arguments: 'compileFlag', 1.
noVars = length(varargin);
for i=1:2:noVars
    if isnumeric(varargin{i+1})
        if (isscalar(varargin{i+1})), commandName=sprintf('%s%s%f%s',varargin{i},'=',varargin{i+1},';');
        else commandName=sprintf('%s%s%s%s',varargin{i},'=',mat2str(varargin{i+1}),';');
        end
    else commandName=sprintf('%s%s%s%s',varargin{i},'=''',varargin{i+1},''';');
    end
    eval(commandName);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%% prepare some things %%%%%%%%%%%%%%%%%%%%%%%%
% open model
open_system(myModelName);

% AMPLIFIER RESOLUTION VALUES
run(apmilifierConfigFile);

% AEC kernel
if ( ~isempty(kernelfile) && exist(kernelfile,'file') )
    Kernel = load(kernelfile);
    enableAEC = 1;
    disp('-Kernel loaded.');
else
    kernelfile = '';
    Kernel = zeros(1,100);
    enableAEC = 0;
    disp('- NO kernel loaded.');
end

% time
systemtime=clock;

%%%%%%%%%%%%%%%%%%%%%%%%%%%% create data to be streamed %%%%%%%%%%%%%%%%%

% create stimulus
if ischar(StimFiles), StimFiles = {StimFiles};
elseif iscell(StimFiles), % is fine
else error('Wrong StimFiles input form');
end

stimFileContents = [];

StreamValues = {};
maxLength = 0;
for i=1:length(StimFiles)
    % get simt file contents
    stimFileContents(i).name = StimFiles{i};
    stimFileContents(i).line = {};
    fpp = fopen(StimFiles{i});
    tline = fgetl(fpp);
    while ischar(tline)
        stimFileContents(i).line{end+1} = tline;
        tline = fgetl(fpp);
    end
    fclose(fpp);
    
    % generate trace
    command = [ 'create_stimulus.exe' ' 0 1 ' num2str(1./myStepSize) ' ' StimFiles{i}]; % result data is now in stimulus.dat
    [status, result] = system(command);
    [srate N M time values] = load_binary('stimulus.dat');
    StreamValues{i}(1,:) = values;
    if length(values)>maxLength, maxLength = length(values); end 
end
% pas with the last value to have equal length
for i=1:length(StreamValues)
    pad = maxLength - length(StreamValues{i});
    if pad>0, StreamValues{i} = [StreamValues{i}(1,:), StreamValues{i}(end).*ones(1,pad) ]; end
end
% make it a matrix
StreamValues = cell2mat(StreamValues');
[StreamSignalNumber, StreamSignalLength] = size(StreamValues);

% calculate run duration
if isempty(FORCE_runDuration),     runDuration = myStepSize * (StreamSignalLength+1);
else runDuration = FORCE_runDuration;
end




%% 2) run general setup
% This will set up most commonly used settings for running the model on
% xPC. This function (setup_general_model_settings_for_xPC) should be
% called at least once, after model construction, to set up the right
% compiler and other options for xPC.
if compileFlag~=0 || ~exist(fullfile(pwd, [myModelName '.dlm']),'file')
    tg = setup_general_model_settings_for_xPC(myTargetPcName , myModelName, runDuration, myStepSize);
else
    tg = xpctarget.xpc(myTargetPcName);
end


%% 3) set other model parameters, if needed (this is on HOST level, before save and compile !!)
% using set_param( myModelName/mySubSystemname/.../blockName , 'PropertyName', PropertyValue );
% NOTE: if you change model parameters here, using this command, you MUST
% compile the model (see step 4) ) again before downloading (next steps). If you want to
% avoid this, set parameters AFTER downloading (see step 6) ). Any model
% needs to be compiled once, but if no major changes to it are done (major
% changes are those which mofify more than the values of its parameters),
% you can just download it and than set parameter values, avoiding the
% lengthy compilation time.

% just check if the file treaming is configured correctly
if strcmp(warnings_checks,'on')
    a = get_param( 'arbitraryStream_and_record_ConductanceNoiseTest/InputData/Byte Unpacking' , 'MaskUnpackedDataSizes' );
    aa = eval(a);
    b = get_param( 'arbitraryStream_and_record_ConductanceNoiseTest/InputData/From File' , 'dataSize' );
    if length(aa)~=StreamSignalNumber | str2num(b)~= 8*StreamSignalNumber
        disp([ ' --> !!!!!  WARNING: you seem to have ' num2str(StreamSignalNumber) ' signals -- which does not agree with the "From File" and "Byte Unpacking" block settings!!!! ' ]);
        disp(['  You should have set "From File" "Output width" to ' num2str(8*StreamSignalNumber) ' bytes and "Byte Unpacking" block to output the right number of signals and types (see help of block).' ]);
        disp(' Type dbcont to continue, dbquit to stop and debug');
        keyboard
    end
end

% set kernel length
set_param( 'arbitraryStream_and_record_ConductanceNoiseTest/AEC' , 'Kernel', [' [' num2str(Kernel) ' ]' ] );


% Set up memory for logging data to memory, if that is require. Do so by
% running here:
% setUpMenoryLoggingFromTopOutports(myModelName, runDuration, myStepSize);
% Note: siganls logged will be those, which connect to an "OUT" port on the
% top level of the model (not in a subsystem but on highest level). See
% step 8) on how to retrieve data after the run (data = tg.OutputLog;).
% NOTE: if you did not log to memory from this model before and just
% changed this option, make sure to set compileFlag=1, as this change
% requires re-compilation.
if logToMemoryFlag==1
    setUpMenoryLoggingFromTopOutports(myModelName, runDuration, myStepSize);
end
measurementData = [];



%% 4) compile if necessary
% NOTE: see comments at 3)  !!! 
% this takes long time
if compileFlag~=0 || ~exist(fullfile(pwd, [myModelName '.dlm']),'file')
    compile_general_model_for_xPC(myModelName);
end


%% 5) download model and files to xPC
% NOTE: files from which you'd like to tream can be put on xPC like this: 
if useTargetFileSystem~=0, [driveLetterFound, tgfs] = setup_fileSystem_xPC(tg, ''); end  % set up tardet hard disk if needed, choosing the correct drive.
% datavalues = [ sin(1.*t); sin(2.*t); sin(3.*t); sin(4.*t); sin(5.*t) ]; -- this will give 5 parallel output signals
% xpcbytes2file('file.dat', datavalues);
% myftp = tg.ftp;
% myftp.put(currentAddFile);
% You can also write to xPC file system directly (faster). For this use
% [driveLetterFound, tgfs] = setup_fileSystem_xPC(tg, ''); to get tgfs (IF
% NOT DONE SO PREVIOUSLY!! ) and than:
% xpcbytes2file(currentAddFile, datavalues); % makes binary file on host, in special xPC format
% fh = fopen('input.dat','r');
% ddataa = fread(fh);
% fclose(fh);   % data now read
% h = tgfs.fopen(currentAddFile, 'w');
% fwrite(tgfs, h, ddataa);
% tgfs.fclose(h);

% generate the stimulus and download it 
disp('--> Uploading stimulus file, please wait..');

currentAddFile = 'input.dat'; 
xpcbytes2file(currentAddFile, StreamValues); % makes binary file on host, in special xPC format
fh = fopen('input.dat','r');
ddataa = fread(fh);
fclose(fh);   % data now read
h = tgfs.fopen(currentAddFile, 'w');
fwrite(tgfs, h, ddataa);
tgfs.fclose(h);
ddataa = []; % to save memory

% download model
download_general_model_for_xPC(tg, myModelName);


%% 6) set specific values IN THE DOWNLOADED TARGET MODEL (directly in xPC)
% using tg.setparam(tg.getparamid( 'mySubSystemname/.../blockName', 'PropertyName') ,PropertyValue);
% Since this affects the downloaded model, no recompilation is necessary at
% this step.

tg.SampleTime = myStepSize;
tg.setparam(tg.getparamid( 'ModelParaneters/myStepSize', 'Value'), myStepSize ); 

% set patch amplifier conversion factors
tg.setparam(tg.getparamid( 'Input//Output_DAQ/commandConversionFactor', 'Value'), commandConversionFactor ); 
tg.setparam(tg.getparamid( 'Input//Output_DAQ/PrimaryOutputConversionFactor', 'Value'), PrimaryOutputConversionFactor ); 
tg.setparam(tg.getparamid( 'Input//Output_DAQ/SecondaryOutputConversionFactor', 'Value'), SecondaryOutputConversionFactor ); 

% set AEC Kernel
tg.setparam(tg.getparamid( 'AEC/Kernel', 'Value'), Kernel ); 
tg.setparam(tg.getparamid( 'ModelParaneters/EnableKernel', 'Value'), enableAEC ); 

% synaptic variables
tg.setparam(tg.getparamid( 'GeneratePresynapticInputs/Rs', 'Value'), [R0_exc; R0_inh] ); 
tg.setparam(tg.getparamid( 'GeneratePresynapticInputs/g0s', 'Value'), [g_unit_exc; g_unit_inh] ); 
tg.setparam(tg.getparamid( 'GeneratePresynapticInputs/ts', 'Value'), [tau_exc; tau_inh] ); 
tg.setparam(tg.getparamid( 'GeneratePresynapticInputs/Ers', 'Value'), [E_rev_exc; E_rev_inh] ); 
tg.setparam(tg.getparamid( 'GeneratePresynapticInputs/seeds1', 'Value'), [intmax('int16')*rand(1,1); intmax('int16')*rand(1,1)] );



%% 7) Do what is necessary for starting the model and handle data/parameters/feedback/... DURING its running time
% Note: you can check if a model is running by strcmp(tg.Status,'running')
% and access values of signals too using tg.xxxxx
% NOTE: start model by tg.start;
% NOTE: wait for model to finish like this:
% while strcmp(tg.Status,'running')
%     pause(0.2);
% end

 tg.set('StopTime',runDuration); % total run time % IMPORTANT to set run duration right
 tg.start;
 disp('--> Model running, please wait..');
while strcmp(tg.Status,'running')
    pause(0.2);
end


%% 8) Do what is necessary after model finished running

% NOTE: if you recorded into a file, get it like this:
% xpcftp=tg.ftp;
% try 
%     xpcftp.get('xPCdata.dat');
% catch err
%     xpcftp.get('xPCdata.dat');
% end
% xpcFileData = readxpcfile('xPCdata.dat'); % Convert the data

% You can also read from xPC file system directly (faster). For this use
% [driveLetterFound, tgfs] = setup_fileSystem_xPC(tg, ''); to get tgfs (IF
% NOT DONE SO PREVIOUSLY!! ) and than:
% h = tgfs.fopen('xPCdata.dat');
% data = tgfs.fread(h);
% tgfs.fclose(h);
% xpcFileData  = readxpcfile(data);
% tgfs.removefile('xPCdata.dat');

% NOTE: if you recorded data to xPC memory with top level (highest level in model diagram) OUTPORT elements, get
% it like this:
% data = tg.OutputLog;

% Do not forget to save the data to the file format of your choice.

disp('--> Obtaining data from target, please wait..');

if logToMemoryFlag==1 % if we logged to memory, we save the data to the host
    measurementData = tg.OutputLog;
end

disp('--> DONE!');



%%% Store some things just to be sure
parameters.myModelName= myModelName;
parameters = [];
parameters.dt = myStepSize;
parameters.runDuration = runDuration;
parameters.AEC_Kernel = Kernel;
parameters.enableAEC = enableAEC;
parameters.systemtime = systemtime;
parameters.stimFileContents = stimFileContents;

parameters.R0_exc = R0_exc;
parameters.R0_inh = R0_inh ;
parameters.tau_exc = tau_exc;
parameters.tau_inh = tau_inh;
parameters.E_rev_exc = E_rev_exc;
parameters.E_rev_inh = E_rev_inh;
parameters.g_unit_exc = g_unit_exc;
parameters.E_rev_inh = E_rev_inh;

end