function getXPCtraces(tg, logFile)
% This function gets the traces from the xPC which belong to the recording
% run from which the logfile "logFile" originates from. It also converts
% the traces and places them into the same folder where "logFile" is.
% NOTE: "logFile" MUST HAVE FULL PATH!
% Note: "logFile" is usually named 'MYDATA.mat' and is generated by
% setup_probabilityClamp() at the end of its call.
% ** tg : is a reference to the xPC !

% Istvan Biro, 2012.08.02.


% open log file and see which files must be obtained
if exist(logFile,'file'), load(logFile); [mypath,NAME,EXT] = fileparts(logFile);
else [logFile, mypath] = uigetfile();
end

fileList = {};
for i=1:length(recording)
    fileList{i} = recording{i}.traceFile;
end

%get data logged to the files on xPC
xpcftp=tg.ftp;

for i=1:length(fileList)
    if ~strcmp(fileList{i},'none.a')  
        
        disp([' ... Get file : ' fileList{i}]);
        try
            xpcftp.get(fileList{i});
        catch err
            xpcftp.get(fileList{i});
        end
        
        %    xpcftp.delete(fileList{i});
        
        % mowe to the right place
        movefile(fileList{i},fullfile(mypath, fileList{i}) ); % xPC original file
        
        % convert data
        xpcFileData = readxpcfile(fullfile(mypath, fileList{i})); % Convert the data
        recording.approxSTGStimulus = xpcFileData.data(:,3);
        recording.measuredCurrent = xpcFileData.data(:,2);
        recording.measuredVoltage = xpcFileData.data(:,1);
%         recording.firedORnot =  xpcFileData.data(:,6) ;
%         recording.slidingFiringProbability = xpcFileData.data(:,5);
        
        save(fullfile(mypath, [fileList{i}(1:end-2) '.mat'] ), 'recording');
        delete(fullfile(mypath, fileList{i}));
        
    end
    
end



end
