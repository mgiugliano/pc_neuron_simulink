function [DeviceID] = STG_Prepare(STG_serial)
%DeviceID] = STG_Prepare(STG_serial)
%
% opens connection to device and prepare it (libraries, memory management, trigger setup, etc...).

% Istvan Biro, 2012.03.02.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% PREPARE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~exist('STG_serial','var'), STG_serial = '9058-0163'; end

verbose = 0;

mainpath = fullfile(pwd(),'STG');



%%%%%%%%%%%%%%%%%% END PREPARE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% SET MAPS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Input for STG control by Istvan Biro
%%% Use such a file for EACH STG device
%%% Use names of format input_param[_optionals].m only and make sure they are called in initiate_device.m!
%%% Complete till /*\/*\/*\/*\ line, all below is computed automatically
%%
%* ****************VERBOSE OUT? 0=no 1=yes**********
if (verbose == 1), fprintf(1,'%s - %s\n',STG_serial,'Verbose: on.'); end
%* *************************************************


%%
%* *************************************************
%* ****************SYNC OUT SIGNAL******************
% after a syncout is triggered it should give a TTL signal at the times
% given below (put in as many as you wish) BUT all MUST have the same nr of
% points!
so_signal_length = 0.5; % length of TTL pulse in s (! must be multiple of 20us)
% --times of sending pulse (after being triggered) in s ---->
syncout_signals = [  ; ... % syncout 1
    ; ... % syncout 2
    ; ... % syncout 3
    ];    % syncout 4

%%
%* *************************************************
%* ****************SETTING TRIGGERS*****************
% * CHANNELMAP - complete the [channel trigger] assignment table as: 1:channel linked to trigger, 0-not linked;
% ----channel-1-2-3-4-5-6-7-8-------->
chMapFull = [ 1 1 1 1 1 1 1 1 ; ...  % trigger1 (linked to or not)
    0 0 0 0 0 0 0 0 ; ...  % trigger2
    0 0 0 0 0 0 0 0 ; ...  % trigger3
    0 0 0 0 0 0 0 0 ];     % trigger4

% * SYNCOUTMAP - link syncOut to triggers; 1=yes, 0=no.
% ----syncOut------1-2-3-4---------->
syncOutMapFull = [ 1 1 1 1 ; ... % trigger1 (linked to or not)
    0 0 0 0 ; ... % trigger2
    0 0 0 0 ; ... % trigger3
    0 0 0 0 ];    % trigger4

% * DIGOUTMAP - link digOut to triggers; 1=yes, 0=no.
% ----digOut------1-2-3-4---------->
digOutMapFull = [ 1 0 0 0 ; ... % trigger1 (linked to or not)
    0 0 0 0 ; ... % trigger2
    0 0 0 0 ; ... % trigger3
    0 0 0 0 ];    % trigger4

% * REPEATMAP how namy times to repeat trigger (0-infinite);
% ----trigger-----1-2-3-4---------->
repeatMapFull = [ 1 1 1 1];

% * STRART MAP 1-start trigger immediately after upload, 0=start by other means;
% ----trigger----1-2-3-4---------->
startMapFull = [ 0 0 0 0];



%%
%* *************************************************
%* ****************SETTING MEMORY ******************
% UNCERTAIN if works, must be tested first --> commented out in
% initiate_device.m !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% ----channel-------1-------2-------3-------4-------5-------6-------7-------8-->
channelMemory = [ 1223338 1223338 1223338 1223338 1223338 1223338 1223338 1223338 ];

% ----channel-------1-------2-------3-------4---->
syncOutMemory = [ 1223338 1223338 1223338 1223338 ];




%%
% /*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\
% /*\/*\/*\/*\/*\/*\/*\/*\PERFORMING CONVERSIONS/*\/*\/*\/*\/*\/*\/*\/*\/*\
% /*\/*\/*\/*\/*\/*\/*\/*\ !!!DO NOT MODIFY!!!! /*\/*\/*\/*\/*\/*\/*\/*\/*\

% memory data
channelCapacity = libpointer('uint32Ptr', channelMemory);
syncOutCapacity = libpointer('uint32Ptr', syncOutMemory);
chnomem = length(channelMemory);
sonomem = length(syncOutMemory);

% converting to binary encoding of channel assignements for each trigger
% and setting channelmap
[no_of_triggers no_of_channels] = size(chMapFull);
twos = 2.*ones(1,no_of_channels);
exps = [0:(no_of_channels-1)];
twosexps = twos .^ exps;
ChannelMap = twosexps * transpose(chMapFull);
if (verbose == 1), fprintf(1,'%s - ChannelMap: %s\n', STG_serial,num2str(ChannelMap)); end

% converting syncOutMap
[no_of_triggers no_of_syncouts] = size(syncOutMapFull);
twos = 2.*ones(1,no_of_syncouts);
exps = [0:(no_of_syncouts-1)];
twosexps = twos .^ exps;
SyncOutMap = twosexps * transpose(syncOutMapFull);
if (verbose == 1), fprintf(1,'%s - SyncOutMap: %s\n', STG_serial,num2str(SyncOutMap)); end

% converting digOutMap
[no_of_triggers no_of_digouts] = size(digOutMapFull);
twos = 2.*ones(1,no_of_digouts);
exps = [0:(no_of_digouts-1)];
twosexps = twos .^ exps;
DigoutMap = twosexps * transpose(digOutMapFull);
if (verbose == 1), fprintf(1,'%s - DigoutMap: %s\n', STG_serial,num2str(DigoutMap)); end

% converting repeatMapFull
RepeatMap = repeatMapFull;
if (verbose == 1), RepeatMap; end

% converting startMapFull            ?????????????
AutostartMap = startMapFull;
if (verbose == 1), fprintf(1,'%s - AutostartMap: %s\n', STG_serial,num2str(AutostartMap)); end




%%%%%%%%%%%%%%%%%% END SET MAPS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% prepare syncout data %%%%%%%%%%%%%%%%%%%%%%%

soutData = {};
soutTime = {};
syncout_signals = sort(syncout_signals * 1000000,2); % pulse times in us
so_signal_length = so_signal_length* 1000000; % pulse duration in us
[tr tc] = size(syncout_signals);
tmpd = zeros(tr,2*tc);
tmpt = zeros(size(tmpd));
for i=1:tr
    for j=1:tc
        tmpd(i,2*j) = 1;
        tmpt(i,2*j) = so_signal_length;
        % duration of 0 signal before
        if j==1, tbeg = 0; else tbeg = syncout_signals(i,j-1) + so_signal_length; end
        tend = syncout_signals(i,j);
        tmpt(i,2*j-1) = tend - tbeg;
    end
    soutTime{i} = [tmpt(i,:)];
    soutData{i} = [tmpd(i,:)];
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% load libraries and connect %%%%%%%%%%%%%%%%%

if ~libisloaded('McsUsbDLL')
    loadlibrary('McsUsbDLL', 'McsUsbDLL.h','alias','McsUsbDLL')
end

if (exist('DeviceID','var') == 1) && (DeviceID ~= 0)
    fprintf(1, 'Closing existing connection\n')
    calllib('McsUsbDLL', 'McsUsb_Disconnect', DeviceID);
    DeviceID = 0;
end

DeviceID = calllib('McsUsbDLL', 'STG200x_Connect', -1, STG_serial); % connecting to a certain device via the serial number



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% apply settings %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (DeviceID ~= 0)
    calllib('McsUsbDLL', 'STG200x_DisableStreamingMode', DeviceID);
    calllib('McsUsbDLL', 'STG200x_DisableStreamingMode', DeviceID);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%% set memory %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % ---- not tested, leaving with defaults: 1223338bytes for each
    % channel ! no error at overwriting !
    
    if (verbose == 1), fprintf(1,'%s - Setting channel memories: %s\n  - Setting syncout memories: %s\n', STG_serial,num2str(channelCapacity.val), num2str(syncOutCapacity.val)); end
    mmm= calllib('McsUsbDLL','STG200x_SetDownloadCapacity', DeviceID, chnomem, channelCapacity, sonomem, syncOutCapacity);
    % unsigned int USBAPI STG200x_SetDownloadCapacity(usb_t device, uint32
    % channels, uint32 *channel_cap, uint32 sync_out, uint32 *syncout_cap);
    
    real_channel_no = libpointer('uint32Ptr', 0);
    mmm = calllib('McsUsbDLL','STG200x_GetNumberOfAnalogChannels', DeviceID, real_channel_no);
    real_syncout_no = libpointer('uint32Ptr', 0);
    mmm = calllib('McsUsbDLL','STG200x_GetNumberOfSyncoutChannels', DeviceID, real_syncout_no);
    real_trigger_no = libpointer('uint32Ptr', 0);
    mmm = calllib('McsUsbDLL','STG200x_GetNumberOfTriggerInputs', DeviceID, real_trigger_no);
    if (verbose == 1), fprintf(1,'%s - Channels: %d, Triggers: %d, SyncOuts: %d.\n', STG_serial,real_channel_no.val,real_trigger_no.val,real_syncout_no.val); end
    
    real_channel_memory = libpointer('uint32Ptr', zeros(1,real_channel_no.val));
    real_syncout_memory = libpointer('uint32Ptr', zeros(1,real_syncout_no.val));
    mmm= calllib('McsUsbDLL','STG200x_GetDownloadCapacity', DeviceID, real_channel_no.val, real_channel_memory, real_syncout_no.val, real_syncout_memory );
    if (verbose == 1), fprintf(1,'%s - New channel memories: %s\n - New syncout memories: %s\n', STG_serial,num2str(real_channel_memory.val), num2str(real_syncout_memory.val)); end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%% set triggers & syncOut %%%%%%%%%%%%%%%%%%
    
    myChannelMapPtr = libpointer('uint32Ptr', ChannelMap);
    mySyncOutMapPtr = libpointer('uint32Ptr', SyncOutMap);
    myRepeatMapPtr = libpointer('uint32Ptr', RepeatMap);
    myDigoutMapPtr = libpointer('uint32Ptr', DigoutMap);
    myAutostartMapPtr = libpointer('uint32Ptr', AutostartMap);
    
    elements = 4;
    
    % unsigned int USBAPI STG200x_SetupTrigger(usb_t device, uint32 elements, uint32 *channelmap, uint32 *syncoutmap, uint32 *digoutmap, uint32 *autostart, uint32 *repeat);
    res = calllib('McsUsbDLL', 'STG200x_SetupTrigger', DeviceID, elements, myChannelMapPtr, mySyncOutMapPtr, myDigoutMapPtr, myAutostartMapPtr, myRepeatMapPtr );
    
    % unsigned int USBAPI STG200x_GetTrigger(usb_t device, uint32 elements, uint32 *channelmap, uint32 *syncoutmap, uint32 *digoutmap, uint32 *autostart, uint32 *repeat);
    res = calllib('McsUsbDLL', 'STG200x_GetTrigger', DeviceID, elements, myChannelMapPtr, mySyncOutMapPtr, myDigoutMapPtr, myAutostartMapPtr, myRepeatMapPtr );
    
    % print out maps
    if (verbose==1)
        fprintf(1, 'Device %s - ChannelMap: %s \n', STG_serial, num2str(myChannelMapPtr.val));
        fprintf(1, 'Device %s - SyncOutMap: %s \n', STG_serial, num2str(mySyncOutMapPtr.val));
        fprintf(1, 'Device %s - DigoutMap: %s \n', STG_serial, num2str(myDigoutMapPtr.val));
        fprintf(1, 'Device %s - AutostartMap: %s \n', STG_serial, num2str(myAutostartMapPtr.val));
        fprintf(1, 'Device %s - RepeatMap: %s \n', STG_serial, num2str(myRepeatMapPtr.val));
    end
    
    
    % clear all previous data from all 8 channels
    for i=0:7, res = calllib('McsUsbDLL', 'STG200x_ClearChannelData', DeviceID, i); end

    
else
    fprintf (1,'Cant connect to STG\n');
end






end % function
