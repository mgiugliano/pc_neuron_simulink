
#include "rando.h"



double OU_oneStep(double previous_value, double tmp1, double tmp2, double tmp3, long enforced_seed , int thisSignalNo) {
// Generates the next value of the required noisy signal, depending on the previous one.	
// INPUT PARAMETERS MUST BE:
	// previous_value : previously generated data point - use mean for the first call
	// tmp1 = dt / tau [DIMENSIONLESS!!]
	// tmp2 = mean * tmp1;
	// tmp3 = stdv * sqrt(2.*tmp1);
	// enforced_seed: enforce the seed for the next step -- pass the initialization seed on the first call of the function and then -1 at each call after the first one, to get a reproducible sequence depending only on the initial seed
	// int thisSignalNo : the index of the signal that you want to make at this call. (number of possible signals to be set in rando.h). 

double new_value;
long oldSeed;

if (enforced_seed>0){                            // set the seed of this step if required
	oldSeed = mysrand49(enforced_seed, thisSignalNo);
}

//printf("%f\n",previous_value);
  								                              // the appropriate value of the state var, coming from the 
new_value = previous_value + tmp2 - (tmp1 * previous_value) + tmp3 * gauss(thisSignalNo);  // implemented Ornstein-Ulhenbeck stochastic diff. equation.

return new_value;
} 


double OU_oneStep_forSimulinkSfunctionBuilder(double tmp1, double tmp2, double tmp3, long init_seed, int thisSignalNo, int resetEvent){
// version of OU_oneStep intended for building into a simulink model - see comment sin code (to see why)
	
	static double number[signalNumber];       // static for remembering the last value from previous call of this function
	static long counter[signalNumber];       // initialized ONCE to 0 at first call, then counting calls

	if (resetEvent==1) {
		counter[signalNumber] = 0;
	}

	switch (counter[thisSignalNo]){
	case 0:                        // if called first time, one must return the mean(=tmp2/tmp1) value
		number[thisSignalNo] = tmp2/tmp1;
		break;
	case 1:                        // if called the second time, OU_oneStep will get the mean as previous value value AND will initialize the seed
		number[thisSignalNo] = OU_oneStep(tmp2/tmp1, tmp1, tmp2, tmp3, init_seed, thisSignalNo );
		break;
	default:                      // later calls will not change the seed (i.e. let the seed be changed in the random generation algortims only) --> reproducible trace
		number[thisSignalNo] = OU_oneStep(number[thisSignalNo], tmp1, tmp2, tmp3, -1 , thisSignalNo);
		break;
	}

	counter[thisSignalNo] = counter[thisSignalNo] + 1;
	

	return number[thisSignalNo];
}


void OU_generator_oneSignal(double dt, double time, double mean, double stdv, double tau, long init_seed, double *data_pointer , int thisSignalNo ) {
// Generates a sequence of Ornstein Uhlenbeck signal points - use for recreation of signal based on the initialization seed.	
// INPUT PARAMETERS MUST BE:
	// dt: timestep in [s]
	// time: duration of trace in [s] (will be upwrsd rounded to multiples of dt)
	// mean: mean value of signal
	// stdv: standard deviation of signal
	// tau: autocorellation time of signal in [ms]
	// init_seed: initialization seed - for initialization of random number generator (resulting in reproducible trace)
	// int thisSignalNo : the index of the signal that you want to make at this call. (number of possible signals to be set in rando.h). 

	int nn, i;
	double tmp1, tmp2, tmp3;

	nn = (int)ceil(time/dt);
	tmp1 = dt *1000./ tau;
	tmp2 = mean * tmp1;
	tmp3 = stdv * sqrt(2.*tmp1);

	//the first signal point shall be the mean itself
	data_pointer[0] = mean;
	// output of data

	//the first call of OU_oneStep (second signal point) will have the mean as previous value AND will initialize the seed 
	if (nn>1){
		data_pointer[1] =  OU_oneStep(data_pointer[0], tmp1, tmp2, tmp3, init_seed , thisSignalNo);
		// output of data
	}

	//further calls will not change the seed (i.e. let the seed be changed in the random generation algortims only) --> reproducible trace
	for (i=3;i<=nn;i++) {
		data_pointer[i-1] =  OU_oneStep(data_pointer[i-2], tmp1, tmp2, tmp3, -1 , thisSignalNo);
		// output of data
	}

}




void OU_generator(double dt, double time, double *mean, double *stdv, double *tau, long *init_seed, double **data_pointer) {
	// Generates a sequence of Ornstein Uhlenbeck signal points - use for recreation of signal based on the initialization seed.	
	// CREATES AS MANY SIGNALS AS  set in rando.h. BE CAREFUL TO HAVE TH RIGHT NUMBER OF INPUTS!!!!!
// INPUT PARAMETERS MUST BE:
	// dt: timestep in [s]. same dt for ALL signals
	// time: duration of trace in [s] (will be upwrsd rounded to multiples of dt) --  same time for ALL signals
	// *mean: pointer to mean value of signal, mean[1] is for signal number 1, ... and so on, till signalNumber (defined in rando.h).
	// *stdv: pointer to standard deviation of signal
	// *tau: pointer to autocorellation time of signal in [ms]
	// *init_seed: pointer to initialization seed - for initialization of random number generator (resulting in reproducible trace)
	
	double  *datap;
	int nn,i,j;

	// space for one signal
	nn = (int)ceil(time/dt);
	datap = (double *) malloc( nn * sizeof (double));

	// creating all signals and saving them into data_pointer
	for (i=0;i<signalNumber;i++){ // for all signals
		OU_generator_oneSignal(dt, time, mean[i], stdv[i], tau[i], init_seed[i], datap , i );
		//save data
		for (j=0;j<nn;j++) {
			data_pointer[i][j] = datap[j];
		}
	} // end i:  for all signals
}



void OU_generator_toFile(double dt, double time, double *mean, double *stdv, double *tau, long *init_seed, char *fileName){
	// Generates a sequence of Ornstein Uhlenbeck signal points - use for recreation of signal based on the initialization seed.	
	// CREATES AS MANY SIGNALS AS  set in rando.h. BE CAREFUL TO HAVE THE RIGHT NUMBER OF INPUTS!!!!! - see OU_generator() for details
	
	int i,nn,j;
	double **datap;
	FILE *fp;

	// allocate memory for all signals
	nn = (int)(ceil(time/dt));
	datap = (double **) malloc( signalNumber * sizeof (double*));
	for (i=0;i<signalNumber;i++){
		datap[i] = (double *) malloc( nn * sizeof (double));
	}

	// make all signals
	OU_generator( dt,  time,  mean,  stdv,  tau,  init_seed, datap  );

	//============== write to a text file =================================

	fp = fopen(fileName,"w+");

	for (j=0;j<nn;j++){
		for (i=0;i<signalNumber;i++){
			fprintf(fp,"%.15f\t",datap[i][j]);
		}
		fprintf(fp,"\n");
	}

	fclose(fp);

	//=====================================================================

	free(datap);

}
