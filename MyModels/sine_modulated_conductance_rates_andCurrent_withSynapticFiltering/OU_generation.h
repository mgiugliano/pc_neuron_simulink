
#include "rando.h"


double OU_oneStep_forSimulinkSfunctionBuilder(double tmp1, double tmp2, double tmp3, long init_seed, int thisSignalNo ,  int resetEvent);
// version of OU_oneStep intended for building into a simulink model - see comment sin code (to see why)
// bool resetEvent: if 1, the generation mechanism is reset (seed reinitialized to init_seed provided at the call when resetEvent=1 provided).

double OU_oneStep(double previous_value, double tmp1, double tmp2, double tmp3, long enforced_seed , int thisSignalNo) ;
// Generates the next value of the required noisy signal, depending on the previous one.	
// INPUT PARAMETERS MUST BE:
	// previous_value : previously generated data point - use mean for the first call
	// tmp1 = dt / tau [DIMENSIONLESS!!]
	// tmp2 = mean * tmp1;
	// tmp3 = stdv * sqrt(2.*tmp1);
	// enforced_seed: enforce the seed for the next step -- pass the initialization seed on the first call of the function and then -1 at each call after the first one, to get a reproducible sequence depending only on the initial seed
	// int thisSignalNo : the index of the signal that you want to make at this call. (number of possible signals to be set in rando.h). 


void OU_generator_oneSignal(double dt, double time, double mean, double stdv, double tau, long init_seed, double *data_pointer , int thisSignalNo) ;
// Generates a sequence of Ornstein Uhlenbeck signal points - use for recreation of signal based on the initialization seed.	
// INPUT PARAMETERS MUST BE:
	// dt: timestep in [s]
	// time: duration of trace in [s] (will be upwrsd rounded to multiples of dt)
	// mean: mean value of signal
	// stdv: standard deviation of signal
	// tau: autocorellation time of signal in [ms]
	// init_seed: initialization seed - for initialization of random number generator (resulting in reproducible trace)
	// int thisSignalNo : the index of the signal that you want to make at this call. (number of possible signals to be set in rando.h). 
	


void OU_generator(double dt, double time, double *mean, double *stdv, double *tau, long *init_seed, double **data_pointer) ;
	// Generates a sequence of Ornstein Uhlenbeck signal points - use for recreation of signal based on the initialization seed.	
	// CREATES AS MANY SIGNALS AS  set in rando.h. BE CAREFUL TO HAVE TH RIGHT NUMBER OF INPUTS!!!!!
// INPUT PARAMETERS MUST BE:
	// dt: timestep in [s]. same dt for ALL signals
	// time: duration of trace in [s] (will be upwrsd rounded to multiples of dt) --  same time for ALL signals
	// *mean: pointer to mean value of signal, mean[1] is for signal number 1, ... and so on, till signalNumber (defined in rando.h).
	// *stdv: pointer to standard deviation of signal
	// *tau: pointer to autocorellation time of signal in [ms]
	// *init_seed: pointer to initialization seed - for initialization of random number generator (resulting in reproducible trace)
	
	
void OU_generator_toFile(double dt, double time, double *mean, double *stdv, double *tau, long *init_seed, char *fileName);
	// Generates a sequence of Ornstein Uhlenbeck signal points - use for recreation of signal based on the initialization seed.	
	// CREATES AS MANY SIGNALS AS  set in rando.h. BE CAREFUL TO HAVE TH RIGHT NUMBER OF INPUTS!!!!! - see OU_generator() for details
	
	