#include "rando.h"


#define MM 714025
#define IA 1366
#define IC 150889

// long rand49_idum[signalNumber];//-77531;


float drand49(int thisSignalNo)	{
        static long iy[signalNumber],ir[signalNumber][98];
        static int iff[signalNumber];
        int j;

    if (rand49_idum[thisSignalNo] < 0 || iff[thisSignalNo] == 0) {
            iff[thisSignalNo]=1;
            if((rand49_idum[thisSignalNo]=(IC-rand49_idum[thisSignalNo]) % MM)<0)
                             rand49_idum[thisSignalNo]=(-rand49_idum[thisSignalNo]);
            for (j=1;j<=97;j++) {
                    rand49_idum[thisSignalNo]=(IA*(rand49_idum[thisSignalNo])+IC) % MM;
                    ir[thisSignalNo][j]=(rand49_idum[thisSignalNo]);
            }
            rand49_idum[thisSignalNo]=(IA*(rand49_idum[thisSignalNo])+IC) % MM;
            iy[thisSignalNo]=(rand49_idum[thisSignalNo]);
        }
        j=1 + 97.0*iy[thisSignalNo]/MM;
    if (j > 97 || j < 1) printf("RAN2: This cannot happen.");
        iy[thisSignalNo]=ir[thisSignalNo][j];
        rand49_idum[thisSignalNo]=(IA*(rand49_idum[thisSignalNo])+IC) % MM;
        ir[thisSignalNo][j]=(rand49_idum[thisSignalNo]);
        return (float) iy[thisSignalNo]/MM;
} // end drand49()
//----------------------------------------------------------------


//----------------------------------------------------------------
float srand49(long seed, int thisSignalNo) {
   rand49_idum[thisSignalNo]=(-seed);
   return drand49(thisSignalNo);
} // end srand49()
//----------------------------------------------------------------


//----------------------------------------------------------------
long mysrand49(long seed, int thisSignalNo) {
  long temp;
  temp = -rand49_idum[thisSignalNo];
  rand49_idum[thisSignalNo] = (-seed);
  return temp;
} // end mysrand49()
//----------------------------------------------------------------


//----------------------------------------------------------------
float gauss(int thisSignalNo)	{
    static int iset[signalNumber];
    static float gset[signalNumber];
    float fac,r,v1,v2;
	
    if  (iset[thisSignalNo] == 0) {
        do {
            v1=2.0*drand49(thisSignalNo)-1.0;
            v2=2.0*drand49(thisSignalNo)-1.0;
            r=v1*v1+v2*v2;
        } while (r >= 1.0);
        fac=sqrt(-2.0*log(r)/r);
        gset[thisSignalNo]=v1*fac;
        iset[thisSignalNo]=1;
        return v2*fac;
    } else {
        iset[thisSignalNo]=0;
        return gset[thisSignalNo];
    }
} // end gauss()



#undef MM
#undef IA
#undef IC
