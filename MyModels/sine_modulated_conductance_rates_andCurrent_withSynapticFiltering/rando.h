#include <math.h>
#include <stdio.h>
#include <stdlib.h>


/* 
   float drand49():      returns a uniformly distributed (pseudo)random (float) number between 0.0 and 1.0 
   float gauss():        returns a Gauss-distributed (pseudo)random (float) number with zero mean and unitary variance
   float srand49(long):  inits the 'seed' and returns a.... between 0.0 and 1.0 (from Numerical Recipes) 
   long mysrand49(long): inits the 'seed' and returns the *previous* seed (custom made!)
*/
   
#define signalNumber 2

long rand49_idum[signalNumber];



//----------------------------------------------------------------
float drand49(int thisSignalNo)	;

//----------------------------------------------------------------


//----------------------------------------------------------------
float srand49(long seed, int thisSignalNo);
//----------------------------------------------------------------


//----------------------------------------------------------------
long mysrand49(long seed, int thisSignalNo);
//----------------------------------------------------------------


//----------------------------------------------------------------
float gauss(int thisSignalNo);
//----------------------------------------------------------------

