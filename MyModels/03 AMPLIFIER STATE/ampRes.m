% conversion factors
commandCurrentSensitivity = 4000; %2000 %400;  % in [pA/V], THIS MUST EQUAL to I sensitivity setting of amplifier CMD, e.g. 400pA/V, i.e. 400pA to be injested for each volt of command
% -- this should normally correspond to = 1 / (Mike's AFS File to DAC pA/V seeting)
commandVoltageSensitivity = 20;   % in [mA/V], THIS MUST EQUAL to V sensitivity setting of amplifier CMD, e.g. 20mV/V, i.e. 20mV to be injested for each volt of command
% -- this should normally correspond to = 1 / (Mike's AFS File to DAC mV/V seeting)
voltageSensitivity = 50;          % in [mV/mV] = [V/V], THIS MUST EQUAL to sensitivity setting of amplifier PRIMARY out , e.g. 50mV/mV, i.e. 1mV of real signal for every 50mV of amplifer output
% -- this should normally correspond to =  1000 / (Mike's AFS ADC to File mV/V seeting)
currentSensitivity = 0.25/1000 %0.005;       % in [V/pA], THIS MUST EQUAL to sensitivity setting of amplifier SECONDARY out , e.g. 0.005V/pA, i.e. 1pA of real signal for every 0.005V of amplifer output
% -- this should normally correspond to =  1 / (Mike's AFS ADC to File pA/V seeting)
