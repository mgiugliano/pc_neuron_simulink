function [srate N M time data] = load_binary(fname)
%
% [srate N M time data] = load_binary(file_name)
%
% srate [Hz] : sampling rate
% N          : number of simultaneous waves (channels)
% M          : number of samples per wave (channel)
%
% time   [s] : vector M x 1, containing the time axis
% data       : vector M x N, containing the data
%
%
%
% Nov 4th 2009 - Michele Giugliano, PhD - mgiugliano@gmail.com
%
srate = -1;                     % default value
N     = -1;                     % default value 
M     = -1;                     % default value 
time  = [];                     % default value 
data  = [];                     % default value
if ~exist(fname, 'file')
 disp(sprintf('load_binary() - error: file <%s> not found!', fname));
 return;
end

fp = fopen(fname, 'r');        % file is open for read-only

srate = fread(fp, 1, 'double');% sampling rate [Hz]
dt    = 1./srate;              % sampling interval [s]
N     = fread(fp, 1, 'ulong'); % number of simultaneous waves (channels)
M     = fread(fp, 1, 'ulong'); % number of samples per wave (channel)
maxT  = (M-1)*dt;              % largest sampling time [s]

time  = 0:dt:maxT;
data  = zeros(M,N);            % initialization, improves performances
for i=1:N,
 data(:,i) = fread(fp, M, 'double');
end


fclose(fp);                     % file is released
