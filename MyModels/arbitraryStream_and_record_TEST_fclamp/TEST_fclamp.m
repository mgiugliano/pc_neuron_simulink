% TEST f-clamp in cc
[tg measurementData StreamValues parameters] = setup_arbitraryStream_and_record_FiringClampTest('zero.stim');
voltage = measurementData(:,1);
current = measurementData(:,2);
fMeasured = measurementData(:,3);
IClamp = measurementData(:,4);
time = [0:parameters.dt:parameters.dt*(length(voltage)-1)];
save('FclampTestData.mat', 'voltage', 'current', 'fMeasured', 'IClamp', 'parameters', 'StreamValues');
fig = figure(); hold all;
plot(time,fMeasured);