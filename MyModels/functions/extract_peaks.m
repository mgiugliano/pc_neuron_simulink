function out = extract_peaks(data, threshold)
% peaks_indexes = extract_peaks(data_waveform, threshold)
%
% note: this is based on (positive slope) threshold crossing detections..
%
% Simplified version - this extracts the +peaks, returning just the indexes
% corresponding to those maxima which are between two successive threshold
% crossings..
%
% Aug 22nd 2007 - Michele Giugliano
%

N    = length(data);        % Number of points in the data waveform..
bool = 0;                   % Useful boolean variable for threshold-cross
out  = [];                  % Vector containing first the indexes of 
                            % threshold crossings and later of the peaks..

for i=1:N,                  % Detection of threshold crossings..
 if ((data(i) > threshold) & (~bool)), bool = 1; out = [out, i]; end % if
 if ((data(i) < threshold) & (bool)),  bool = 0;                 end % if
end % for i

M    = length(out);         % Number of (positive slope) threshold crossing
peak = [];                  % Temporary data structure..

for k=1:M,                  
 istart = out(k);
 if (k == M), istop = N; else istop = out(k+1); end;
 chunk  = data(istart:istop);   % An index interval is set for each event..
 tmq    = find(chunk == max(chunk));
 peak   = [peak, (istart + tmq(1) - 1)];
end

out = peak;            % Indexes corresponding to peaks only are returned.
end
