function [tg stimuli probabilities times picLocation] = doResponseClampReplay( )
% Does a replay of a previously done clamp of firing response probability by applying the same stimuli. 

% load cell folder location
A_OUTPUT_LOCATION

%% ask fot he log file of the previous experiment ( usually named 'MYDATA.mat' ).
[FileName,PathName] = uigetfile(cellFolder);
getDataNow = 0;                    % transfer traces from target immediately at end. (1 yes 2 means no)






%% USUALLY YOU DO NOT CHANGE THE PART BELOW ! ! ! ! ! !
%% SETTINGS AND PARAMETERS -- this time they are loaded from previous experiment's settings!!!!!!!!!

load(fullfile(PathName,FileName));

% STIMULI TO APPLY 
PlaybackModeSTGvalues = parameters.extracellStim.stimulusAmplitudes;

kernelfile = parameters.kernelfile;            % name of kernel file to use

% DURATION
runDuration = parameters.runDuration;          % [s] duration of run

 % EXTRACELLULAR STIMULUS
InterstimInterval = parameters.extracellStim.InterstimInterval;       % [s] interstimulus interval
firstSTGstimulusValue = parameters.extracellStim.firstSTGstimulusValue;   % [uA] OR [mV] fist STG stimulus value (to start from).
LIMITSTGstimulusValue = parameters.extracellStim.LIMITSTGstimulusValue;   % [uA] OR [mV] maximal possible STG stimulus value (to start from).

% PROBABILITY CLAMP
targetProbability = parameters.probabilityClamp.targetProbability; % [0 to 1] firing probability to keep
P_stg = parameters.probabilityClamp.P_stg;                 % P of a PID controller
I_stg = parameters.probabilityClamp.I_stg;                 % I of a PID controller
probToStimAmpltude = parameters.probabilityClamp.probToStimAmpltude;   % "gain", i.e. conversion of Probability-->nextSTGstimulus calculation
tau_history = parameters.probabilityClamp.tau;        % [s] decay time constant of history

% SPIKE DETECTION 
threshold = parameters.spikeDetection.threshold;               % [mV] spike detection by threshold crossing

% VOLTAGE HOLDING - apply current to hold voltage at vHold, except at
% stimulus and recording events - done by PI controller
enableVoltageHold = parameters.voltageClamping.enableVoltageHold;      % 1 or 0 for enabling or disabling feature
vHold = parameters.voltageClamping.vHold;                % [mV] voltage to hold by current injection


% OTHER
tag = 'PclampReplay_';                   % tag for folder name (experiment distingisher or comment)


%% DO IT

% load cell folder location
A_OUTPUT_LOCATION

% destinationFolder = fullfile('Recordings', 'StabilityTests');
ttt = clock;
tstamp = [num2str(ttt(1)) num2str(ttt(2)) num2str(ttt(3)) num2str(ttt(4)) num2str(ttt(5)) num2str(ceil(ttt(6)))];
destinationFolder=fullfile(cellFolder,[tag tstamp]);

% set destionation
addpath('functions');
A_OUTPUT_LOCATION

here = pwd;
cd(frequencyClampDir);


% do clamp
[state, stimuli, probabilities, allFiredOrNot, times , TET,dt, tg] = setup_probabilityClamp(runDuration,...
    'InterstimInterval', InterstimInterval, 'firstSTGstimulusValue', firstSTGstimulusValue, 'LIMITSTGstimulusValue', LIMITSTGstimulusValue, ...
    'targetProbability',targetProbability, 'P_stg',P_stg, 'I_stg',I_stg, 'probToStimAmpltude',probToStimAmpltude, 'tau_history',tau_history, ...
    'threshold',threshold, 'enableVoltageHold',enableVoltageHold, 'vHold', vHold, ...
    'destinationFolder', destinationFolder, 'getDataNow', getDataNow,...
    'PlaybackModeSTGvalues', PlaybackModeSTGvalues);

% make a plot
ff=figure(); subplot(2,1,1); ylabel('stimulus [uA]')
plot(times,stimuli)
subplot(2,1,2); plot(times,probabilities);  ylabel('probability'); xlabel('time (s)');
saveas(gcf, fullfile(destinationFolder,['PClampReplay_' tstamp]),'fig')
picLocation = fullfile(destinationFolder,['PClampReplay_' tstamp '.png']);
print(gcf, picLocation, '-dpng', '-loose');
%close(ff);

% add a note of the replayed file
originalFile = fullfile(PathName,FileName);
save(fullfile(destinationFolder, 'thisIsReplayOfFile.mat'), 'originalFile');

% go back
cd(here);

end


