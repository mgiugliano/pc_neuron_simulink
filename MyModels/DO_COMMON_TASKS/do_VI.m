function [R outFolder] = do_VI()
% This function will do a V(I) curve of the patched neuron.
% It uses the model arbitraryStream_and_record.
% Output folder will be set according to settings in A_OUTPUT_LOCATION.m,
% in the directory of the neuron (cellFolder), under VI_<time> and in a
% folder named from the execution time of the script.


% Istvan Biro, 2014.05.26. (last update).

%% Parameters

shuffleFlag = 0; % shuffle (1) or not (0) the input values

threshold = 0; % spike detection threshold

inputCurrents = [-200:25:25]; % vector of current steps in pA --- these will be done "repeatTimes" times. E.g.: [-200:50:50];
repeatTimes = 1;
ignoreSpikyTracesFlag = 1; % 1 or 0. 1 will ignore any trace with action potentials.

waitBetweenTrials = [0]; % one number or a vector of length(inputCurrents) saying how long to wait after each trial

kernelfile = '';  % leave '' for no AEC

dataFileName = 'RECORDINGS.mat'; % each recording will be in a folder named by the time, containing a file named like this.

destination_config_file = '.\A_OUTPUT_LOCATION.m';  % name of file containing destination paths and additional configurations

apmilifierConfigFile = '.\..\..\HARDWARE_CONFIGURATION\patchAmplifier_CClamp.m';  % file that stores conversion factors for amplifier - MUST HAVE PATH RELATIVE TO THE MODEL FILE LOCATION!!!!!!!!!

%% Prepare
R = []; outFolder = '';

% read destination and parameters file
run(destination_config_file);

% settle waiting times
if length(waitBetweenTrials)==1, waitBetweenTrials = repmat(waitBetweenTrials,1,length(inputCurrents));
elseif length(waitBetweenTrials)~= length(inputCurrents), error(' Waiting times are wrong, give one number or a vector of same length as current steps!');
end

% get all current steps
[rr cc] = size(inputCurrents);
if rr>cc, inputCurrents=inputCurrents'; end
inputCurrents = repmat(inputCurrents,1,repeatTimes);
[rr cc] = size(waitBetweenTrials);
if rr>cc, waitBetweenTrials=waitBetweenTrials'; end
waitBetweenTrials = repmat(waitBetweenTrials,1,repeatTimes);

% shuffle if needed
if shuffleFlag==1
     ss = length(inputCurrents);
     ind = randperm(ss);
     inputCurrents = inputCurrents(ind);
     waitBetweenTrials = waitBetweenTrials(ind);
end

% get time and make output file name
systemtime=clock;
timestr = sprintf('%04d%02d%02d%02d%02d%02d%02d',systemtime(1),systemtime(2),systemtime(3),systemtime(4),systemtime(5),floor( systemtime(6) ), ceil(mod( systemtime(6),100 )) ); % name YYYMMDDHHMMSSdd
outFolder = fullfile(cellFolder, [VI_Folder '_' timestr]);
mkdir(outFolder);


%% Do task
here = pwd;
cd(streamAndRecordModelLocation);
for i = 1 : length(inputCurrents)
    
    % make .stim file
    % make current step stimulus file
    thisI = inputCurrents(i);
    trialStimFile = 'thisStim.stim'; % DONT TOUCH!
    fid = fopen(trialStimFile,'w');
    fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', 0.5,	1,	0,	    0, 	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', 0.01,	1,	-300,	0,	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', 0.5,	1,	0,	    0,	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', 0.6,	1,	-100,	0,	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', 1, 	1,	0,	    0,	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fprintf(fid, '%f\t%f\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', 5, 	1,	thisI,	    0,	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', 0.1, 	1,	0,	    0,	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fclose(fid);
    
    
    % run measurement
    [tg measurementData StreamValues parameters] = setup_arbitraryStream_and_record(trialStimFile, 'kernelfile', kernelfile, 'compileFlag', 0, 'apmilifierConfigFile', apmilifierConfigFile);
    rec = [];
    rec.voltage = measurementData(:,1);
    rec.current = measurementData(:,2);
    rec.command = StreamValues;
    
    % save file
    systemtime=clock;
    timestr = sprintf('%04d%02d%02d%02d%02d%02d%02d',systemtime(1),systemtime(2),systemtime(3),systemtime(4),systemtime(5),floor( systemtime(6) ), ceil(mod( systemtime(6),100 )) ); % name YYYMMDDHHMMSSdd
    pp = fullfile(outFolder,timestr); mkdir(pp);
    save(fullfile(pp,dataFileName), 'parameters', 'rec');
    
    
    % wait
    pause(waitBetweenTrials(i));
end

cd(here);


%% Call analysis
[RR  taus Vrests Vs Is figh] = get_VI_R_specialForVIprotocol(outFolder, 'threshold',threshold, 'ignoreSpikyTracesFlag', ignoreSpikyTracesFlag);
saveas(figh, fullfile(outFolder, 'VIfig.fig'));
save( fullfile(outFolder,'VI_R_data.mat') , 'RR',  'taus', 'Vrests', 'Vs', 'Is');

end