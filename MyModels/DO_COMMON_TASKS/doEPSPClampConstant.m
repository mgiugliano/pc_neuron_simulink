function [tg allStimuli allSlidingEPSPsizes allTimes picLocation] = doEPSPClampConstant( )
% Does a constant extracellular stimulus, using the all other properties from a previous file. 

% load cell folder location
A_OUTPUT_LOCATION
%% ask fot he log file of the previous experiment ( usually named 'MYDATA.mat' ).
[FileName,PathName] = uigetfile(cellFolder);

getDataNow = 0;                    % transfer traces from target immediately at end. (1 yes 2 means no)





%% USUALLY YOU DO NOT CHANGE THE PART BELOW ! ! ! ! ! !
%% SETTINGS AND PARAMETERS -- this time they are loaded from previous experiment's settings!!!!!!!!!

load(fullfile(PathName,FileName));

%mean of all stimulus amps
thisStimulus=mean(parameters.extracellStim.stimulusAmplitudes); % [uA] extracellular stimulus amplitude 
thisStimulus=90;

% STIMULI TO APPLY 
PlaybackModeSTGvalues = thisStimulus * ones(1,length(parameters.extracellStim.stimulusAmplitudes));  % same ammount of stimuli but different amplitudes

kernelfile = parameters.kernelfile;            % name of kernel file to use

% DURATION
runDuration = parameters.runDuration;          % [s] duration of run

 % EXTRACELLULAR STIMULUS
InterstimInterval = parameters.extracellStim.InterstimInterval;       % [s] interstimulus interval
firstSTGstimulusValue = parameters.extracellStim.firstSTGstimulusValue;   % [uA] OR [mV] fist STG stimulus value (to start from).
LIMITSTGstimulusValue = parameters.extracellStim.LIMITSTGstimulusValue;   % [uA] OR [mV] maximal possible STG stimulus value (to start from).

% PROBABILITY CLAMP
targetEPSP = parameters.EPSPsizeClamp.targetEPSPsize; % [0 to 1] firing probability to keep
P_stg = parameters.EPSPsizeClamp.P_stg;                 % P of a PID controller
I_stg = parameters.EPSPsizeClamp.I_stg;                 % I of a PID controller
EPSPToStimAmpltude = parameters.EPSPsizeClamp.EPSPToStimAmpltude;   % "gain", i.e. conversion of EPSP-->nextSTGstimulus calculation
tau_history = parameters.EPSPsizeClamp.tau;        % [s] decay time constant of history

% VOLTAGE HOLDING - apply current to hold voltage at vHold, except at
% stimulus and recording events - done by PI controller
enableVoltageHold = parameters.voltageClamping.enableVoltageHold;      % 1 or 0 for enabling or disabling feature
vHold = parameters.voltageClamping.vHold;                % [mV] voltage to hold by current injection


% OTHER
tag = 'EPSPclampReplayConstant_';                   % tag for folder name (experiment distingisher or comment)


%% DO IT

% load cell folder location
A_OUTPUT_LOCATION

% destinationFolder = fullfile('Recordings', 'StabilityTests');
ttt = clock;
tstamp = [num2str(ttt(1)) num2str(ttt(2)) num2str(ttt(3)) num2str(ttt(4)) num2str(ttt(5)) num2str(ceil(ttt(6)))];
destinationFolder=fullfile(cellFolder,[tag tstamp]);

% set destionation
addpath('functions');
A_OUTPUT_LOCATION

here = pwd;
cd(EPSPClampDir);


% do clamp
[state, allStimuli, allSlidingEPSPsizes, allTimes , TET,dt, tg] = setup_EPSPsizeClamp(runDuration,...
    'InterstimInterval', InterstimInterval, 'firstSTGstimulusValue', firstSTGstimulusValue, 'LIMITSTGstimulusValue', LIMITSTGstimulusValue, ...
    'targetEPSP',targetEPSP, 'P_stg',P_stg, 'I_stg',I_stg, 'EPSPToStimAmpltude',EPSPToStimAmpltude, 'tau_history',tau_history, ...
    'enableVoltageHold',enableVoltageHold, 'vHold', vHold, ...
    'destinationFolder', destinationFolder,  'getDataNow', getDataNow,...
    'PlaybackModeSTGvalues', PlaybackModeSTGvalues);

% make a plot
ff=figure(); subplot(2,1,1); ylabel('stimulus [uA]')
plot(allTimes,allStimuli)
subplot(2,1,2); plot(allTimes,allSlidingEPSPsizes);  ylabel('EPSP [mV]'); xlabel('time (s)');
picLocation = fullfile(destinationFolder,['EPSPclampReplayConstant_' tstamp '.png']);
print(gcf, picLocation, '-dpng', '-loose');
close(ff);

% add a note of the replayed file
originalFile = fullfile(PathName,FileName);
save(fullfile(destinationFolder, 'thisIsReplayOfFile.mat'), 'originalFile');

% go back
cd(here);

end


