function [tg stimuli probabilities times picLocation] = doResponseClamp( )
% Does a clamp of firing response probability by changins extracellular stimulus amplitude. 




%% SETTINGS AND PARAMETERS
kernelfile = '';            % name of kernel file to use

% DURATION
runDuration = 1200;          % [s] duration of run

 % EXTRACELLULAR TIMULUS
InterstimInterval = 4;       % [s] interstimulus interval
firstSTGstimulusValue = 230;   % [uA] OR [mV] fist STG stimulus value (to start from).
LIMITSTGstimulusValue = 799;   % [uA] OR [mV] maximal possible STG stimulus value (to start from).

% PROBABILITY CLAMP
targetProbability = 0.5;   % [0 to 1] firing probability to keep
P_stg = 1;                 % P of a PID controller
I_stg = 0.3;                 % I of a PID controller
probToStimAmpltude = 700;   % "gain", i.e. conversion of Probability-->nextSTGstimulus calculation
tau_history = 600;        % [s] decay time constant of history

% SPIKE DETECTION 
threshold = -30;               % [mV] spike detection by threshold crossing

% VOLTAGE HOLDING - apply current to hold voltage at vHold, except at
% stimulus and recording events - done by PI controller
enableVoltageHold = 1;      % 1 or 0 for enabling or disabling feature
vHold = -70;                % [mV] voltage to hold by current injection

% OTHER
tag = 'Pclamp_';                   % tag for folder name (experiment distingisher or comment)
getDataNow = 0;                    % transfer traces from target immediately at end. (1 yes 2 means no)






%% DO IT

% load cell folder location
A_OUTPUT_LOCATION

% destinationFolder = fullfile('Recordings', 'StabilityTests');
ttt = clock;
tstamp = [num2str(ttt(1)) num2str(ttt(2)) num2str(ttt(3)) num2str(ttt(4)) num2str(ttt(5)) num2str(ceil(ttt(6)))];
destinationFolder=fullfile(cellFolder,[tag tstamp]);

% set destionation
addpath('functions');
A_OUTPUT_LOCATION

here = pwd;
cd(frequencyClampDir);


% do clamp
[state, stimuli, probabilities, allFiredOrNot, times , TET,dt, tg] = setup_probabilityClamp(runDuration,...
    'InterstimInterval', InterstimInterval, 'firstSTGstimulusValue', firstSTGstimulusValue, 'LIMITSTGstimulusValue', LIMITSTGstimulusValue, ...
    'targetProbability',targetProbability, 'P_stg',P_stg, 'I_stg',I_stg, 'probToStimAmpltude',probToStimAmpltude, 'tau_history',tau_history, ...
    'threshold',threshold, 'enableVoltageHold',enableVoltageHold, 'vHold', vHold, ...
    'destinationFolder', destinationFolder, 'getDataNow', getDataNow,...
    'PlaybackModeSTGvalues', []);

% make a plot
ff=figure(); subplot(2,1,1); ylabel('stimulus [uA]')
plot(times,stimuli)
subplot(2,1,2); plot(times,probabilities);  ylabel('probability'); xlabel('time (s)');
saveas(gcf, fullfile(destinationFolder,['PClamp_' tstamp]),'fig')
picLocation = fullfile(destinationFolder,['PClamp_' tstamp '.png']);
print(gcf, picLocation, '-dpng', '-loose');
%close(ff);

% go back
cd(here);

end


