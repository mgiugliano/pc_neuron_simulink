function [taus Rs outFolder] = do_Tau_R()
% This function will do a tau and R test of the patched neuron.
% It uses the model arbitraryStream_and_record.
% Output folder will be set according to settings in A_OUTPUT_LOCATION.m,
% in the directory of the neuron (cellFolder), under Tau_R_<time> and in a
% folder named from the execution time of the script.


% Istvan Biro, 2014.05.26. (last update).

%% Parameters

threshold = 0; % spike detection threshold

repeatTimes = 30;
ignoreSpikyTracesFlag = 1; % 1 or 0. 1 will ignore any trace with action potentials.

waitBetweenTrials = 0; % one number 

kernelfile = '';  % leave '' for no AEC

dataFileName = 'RECORDINGS.mat'; % each recording will be in a folder named by the time, containing a file named like this.

destination_config_file = '.\A_OUTPUT_LOCATION.m';  % name of file containing destination paths and additional configurations

apmilifierConfigFile = '.\..\..\HARDWARE_CONFIGURATION\patchAmplifier_CClamp.m';  % file that stores conversion factors for amplifier - MUST HAVE PATH RELATIVE TO THE MODEL FILE LOCATION!!!!!!!!!

%% Prepare
taus=[]; Rs = []; Vrests = []; systimes = {}; outFolder = '';

% read destination and parameters file
run(destination_config_file);


% get time and make output file name
systemtime=clock;
timestr = sprintf('%04d%02d%02d%02d%02d%02d%02d',systemtime(1),systemtime(2),systemtime(3),systemtime(4),systemtime(5),floor( systemtime(6) ), ceil(mod( systemtime(6),100 )) ); % name YYYMMDDHHMMSSdd
outFolder = fullfile(cellFolder, [Tau_R_Folder '_' timestr]);
mkdir(outFolder);


%% Do task
here = pwd;
cd(streamAndRecordModelLocation);
for i = 1 : repeatTimes
    
    % make .stim file
    % make current step stimulus file
    trialStimFile = 'thisStim.stim'; % DONT TOUCH!
    fid = fopen(trialStimFile,'w');
    fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', 0.5,	1,	0,	    0, 	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', 0.01,	1,	-300,	0,	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', 0.5,	1,	0,	    0,	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', 0.6,	1,	-100,	0,	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', 0.2, 	1,	0,	    0,	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fclose(fid);
    
    
    % run measurement
    [tg measurementData StreamValues parameters] = setup_arbitraryStream_and_record(trialStimFile, 'kernelfile', kernelfile, 'compileFlag', 0, 'apmilifierConfigFile', apmilifierConfigFile);
    rec = [];
    rec.voltage = measurementData(:,1);
    rec.current = measurementData(:,2);
    rec.command = StreamValues;
    
    % save file
    systemtime=clock;
    timestr = sprintf('%04d%02d%02d%02d%02d%02d%02d',systemtime(1),systemtime(2),systemtime(3),systemtime(4),systemtime(5),floor( systemtime(6) ), ceil(mod( systemtime(6),100 )) ); % name YYYMMDDHHMMSSdd
    pp = fullfile(outFolder,timestr); mkdir(pp);
    save(fullfile(pp,dataFileName), 'parameters', 'rec');
    
    
    % wait
    pause(waitBetweenTrials);
end

cd(here);


%% Call analysis
checkForSpikesInPassiveProperties = ignoreSpikyTracesFlag; % 1 or 0 : check or not if an AP is present in the traces used for passeive cell properties. If checking is on and an AP is present, the passive properties will be [].
% Vrest part
vrestStartStopTime = [0 0.5]; % [s] starting and stopping time of the trace to extract Vrest from.
% tau part
tauStartStopTime = [0.5103 0.565]; % [s] starting and stopping time of the trace to extract tau from.
expNo = 1;              % number of esplonentilas to fit - use 1, 2 or 3. 1 is best though.
% R part
RStartStopTime = [1.4 1.6]; % [s] starting and stopping time of the trace to extract R from.
[outFileList_Vrest_single, outFileList_R_single, outFileList_tau_single, outFileList_F_currentNoise_single] = get_tau_R(outFolder, 0, 'checkForSpikesInPassiveProperties', checkForSpikesInPassiveProperties, ...
    'vrestStartStopTime',vrestStartStopTime,'tauStartStopTime',tauStartStopTime,'expNo',expNo);
for i=1:length(outFileList_Vrest_single)
    load(outFileList_Vrest_single{i});
    v = Vrest;
    load(outFileList_R_single{i});
    r = R;
    load(outFileList_tau_single{i});
    t = tau;
    tm = systemtime;
    if ~isempty(v) & ~isempty(r) & ~isempty(t) & ~isempty(tm) , taus(end+1)=t; Rs(end+1) = r; Vrests(end+1)=v; systimes{end+1}= tm; end
    
end
save( fullfile(outFolder,'tau_R_data.mat') , 'taus',  'Rs', 'Vrests', 'systimes');

end