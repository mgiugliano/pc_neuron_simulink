

%% enter here the location where to place the results
% trialFolder is optional (for further separation)- leave '' fi not provided.

cellFolder =  'D:\ISTVAN\DATA ALL\2014_05_28_TESTS\cell03';
trialFolder = '01'; 
% kernelfile = '';  % for AEC. use '' not to use the AEC.

%% specific folder names for specific measurements within the above selected location 
kernelFolder = 'KERNELS';    % for AEC kernel measurements 
VI_Folder = 'VI';            % for VI curves
FI_Folder = 'FI';
KernelFolder = 'KERNEL';
Tau_R_Folder = 'Tau_R';

test_Folders_G_noise = 'TEST_Gnoise';
test_Folders_freqClamp = 'TEST_Fclamp';
test_Folders_vClamp = 'TEST_Vclamp';

sineInputRates_SYN_FILTER_folder = 'sinRates_SynFilter';

IforF_Folder = 'IforFs';
Tau_R = 'Tau_R';
RampFolder = 'Ramp';
StabilityWithFiringFolder = 'StabilityWithFiring';
sinCurr_condNoise_Folder = 'sinCurrMod_condNoise';

%% definition of model locations - do not change.
streamAndRecordModelLocation = '.\..\arbitraryStream_and_record';
streamAndRecordModelLocation_ConductanceClamp = '.\..\arbitraryStream_and_record_ConductanceClamp';
streamAndRecordModelLocation_TEST_fclamp = '.\..\arbitraryStream_and_record_TEST_fclamp';
streamAndRecordModelLocation_TEST_synapticInputs = '.\..\arbitraryStream_and_record_TEST_synapticInputs';
streamAndRecordModelLocation_TEST_vclamp = '.\..\arbitraryStream_and_record_TEST_vclamp';
sine_GImodulation_withSynapticFilter_Folder = '.\..\sine_modulated_conductance_rates_andCurrent_withSynapticFiltering';

sinModelDir = 'D:\ISTVAN\MY MODELS\03_SINUS_currentANDconductance';
I_offset_for_F_dir = 'D:\ISTVAN\MY MODELS\03_getCurrentForFiringFrequency';
stabilityTestModelDir = 'D:\ISTVAN\MY MODELS\03_arbitraryStream_and_KernelOnline';
frequencyClampDir  = 'D:\ISTVAN\MY MODELS\RESPONSE PROBABILITY CLAMP';
EPSPClampDir = 'D:\ISTVAN\MY MODELS\EPSP SIZE CLAMP';

