function [] = do_ConductanceNoise_nomodulation_TEST()


% Istvan Biro, 2014.05.26. (last update).

%% Parameters
totalTime = 30; % s to run.

threshold = 0; % spike detection threshold

kernelfile = '';  % leave '' for no AEC

dataFileName = 'RECORDINGS.mat'; % each recording will be in a folder named by the time, containing a file named like this.

destination_config_file = '.\A_OUTPUT_LOCATION.m';  % name of file containing destination paths and additional configurations

apmilifierConfigFile = '.\..\..\HARDWARE_CONFIGURATION\patchAmplifier_CClamp.m';  % file that stores conversion factors for amplifier - MUST HAVE PATH RELATIVE TO THE MODEL FILE LOCATION!!!!!!!!!

%% Prepare
outFolder = '';

% read destination and parameters file
run(destination_config_file);


% get time and make output file name
systemtime=clock;
timestr = sprintf('%04d%02d%02d%02d%02d%02d%02d',systemtime(1),systemtime(2),systemtime(3),systemtime(4),systemtime(5),floor( systemtime(6) ), ceil(mod( systemtime(6),100 )) ); % name YYYMMDDHHMMSSdd
outFolder = fullfile(cellFolder, [test_Folders_G_noise '_' timestr]);
mkdir(outFolder);


%% Do task
here = pwd;
cd(streamAndRecordModelLocation_TEST_synapticInputs);

    
    % make .stim file
    % make current step stimulus file
    trialStimFile = 'thisStim.stim'; % DONT TOUCH!
    fid = fopen(trialStimFile,'w');
    fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', totalTime,	1,	0,	    0, 	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fclose(fid);
    
    
    % run measurement
    [tg measurementData StreamValues parameters] = setup_arbitraryStream_and_record_ConductanceNoiseTest(trialStimFile, 'kernelfile', kernelfile, 'compileFlag', 0, 'apmilifierConfigFile', apmilifierConfigFile);
    rec = [];
    rec.voltage = measurementData(:,1);
    rec.current = measurementData(:,2);
    rec.command = StreamValues;
    rec.synapticCurrent = measurementData(:,3);
    rec.G_exc = measurementData(:,4);
    rec.G_inh = measurementData(:,5);
    
    % save file
    systemtime=clock;
    timestr = sprintf('%04d%02d%02d%02d%02d%02d%02d',systemtime(1),systemtime(2),systemtime(3),systemtime(4),systemtime(5),floor( systemtime(6) ), ceil(mod( systemtime(6),100 )) ); % name YYYMMDDHHMMSSdd
    pp = fullfile(outFolder,timestr); mkdir(pp);
    save(fullfile(pp,dataFileName), 'parameters', 'rec');
    
    
    % wait


cd(here);


%% Call analysis


end