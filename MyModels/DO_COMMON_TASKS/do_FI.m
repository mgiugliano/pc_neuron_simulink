function [R outFolder] = do_FI()
% This function will do a F(I) curve of the patched neuron.
% It uses the model arbitraryStream_and_record.
% Output folder will be set according to settings in A_OUTPUT_LOCATION.m,
% in the directory of the neuron (cellFolder), under FI_<time> and in a
% folder named from the execution time of the script.
% ***** If inputCurrents = [], than you will be asked for each step manually!


% Istvan Biro, 2014.05.26. (last update).

%% Parameters

shuffleFlag = 0; % shuffle (1) or not (0) the input values

threshold = 0; % spike detection threshold

stepDur = 7;  % [s] step duratio: the length of the spiking DC - one number or a vector of length(inputCurrents)

inputCurrents = []; % [0:50:300]; % []; % vector of current steps in pA --- these will be done "repeatTimes" times. E.g.: [-200:50:50];
repeatTimes = 1;

waitBetweenTrials = [5]; % one number or a vector of length(inputCurrents) saying how long to wait after each trial

kernelfile = '';   % leave '' for no AEC

dataFileName = 'RECORDINGS.mat'; % each recording will be in a folder named by the time, containing a file named like this.

destination_config_file = '.\A_OUTPUT_LOCATION.m';  % name of file containing destination paths and additional configurations

%% Prepare
R = []; outFolder = '';
Fs = [];
Is = [];
spk_times = {};

% read destination and parameters file
run(destination_config_file);

% settle waiting times
if ~isempty(inputCurrents)
    if length(waitBetweenTrials)==1, waitBetweenTrials = repmat(waitBetweenTrials,1,length(inputCurrents));
    elseif length(waitBetweenTrials)~= length(inputCurrents), error(' Waiting times are wrong, give one number or a vector of same length as current steps!');
    end
end

% settle stepDur
if ~isempty(inputCurrents)
    if length(stepDur)==1, stepDur = repmat(stepDur,1,length(inputCurrents));
    elseif length(stepDur)~= length(stepDur), error(' stepDur are wrong, give one number or a vector of same length as current steps!');
    end
end

% get all current steps
if ~isempty(inputCurrents)
    [rr cc] = size(inputCurrents);
    if rr>cc, inputCurrents=inputCurrents'; end
    inputCurrents = repmat(inputCurrents,1,repeatTimes);
    [rr cc] = size(waitBetweenTrials);
    if rr>cc, waitBetweenTrials=waitBetweenTrials'; end
    waitBetweenTrials = repmat(waitBetweenTrials,1,repeatTimes);
    [rr cc] = size(stepDur);
    if rr>cc, stepDur=stepDur'; end
    stepDur = repmat(stepDur,1,repeatTimes);
end

% shuffle if needed
if shuffleFlag==1
    ss = length(inputCurrents);
    ind = randperm(ss);
    inputCurrents = inputCurrents(ind);
    waitBetweenTrials = waitBetweenTrials(ind);
    stepDur = stepDur(ind);
end

% get time and make output file name
systemtime=clock;
timestr = sprintf('%04d%02d%02d%02d%02d%02d%02d',systemtime(1),systemtime(2),systemtime(3),systemtime(4),systemtime(5),floor( systemtime(6) ), ceil(mod( systemtime(6),100 )) ); % name YYYMMDDHHMMSSdd
outFolder = fullfile(cellFolder, [FI_Folder '_' timestr]);
mkdir(outFolder);


%% Do task
here = pwd;
cd(streamAndRecordModelLocation);

% see if manual or automatic mode is done
if isempty(inputCurrents)
    prompt = {'Enter next stimulus value [pA] ( enter Inf to stop) :'};
    dlg_title = 'FI curve point'; num_lines = 1; def = {'Inf'};
    answer = inputdlg(prompt,dlg_title,num_lines,def);
    thisI = str2num(answer{1});
    sDur = stepDur(1);
    waitB = waitBetweenTrials(1);
else
    thisI = inputCurrents(1);
    sDur = stepDur(1);
    waitB = waitBetweenTrials(1);
end

cnt = 0;
while thisI~= Inf
    cnt = cnt + 1;
    
    % make .stim file
    % make current step stimulus file
    trialStimFile = 'thisStim.stim'; % DONT TOUCH!
    fid = fopen(trialStimFile,'w');
    fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', 0.5,	1,	0,	    0, 	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', 0.01,	1,	-300,	0,	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', 0.5,	1,	0,	    0,	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', 0.6,	1,	-100,	0,	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', 1, 	1,	0,	    0,	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fprintf(fid, '%f\t%f\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', sDur, 	1,	thisI,	    0,	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', 0.1, 	1,	0,	    0,	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fclose(fid);
    
    
    % run measurement
    [tg measurementData StreamValues parameters] = setup_arbitraryStream_and_record(trialStimFile, 'kernelfile', kernelfile, 'compileFlag', 0);
    rec = [];
    rec.voltage = measurementData(:,1);
    rec.current = measurementData(:,2);
    rec.command = StreamValues;
    
    % save file
    systemtime=clock;
    timestr = sprintf('%04d%02d%02d%02d%02d%02d%02d',systemtime(1),systemtime(2),systemtime(3),systemtime(4),systemtime(5),floor( systemtime(6) ), ceil(mod( systemtime(6),100 )) ); % name YYYMMDDHHMMSSdd
    pp = fullfile(outFolder,timestr); mkdir(pp);
    save(fullfile(pp,dataFileName), 'parameters', 'rec');
    
    
    % look at trace
    dt = parameters.dt;
    time = [0:dt:dt*(length(rec.voltage)-1)];
    % only look at the relevant segment of firing, 1 s after step onset
    signalIndex = find( (time>2.7+1) & (time<2.6+sDur) );
    % get number of spikes and calculate frequency
    myvoltage = rec.voltage(signalIndex);
    mytime = time(signalIndex);
    out = I_extract_peaks(myvoltage, threshold);
    Fs(cnt) = length(out) / ( length(signalIndex)*dt );
    Is(cnt) = thisI;
    spk_times{cnt} = mytime(out);
    disp(['* I=' num2str(Is(end)) ' , detectd ' num2str(length(out)) ' spikes in ' num2str(length(signalIndex)*dt) ' s => frequency : ' num2str(Fs(end)) ' Hz.']);
    
    
    
    % set things and wait for next run
    if isempty(inputCurrents)
        prompt = {'Enter next stimulus value [pA] ( enter Inf to stop) :'};
        dlg_title = 'FI curve point'; num_lines = 1; def = {'Inf'};
        answer = inputdlg(prompt,dlg_title,num_lines,def);
        thisI = str2num(answer{1});
    else
        if cnt<length(inputCurrents)
            thisI = inputCurrents(cnt);
            sDur = stepDur(cnt);
            waitB = waitBetweenTrials(cnt);
        else thisI = Inf;
        end
    end
    
    % wait
    pause(waitB);
    
end

cd(here);


%% Save FI values
figh = figure(); hold all; plot(Is,Fs,'o');
saveas(figh, fullfile(outFolder, 'FIfig.fig'));
save( fullfile(outFolder,'FI_data.mat') , 'Fs',  'Is', 'spk_times');

end