function [mykernelFileName Ke myvoltage mycurrent myind tg] = doKernelTest(varargin)
% Make Kernel for Active Electrode Compensation (current clamp)

plothandle = 5687132;

kernelSizeTime = 5; % [ms]

myduration = 20; % [s] white noise duration

plotFigure=0;

dataFileName = 'RECORDINGS.mat'; % each recording will be in a folder named by the time, containing a file named like this.

destination_config_file = '.\A_OUTPUT_LOCATION.m';  % name of file containing destination paths and additional configurations

apmilifierConfigFile = '.\..\..\HARDWARE_CONFIGURATION\patchAmplifier_CClamp.m';  % file that stores conversion factors for amplifier - MUST HAVE PATH RELATIVE TO THE MODEL FILE LOCATION!!!!!!!!!


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%% EVALUATE VARAGIN %%%%%%%%%%%%%%%%%%%
% evaluating varargin; (Name, value) pairs, as ('plotFitHandle', 67)
% expected
noVars = length(varargin);
for i=1:2:noVars
    if isnumeric(varargin{i+1})
        if (isscalar(varargin{i+1})), commandName=sprintf('%s%s%f%s',varargin{i},'=',varargin{i+1},';');
        else commandName=sprintf('%s%s%s%s',varargin{i},'=',mat2str(varargin{i+1}),';');
        end
    else commandName=sprintf('%s%s%s%s',varargin{i},'=''',varargin{i+1},''';');
    end
    eval(commandName);
end



% read destination and parameters file
run(destination_config_file);


here = pwd;
cd(streamAndRecordModelLocation);

% get time and make output file name
systemtime=clock;
timestr = sprintf('%04d%02d%02d%02d%02d%02d%02d',systemtime(1),systemtime(2),systemtime(3),systemtime(4),systemtime(5),floor( systemtime(6) ), ceil(mod( systemtime(6),100 )) ); % name YYYMMDDHHMMSSdd
outFolder = fullfile(cellFolder, [KernelFolder '_' timestr]);
mkdir(outFolder);


if plothandle~=0, figure(plothandle); hold on; end

% create .stim file
trialStimFile = 'whiteNoiseStim.stim'; % DONT TOUCH!
fid = fopen(trialStimFile,'w');
fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', 0.1,	1,	0,	    0, 	0,	0,	0,	0,	3532765,	0,	0,	1); %
fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', myduration,	11,	0,	    150,0,	0,	0,	0,	3532765,	0,	0,	1); %
fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', 0.1,	1,	0,	    0,	0,	0,	0,	0,	3532765,	0,	0,	1); %
fclose(fid);
myvoltage = [];
mycurrent = [];


if plotFigure==1,    ff=figure(2586547); close(ff); ff=figure(2586547); hold on; end

%run it
reRun =1;
while reRun==1
    [tg measurementData StreamValues parameters] = setup_arbitraryStream_and_record('whiteNoiseStim.stim', 'kernelfile', '', 'compileFlag', 0, 'apmilifierConfigFile', apmilifierConfigFile);
    rec = [];
    rec.voltage = measurementData(:,1);
    rec.current = measurementData(:,2);
    rec.command = StreamValues;
    
    % save file
    systemtime=clock;
    timestr = sprintf('%04d%02d%02d%02d%02d%02d%02d',systemtime(1),systemtime(2),systemtime(3),systemtime(4),systemtime(5),floor( systemtime(6) ), ceil(mod( systemtime(6),100 )) ); % name YYYMMDDHHMMSSdd
    pp = fullfile(outFolder,timestr); mkdir(pp);
    save(fullfile(pp,dataFileName), 'parameters', 'rec');
    
    %  data(1): measured current [pA]
    %  data(2): measures voltage [mV]
    %  data(3): intended current [pA]
    
     myvoltage = rec.voltage;
     mycurrent = rec.command; % INTENDED current
     dt = parameters.dt;

    % time
    time = [0:dt:dt*(length(myvoltage)-1)]; 
    
    % calculate Kernel
    acceptedKernel = 0;
    myind = find( (time>0.1) & (time<myduration+0.1) );
    kernelSize = round(kernelSizeTime /(dt*1000));
   
    [K,v0] = fullKernel(myvoltage(myind)'*1e-3,mycurrent(myind)*1e-12,kernelSize);
    % v0 [V] ;;; K: [Ohm] ;;; myvoltage [V] ;;; mycurrent [A]
    K = K *1e-6; % [Ohm] to [MOhm]
    kt = [0:dt:dt*(length(K)-1)];
    figure(plothandle);
 %   plot(kt*1000, K*1e-6, 'ko-' );
    % plot(time( myind(1:100) ), K, 'ko-' );
    [maxval maxind] = max(K);
    cutInd = find(kt>mean(kt));
    cutoffPoint = cutInd(1);
    while acceptedKernel==0
        fff = figure(plothandle); close(fff); fff = figure(plothandle);
        plot(kt*1000, K, 'ko-' );
        hold on;
        plot(kt(cutoffPoint)*1000, K( cutoffPoint) , 'ro-' );
%         keyboard
        try
            [Ke,Re,Km, Rm,taum] = electrodeKernel(K,cutoffPoint,dt*1000);
            % Ke [MOhm],Re[MOhm],Km[MOhm],Rm[MOhm],Taum[ms] ;;; K[MOhm],dt[ms]
            outtext = sprintf('%s%.1f%s%.1f%s%.1f%s%.1f%s','Vrest = ',v0*1000,'mV ;; Relectrode(MOhm) = ',Re/1e6,' ; Rmembrane(MOhm) = ',Rm,' ; Relec(MOhm) = ',Re,' .. and point is marked on figure. Do you accept this? If not, set the cutoff point here. If -1 set, the trial will be re-run.');
            outtext1 = sprintf('%d',cutoffPoint);
            answer = inputdlg(outtext,'SELECTION TYPE', 1, {outtext1});
            answer = str2num(cell2mat(answer));
            if cutoffPoint == answer, acceptedKernel=1; reRun=0;
            else
                if answer == -1, acceptedKernel=1; reRun=1;
                else
                    cutoffPoint = answer; acceptedKernel=0;
                end
            end
        catch err
            answer = inputdlg('BAD KERNEL ESTIMATION ! ! ! 1 = use 0 ;; -1 = redo measurement. ','SELECTION TYPE', 1, {'-1'});
            answer = str2num(cell2mat(answer));
            if answer == -1, acceptedKernel=1; reRun=1;
            else
                acceptedKernel=1; reRun=0; Ke = zeros(1,kernelSize); Re=Inf; Rm=Inf; taum=Inf; K = zeros(1,kernelSize);
            end
        end
        
        close(plothandle); 
        
    end
    
end

% plot if needed
if plotFigure==1,
    figure(2586547);
    kt = [0:dt:dt*(length(K)-1)];
    plot(kt*1000,K,'r--');
    kt = [0:dt:dt*(length(Ke)-1)];
    plot(kt*1000,Ke,'b--');
end

% now we write file and set return values 
mykernelFileName = fullfile(outFolder,'KERNEL.dat');

tt = [0:dt:dt*(kernelSize-1)];
if size(tt,1)<size(tt,2), tt = tt'; end
if size(K,1)<size(K,2), K = K'; end
if size(Ke,1)<size(Ke,2), Ke = Ke'; end

Ke = Ke*1e6; % [MOhm] to [Ohm]
save('-ascii',mykernelFileName,'Ke','K');

cd(here);

end

