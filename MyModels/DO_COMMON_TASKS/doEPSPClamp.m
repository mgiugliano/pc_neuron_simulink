function [tg allStimuli allSlidingEPSPsizes allTimes picLocation] = doEPSPClamp( )
% Does a clamp of EPSP size by changins extracellular stimulus amplitude. 




%% SETTINGS AND PARAMETERS
kernelfile = '';            % name of kernel file to use

% DURATION
runDuration = 1200;          % [s] duration of run

 % EXTRACELLULAR TIMULUS
InterstimInterval = 4;       % [s] interstimulus interval
firstSTGstimulusValue = 200;   % [uA] OR [mV] fist STG stimulus value (to start from).
LIMITSTGstimulusValue = 700;   % [uA] OR [mV] maximal possible STG stimulus value (to start from).

% PROBABILITY CLAMP
EPSP_max=10;% [mV] maximal EPSP size found below spike threshold
targetEPSP = 0.5;   %  target factor between maximal EPSPsize ever found, and 0
P_stg = 1;                 % P of a PID controller
I_stg = 0.3;                 % I of a PID controller
EPSPToStimAmpltude = 1600;   % "gain", i.e. conversion of Probability-->nextSTGstimulus calculation
tau_history = 300;        % [s] decay time constant of history


% VOLTAGE HOLDING - apply current to hold voltage at vHold, except at
% stimulus and recording events - done by PI controller
enableVoltageHold = 1;      % 1 or 0 for enabling or disabling feature
vHold = -60;                % [mV] voltage to hold by current injection

% OTHER
tag = 'EPSPclamp_';                   % tag for folder name (experiment distingisher or comment)
getDataNow = 0;                    % transfer traces from target immediately at end. (1 yes 2 means no)






%% DO IT

% load cell folder location
A_OUTPUT_LOCATION

% destinationFolder = fullfile('Recordings', 'StabilityTests');
ttt = clock;
tstamp = [num2str(ttt(1)) num2str(ttt(2)) num2str(ttt(3)) num2str(ttt(4)) num2str(ttt(5)) num2str(ceil(ttt(6)))];
destinationFolder=fullfile(cellFolder,[tag tstamp]);

% set destionation
addpath('functions');
A_OUTPUT_LOCATION

here = pwd;
cd(EPSPClampDir);


% do clamp
[state, allStimuli, allSlidingEPSPsizes, allTimes , TET,dt, tg] = setup_EPSPsizeClamp(runDuration,...
    'InterstimInterval', InterstimInterval, 'firstSTGstimulusValue', firstSTGstimulusValue, 'LIMITSTGstimulusValue', LIMITSTGstimulusValue, ...
    'targetEPSP',targetEPSP, 'EPSP_max',EPSP_max,'P_stg',P_stg, 'I_stg',I_stg, 'EPSPToStimAmpltude',EPSPToStimAmpltude, 'tau_history',tau_history, ...
    'enableVoltageHold',enableVoltageHold, 'vHold', vHold, ...
    'destinationFolder', destinationFolder, 'getDataNow', getDataNow,...
    'PlaybackModeSTGvalues', []);

% make a plot
ff=figure(); subplot(2,1,1); ylabel('stimulus [uA]')
plot(allTimes,allStimuli)
subplot(2,1,2); plot(allTimes,allSlidingEPSPsizes);  ylabel('EPSP [mV]'); xlabel('time (s)');
saveas(gcf, fullfile(destinationFolder,['EPSPClamp_' tstamp]),'fig')
picLocation = fullfile(destinationFolder,['EPSPClamp_' tstamp '.png']);
print(gcf, picLocation, '-dpng', '-loose');
%close(ff);

% go back
cd(here);

end


