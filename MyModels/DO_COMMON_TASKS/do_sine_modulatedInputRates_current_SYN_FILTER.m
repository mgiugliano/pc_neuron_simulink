function [] = do_sine_modulatedInputRates_current_SYN_FILTER(varargin)


% Istvan Biro, 2014.05.26. (last update).

%% Parameters

kernelfile = ''; %'20120203191020_kernel.dat';  File name in which the AEC kernel is stored, as single variable, called Kernel
runDuration = 10; % [s]


%%%%% Synaptic input properties
% R = r0 + R1*sin(2*pi*R_omega*(t+R_t0));
RMembrane = 47 % [];% **** RMembrane - [Mohm]. if empty, standatd unit conductances are used, otherwise 2 and 6 % of 1/Rm.

v_balance = -56; % mV -- membrane voltege to balace conductance components for. use [] to set default exc and inh rates instead.

R0_exc =  7000;  % [Hz] excitatory rate - MEAN
tau_exc = 5;                      % [ms]
tau_inh = 10;                      % [ms]
E_rev_exc = 0;                    % [mV]
E_rev_inh = -80;
R_omega_exc = 1;
R_omega_inh = 1;
R_t0_exc = 0;
R_t0_inh = 0;
R_seed_exc = intmax('int16')*rand(1,1);
R_seed_inh = intmax('int16')*rand(1,1);
if ~isempty(RMembrane)
    g_unit_exc =  2/100 * 1/(RMembrane*1e6) * 1e9;  % [nS]
    g_unit_inh =  6/100 * 1/(RMembrane*1e6) * 1e9; % [nS]
else
    g_unit_exc =  50/1000;  % [nS]
    g_unit_inh =  190/1000; % [nS]
end
if ~isempty(v_balance)
    R_excOverR_inh = computeRatesRatio(v_balance, tau_exc, tau_inh, E_rev_exc, E_rev_inh, g_unit_exc, g_unit_inh);
    R0_inh = 1./R_excOverR_inh.* R0_exc;
else
    R0_inh = 3000;
end

R1_exc = 0 % 0.1*R0_exc;
R1_inh = 0 % 0.1*R0_inh;


%%%%% Spike
threshold = -15; % [mV] detection


%%%%% Sine modulated current
I0 = 0; 
I1 =  0; % 0.1*I0
I_omega = 1;
I_time0 = 0;

%%%%% other added current from file
trialStimFile = '';  % .stim file -- leave '' if none needed.


%%%%% Firing rate clamp (do not use sumultaneously with sine-modulation)
enableFiringRateClamp = 0; % flag (0 or 1) to disable and enable feature.
targetFrate = 5;
maxISI = 2; % [s] maximal interspike interval.
fi_P = 20;
fi_I = 10;
fi_currentIncreaseStep = 0.01;
fi_skipFirstNspikes = 3;
fi_historyWeight = 15;
fi_minSpikeDur = 0.00333;


%%%%% Model paranaters
myStepSize = 1./20000;

compileFlag = 0;

% warnings_checks = 'on';


%%%%% Other
apmilifierConfigFile = '.\..\..\HARDWARE_CONFIGURATION\patchAmplifier_CClamp.m';  % file that stores conversion factors for amplifier
dataFileName = 'RECORDINGS.mat'; % each recording will be in a folder named by the time, containing a file named like this.
destination_config_file = '.\A_OUTPUT_LOCATION.m';  % name of file containing destination paths and additional configurations
apmilifierConfigFile = '.\..\..\HARDWARE_CONFIGURATION\patchAmplifier_CClamp.m';  % file that stores conversion factors for amplifier - MUST HAVE PATH RELATIVE TO THE MODEL FILE LOCATION!!!!!!!!!



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%% EVALUATE VARAGIN %%%%%%%%%%%%%%%%%%%
% evaluating varargin; (Name, value) pairs, as ('plotFitHandle', 67)
% expected
% This allows to overwrite the settings defined above this level, from
% parameters passed to the function as arguments, for e.g. setting it to
% compile, using the additional arguments: 'compileFlag', 1.
noVars = length(varargin);
for i=1:2:noVars
    if isnumeric(varargin{i+1})
        if (isscalar(varargin{i+1})), commandName=sprintf('%s%s%f%s',varargin{i},'=',varargin{i+1},';');
        else commandName=sprintf('%s%s%s%s',varargin{i},'=',mat2str(varargin{i+1}),';');
        end
    else commandName=sprintf('%s%s%s%s',varargin{i},'=''',varargin{i+1},''';');
    end
    eval(commandName);
end





%% Prepare
outFolder = '';

% read destination and parameters file
run(destination_config_file);


% get time and make output file name
systemtime=clock;
timestr = sprintf('%04d%02d%02d%02d%02d%02d%02d',systemtime(1),systemtime(2),systemtime(3),systemtime(4),systemtime(5),floor( systemtime(6) ), ceil(mod( systemtime(6),100 )) ); % name YYYMMDDHHMMSSdd
outFolder = fullfile(cellFolder, [sineInputRates_SYN_FILTER_folder '_' timestr]);
mkdir(outFolder);


%% Do task
here = pwd;
cd(sine_GImodulation_withSynapticFilter_Folder);

if isempty(trialStimFile)
    % make .stim file
    % make current step stimulus file
    trialStimFile = 'thisStim.stim'; % DONT TOUCH!
    fid = fopen(trialStimFile,'w');
    fprintf(fid, '%f\t%d\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', runDuration,	1,	0,	    0, 	0,	0,	0,	0,	3532765,	0,	0,	1); %
    fclose(fid);
end


% run measurement
[tg measurementData StreamValues parameters] = setup_sine_modulated_G_I_withSynapticFiltering(trialStimFile, 'kernelfile', kernelfile, ...
    'runDuration', runDuration, 'RMembrane', RMembrane, 'v_balance', v_balance,...
    'R0_exc', R0_exc, 'R0_inh', R0_inh, 'tau_exc', tau_exc, 'tau_inh', tau_inh, ...
    'E_rev_exc', E_rev_exc, 'E_rev_inh', E_rev_inh, 'R_omega_exc', R_omega_exc, ...
    'R_omega_inh', R_omega_inh, 'R_t0_exc', R_t0_exc, 'R_t0_inh', R_t0_inh, ...
    'R_seed_exc', R_seed_exc, 'R_seed_inh', R_seed_inh, 'g_unit_exc', g_unit_exc, ...
    'g_unit_inh', g_unit_inh, 'R1_exc', R1_exc, 'R1_inh', R1_inh, 'threshold',  threshold, ...
    'I0', I0, 'I1', I1, 'I_omega', I_omega, 'I_time0', I_time0, 'enableFiringRateClamp', enableFiringRateClamp, ...
    'targetFrate', targetFrate, 'maxISI', maxISI, 'fi_P', fi_P, 'fi_I', fi_I, ...
    'fi_currentIncreaseStep', fi_currentIncreaseStep, 'fi_skipFirstNspikes', fi_skipFirstNspikes, ...
    'fi_historyWeight', fi_historyWeight, 'fi_minSpikeDur', fi_minSpikeDur, 'myStepSize', myStepSize, ...
    'compileFlag', compileFlag, 'apmilifierConfigFile',apmilifierConfigFile, ...
    'dataFileName', dataFileName, 'destination_config_file', destination_config_file, 'apmilifierConfigFile', apmilifierConfigFile);



rec = [];
rec.voltage = measurementData(:,1);
rec.current = measurementData(:,2);
rec.command = StreamValues;
rec.synapticCurrent = measurementData(:,3);
rec.G_exc = measurementData(:,4);
rec.G_inh = measurementData(:,5);

% save file
systemtime=clock;
timestr = sprintf('%04d%02d%02d%02d%02d%02d%02d',systemtime(1),systemtime(2),systemtime(3),systemtime(4),systemtime(5),floor( systemtime(6) ), ceil(mod( systemtime(6),100 )) ); % name YYYMMDDHHMMSSdd
pp = fullfile(outFolder,timestr); mkdir(pp);
save(fullfile(pp,dataFileName), 'parameters', 'rec');


% wait


cd(here);


%% Call analysis


end