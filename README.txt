INSTALL KIT FOR xPC BASED MODELS AND METHODS FOR NEUROSCIENCE.

VERSION 3.0 -- 2015.04.20.

Istvan Biro, 2015.04.20.

-- TO DO --

- install all necessary software: consult Documentation_v3.docx for a list and instructions.

- make sure your hardware is set up properly (the 2 computers are LAN-connected). Consult Documentation_v3.docx for details and instructions.

- install the libraries of this package by running Install_custom_libraries_and_functions.m in Matlab

- create host-target environment and BOOT CD for target PC and set up compiler options, by running setup_hostSettings_xPC_boot.m


-- ALL DONE --



NOTE: this package comes without any warranties. It is freely distributable and changable. Simulink documentation attached is a property of Mathworks. If you have questions, contact Istvan Biro: bistvan2001@gmail.com or Prof. Michele Guigliano: michele.giugliano@uantwerpen.be.

