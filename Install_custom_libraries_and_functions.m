% This script will install the customized Simulink blocks into the
% Simpulink Library Browser, making them available to use everywhere.

% Istvan Biro, 2014.02.13.


%% Get installation path
% get installation location
installFolder = input(' - Please enter the full path to the folder where \n you want the library and matleb functions \n to be intalled ( E.g.: C:\\neuroSimulink ). \n >>> ', 's');

if exist(installFolder)~=7 % not an existing folder
    try
        mkdir(installFolder);
    catch err
        disp(' !! ERROR : wrong folder input. Message: ' );
        err
        error('');
    end
end





%% Install libraries...

disp('**************************************************');
disp(' -> Installing custom blocks into your Simulink Browser Library \n - you will be able to find them under "NeuroAssets_xPC_library" ...... \n WARNING: previous installation will be overwritten by the repository library if you continue. \ If you added new elements to "NeuroAssets_xPC_library", but did not include them in the repository library, they will be lost.');
sourcePath = 'CustomSimBlocksLibrary_NeuroAssets_xPC';

answer = input(' Install libraries anyway? (Enter y or n, default y) >>> ', 's');
if isempty(answer), answer = 'y'; end;
if strcmp(answer,'y')
    
    % copy data
    here = fullfile(installFolder, 'NeuroAssets_xPC');
    mkdir( here );
    copyfile(sourcePath, here );
    
    % Add location permanently to Matlab path
    addpath(genpath(here)); savepath;
end


%% Install mat functions and models

disp('**************************************************');
disp(' -> Installing custom scripts AND models... \n - to be used with the provided models and help with your custom ones ...... \n WARNING: existing scripts with the same name will be overwritten with repository versions. \n Changed models will also be overwritten with the repository content.');

answer = input(' Install scripts anyway? (Enter y or n, default y) >>> ', 's');
if isempty(answer), answer = 'y'; end;
if strcmp(answer,'y')
    
    % copy data ---- scripts
    sourcePath = 'MatFunctions_NeuroAssets_xPC';
    here = fullfile(installFolder, 'MatFunctions_NeuroAssets_xPC');
    mkdir( here );
    copyfile(sourcePath, here);
    
    % Add location permanently to Matlab path
    addpath(genpath(here)); savepath;
    
    
    % copy data ---- MODELS
    sourcePath = 'MyModels';
    here = fullfile(installFolder, 'MyModels');
    mkdir( here );
    copyfile(sourcePath, here);
    
    % Add location permanently to Matlab path
    addpath(genpath(here)); savepath;
    
end

disp('******  D O N E !  -- locations were added permanently to Matlab path.');
disp('**************************************************');



%% Install harware and other configuration files

disp('**************************************************');
disp(' -> Installing hardware settings... \n - please correct them if needed in "HARDWARE_CONFIGURATION" ......  \n WARNING: existing files with the same name will be overwritten with repository versions.');
sourcePath = 'HARDWARE_CONFIGURATION';

answer = input(' Install libraries anyway? (Enter y or n, default y) >>> ', 's');
if isempty(answer), answer = 'y'; end;
if strcmp(answer,'y')
    
    % copy data
    here = fullfile(installFolder, 'HARDWARE_CONFIGURATION');
    mkdir( here );
    copyfile(sourcePath, here);
    
    % Add location permanently to Matlab path
    addpath(genpath(here)); savepath;
    
end

disp('******  D O N E !  -- locations were added permanently to Matlab path.');
disp('**************************************************');
