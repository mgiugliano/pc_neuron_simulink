% This script will set up the communication addresses (IP) and the xPC
% environment properties on the HOST computer. Run this script once, before
% creating the target PC's bootable CD image.

% 2014.01.14. Istvan Biro

clc; clear all;

%%
% DEFINE YOUR PARAMETER VALUES
% Enter the parameters you want here...

% COMMUNICATION
myTargetPcName = 'myOneAndOnlyXPC';     % just a name for your Target PC Environment Object
myIP = '143.168.1.2';         % the IP of the target machine you want to use
myGate = '143.168.1.1';       % the TCP/IP Gateway address of the target. This must be the same as the IP adress of the host PC!
myMask = '255.255.255.0';       % subnet mask address of the target (must be the same as on the host pc)
myPort = '22222';             % communication port of the target pc




%% Applying changes - do not touch unless you know what you are doing

disp('*************************************************');
disp(' ** SETTING UP target and host environment and communication details ..... ');

% get the targets, from which one can tell the current one... or create a new one
tgs = xpctarget.targets; % tgs is a "target object collection environment container"

% look if a Target Object Environment named myTargetPcName exists...
allNames = tgs.getTargetNames; % all Target Object Environment names
ii = 0; found = 0;
while found==0 && ii<length(allNames)  % search...
    ii = ii + 1;
    if strcmp(allNames{ii},myTargetPcName), found=1; end
end
if found == 0 % if myTargetPcName does not yet exist, we add a new
    tge = tgs.Add(myTargetPcName);
else % if it already exists, we select it
    tge = tgs.Item(myTargetPcName);
end
% now tge is the  Target Object Environment that we want to set (and use it for our Target Object)
disp(['---> done creating your xPC environment named: ' myTargetPcName]);


% settind this as default xPC and other environment properties
tgs.makeDefault(myTargetPcName);
disp('..it is now the default one.');

% setting environment parameters for our Target
% Name already set (myTargetPcName)
tge.TargetRAMSizeMB          = 'Auto';
tge.MaxModelSize             = '4MB';
tge.SecondaryIDE             = 'on';
tge.NonPentiumSupport        = 'off';
tge.MulticoreSupport         = 'on';
tge.HostTargetComm           = 'TcpIp';
tge.TcpIpTargetAddress       = myIP;
tge.TcpIpTargetPort          = myPort;
tge.TcpIpSubNetMask          = myMask;
tge.TcpIpGateway             = myGate;
tge.RS232HostPort            = 'COM1';
tge.RS232Baudrate            = '115200';
tge.TcpIpTargetDriver        = 'Auto';
tge.TcpIpTargetBusType       = 'PCI';
tge.TcpIpTargetISAMemPort    = '0x300';
tge.TcpIpTargetISAIRQ        = '5';
tge.TargetScope              = 'Enabled';
tge.TargetBoot               = 'CDBoot';
tge.USBSupport               = 'off';


% choose boot option
disp(' ');
disp(' -- > How would zou like to boot zour target PC? (CD recommended, default!)? Answer with number :');
disp('1 : CD/DVD or respective image file');
disp('2 : Removable disk (use for USB drives or Floppy) - might ERASE it!');
choice = input('Answer:  ','s');
if strcmp(choice,'1')
    tge.TargetBoot               = 'CDBoot';
elseif strcmp(choice,'2') 
    tge.TargetBoot               = 'BootFloppy';
    disp('- For using UBS drives for booting, USB support must be reenabled. ')
    disp(' WARNING: USB support can interfere with real-time operation. Read documentation for more details on USB use in xPC.');
    cc = input('Reenable USB support in xPC target? [answer y or n, default: y]  :  ','s');
    if strcmp(cc,'n'), tge.USBSupport = 'off';
    else tge.USBSupport = 'on'; disp('-- USB support for xPC enabled. Please make sure USB is not disabled from BIOS.');
    end
else
    tge.TargetBoot               = 'CDBoot';
end


disp(' ');

% setting up your compilers
disp(' ');
disp(' ');
disp(' ');
choice = input('Would you like to set up compilers now (recommended!)? Answer y or n (default n)? : ','s');
if strcmp(choice,'y')
    try
        mbuild -setup
        mex -setup
        xpcsetCC -setup
    catch err
        disp(' !!!! FAILED TO SET UP COMPILER !!! - please check the documentation and install compiler if missing. ');
    end
end


% the actual Target Objext is then (corresponding to tge)
tg = xpctarget.xpc(myTargetPcName); % tg is now the effective Target Object (representing the Target PC)

disp(' -- > your xPC environmet has been set up. It has following properties (IP is that of the target):')
getxpcenv

disp(' ');
disp(' ');
disp(' ');
disp('--> done setting xPC environment options.');


% write boot CD if needed
disp(' ');
disp(' ');
disp(' ');
if strcmp(tge.TargetBoot,'CDBoot')
    choice = input('--> Would you like to create the xPC boot image and optionally burn it to CD now? Answer y or n (default n)? : ','s');
    if strcmp(choice,'y')
        mypath = input('.. please insert the path where you want the image to be created. Input NO FILENAME just location, like C:\\xpcImage. > ','s');
        if ~exist(mypath), mkdir(mypath); end
        tge.CDBootImageLocation = mypath; 
        
        % just to fix a bug of 2013 in Matlab and make sure your image is
        % in the right place
        tmploc = fullfile(tempdir, 'cdboot.iso');
        realloc = fullfile(mypath, 'cdboot.iso');
        if exist(tmploc), delete(tmploc); end
        if exist(realloc), delete(realloc); end
        try
            try 
                xpcbootdisk 
            end
            copyfile(tmploc,realloc);
            if exist(tmploc), delete(tmploc); end
        catch err
            try             copyfile(tmploc,realloc);  end
            % if exist(tmploc), delete(tmploc); end
            disp(['..some problem occured, check if you have the disk image in: ' realloc ' --- you can burn it to CD wit another program.']);
            disp(['..alternatively, check in: ' tmploc ' --- you can burn it to CD wit another program.']);
        end
        
    end
end


% write USB / floppy boot if needed
disp(' ');
disp(' ');
disp(' ');
if strcmp(tge.TargetBoot,'BootFloppy')
    ee = 0;
    while ee==0
        choice = input('--> Please insert USB stick / floppy now and press Enter to contunue. : ','s');
        disp('--> Please enter the location at which booting files should be placed.');
        disp('For example, enter F:\ if your USB drive or Floppy has this drive letter assigned.');
        disp('Enter ''x'' to cancel operation.');
        choice = input('Location : ','s');
        if ~strcmp(choice, 'x'), 
            try
                tge.BootFloppyLocation = choice;
                xpcbootdisk
                ee=1;
            catch err
                disp(' ');
                disp(' ');
                disp(['- ERROR: could not find ' choice ' , or it is an invalid USB/Floppy drive. Operation skipped.'])
                disp(' ');
                disp(' ');
                ee=0;
            end
        end
        if strcmp(choice, 'x'), ee=1; end
    end
end



%% clean up
clear all;
disp('-------> All done. Now you should have the xPC boot image and communication settings, xPC environment and compilers set up.' );
disp('*************************************************');
